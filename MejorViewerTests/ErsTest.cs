﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MejorViewerTests
{
    [TestClass]
    public class EraTest
    {
        private DateTime 明治開始日 = new DateTime(1868, 9, 8);
        private DateTime 明治終了日 = new DateTime(1912, 7, 29);
        private DateTime 大正開始日 = new DateTime(1912, 7, 30);
        private DateTime 大正終了日 = new DateTime(1926, 12, 24);
        private DateTime 昭和開始日 = new DateTime(1926, 12, 25);
        private DateTime 昭和終了日 = new DateTime(1989, 1, 7);
        private DateTime 平成開始日 = new DateTime(1989, 1, 8);
        private DateTime 平成終了日 = new DateTime(2019, 4, 30);
        private DateTime 令和開始日 = new DateTime(2019, 5, 1);
        private DateTime 今日 = DateTime.Today;
        private DateTime 元号なし = new DateTime(1868, 9, 7);

        [TestMethod]
        public void DateTimeを和暦年へ変換()
        {
            Assert.AreEqual(0, DateTimeEx.GetJpYear(元号なし));
            Assert.AreEqual(1, DateTimeEx.GetJpYear(明治開始日));
            Assert.AreEqual(45, DateTimeEx.GetJpYear(明治終了日));
            Assert.AreEqual(1, DateTimeEx.GetJpYear(大正開始日));
            Assert.AreEqual(15, DateTimeEx.GetJpYear(大正終了日));
            Assert.AreEqual(1, DateTimeEx.GetJpYear(昭和開始日));
            Assert.AreEqual(64, DateTimeEx.GetJpYear(昭和終了日));
            Assert.AreEqual(1, DateTimeEx.GetJpYear(平成開始日));
            Assert.AreEqual(31, DateTimeEx.GetJpYear(平成終了日));
            Assert.AreEqual(1, DateTimeEx.GetJpYear(令和開始日));
        }

        [TestMethod]
        public void DateTimeをアルファベット付和暦年へ変換()
        {
            Assert.AreEqual("", DateTimeEx.GetShortEraJpYear(元号なし));
            Assert.AreEqual("M01", DateTimeEx.GetShortEraJpYear(明治開始日));
            Assert.AreEqual("M45", DateTimeEx.GetShortEraJpYear(明治終了日));
            Assert.AreEqual("T01", DateTimeEx.GetShortEraJpYear(大正開始日));
            Assert.AreEqual("T15", DateTimeEx.GetShortEraJpYear(大正終了日));
            Assert.AreEqual("S01", DateTimeEx.GetShortEraJpYear(昭和開始日));
            Assert.AreEqual("S64", DateTimeEx.GetShortEraJpYear(昭和終了日));
            Assert.AreEqual("H01", DateTimeEx.GetShortEraJpYear(平成開始日));
            Assert.AreEqual("H31", DateTimeEx.GetShortEraJpYear(平成終了日));
            Assert.AreEqual("R01", DateTimeEx.GetShortEraJpYear(令和開始日));
        }
    }
}
