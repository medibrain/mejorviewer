﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace Updater
{
    class Log
    {
        static string erDir = Application.StartupPath + "\\UpError\\";
        static string infoDir = Application.StartupPath + "\\UpInfo\\";

        static Log()
        {
            try
            {
                if (!System.IO.Directory.Exists(erDir)) System.IO.Directory.CreateDirectory(erDir);
                if (!System.IO.Directory.Exists(infoDir)) System.IO.Directory.CreateDirectory(infoDir);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ログフォルダの作成に失敗しました\r\n" + ex.Message);
            }
        }


        /// <summary>
        /// メインスレッドで発生したすべてのキャッチできなかった例外を記録し、
        /// 強制終了するイベントを登録します
        /// </summary>
        public static void SetExceptionEvent()
        {
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            Thread.GetDomain().UnhandledException += new UnhandledExceptionEventHandler(Application_UnhandledException);
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            Log.ErrorWrite(e.Exception);
            Environment.Exit(0);
        }

        static void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            if (ex != null) Log.ErrorWrite(ex);
            Environment.Exit(0);
        }

        /// <summary>
        /// エラー情報をログファイルに書き込みます
        /// </summary>
        /// <param name="ex"></param>
        public static void ErrorWrite(Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    string fileName = erDir + "Error" + DateTime.Today.ToString("yyMMddHHmm") + ".log";
                    using (var sw = new StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                    {
                        sw.WriteLine(DateTime.Now.ToString() + ",\r\n" + ex.Message + "\r\nStack:" + ex.StackTrace + "\r\n");
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    continue;
                }
                break;
            }
        }

        /// <summary>
        /// エラーメッセージを表示し、ログファイルに書き込みます
        /// </summary>
        /// <param name="logText"></param>
        /// <returns></returns>
        public static void ErrorWriteWithMsg(Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    string fileName = erDir + "Error" + DateTime.Today.ToString("yyMMddHHmm") + ".log";//DateTime.Today.ToString
                    using (var sw = new StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                    {
                        sw.WriteLine(DateTime.Now.ToString() + ",\r\n" + ex.Message + "\r\nStack:" + ex.StackTrace + "\r\n");
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }

            System.Windows.Forms.MessageBox.Show("エラーが発生しました\r\n\r\n" + ex.Message,
                "エラー", System.Windows.Forms.MessageBoxButtons.OK,
                System.Windows.Forms.MessageBoxIcon.Error);
        }

        /// <summary>
        /// エラーメッセージを表示し、強制終了させます
        /// </summary>
        /// <param name="logText"></param>
        /// <returns></returns>
        public static void ErrorWriteAndExit(Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    string fileName = erDir + "Error" + DateTime.Today.ToString("yyMMddHHmm") + ".log";
                    using (var sw = new StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                    {
                        sw.WriteLine(DateTime.Now.ToString() + ",\r\n" + ex.Message + "\r\nStack:" + ex.StackTrace + "\r\n");
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }

            System.Windows.Forms.MessageBox.Show("致命的なエラーが発生しました。プログラムを終了します。\r\n\r\n" + ex.Message,
                "エラー", System.Windows.Forms.MessageBoxButtons.OK,
                System.Windows.Forms.MessageBoxIcon.Error);

            Environment.Exit(0);
        }

        /// <summary>
        /// 一般ログを指定されたファイルに書き出します
        /// </summary>
        /// <param name="fileName">(フルパスでない)ファイル名</param>
        /// <param name="info">ログ内容</param>
        public static void WriteInfoLog(string fileName, string info)
        {
            fileName = infoDir + fileName;
        }

    }
}
