﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Updater
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
#if DEBUG
#else
            // エラ―補足用イベント登録
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            Thread.GetDomain().UnhandledException += new UnhandledExceptionEventHandler(Application_UnhandledException);
#endif


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        // 未処理例外をキャッチするイベント・ハンドラ
        public static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            try
            {
                Common.WarningMsg("ご迷惑をおかけします。予期しないエラーが発生しました。システムを終了します。");
                Log.ErrorWrite(e.Exception);
            }
            finally
            {
                //アプリケーションを終了する
                Application.Exit();
            }
        }

        // 未処理例外をキャッチするイベント・ハンドラ
        public static void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            try
            {
                Common.WarningMsg("ご迷惑をおかけします。予期しないエラーが発生しました。システムを終了します。");
                if (ex != null) Log.ErrorWrite(ex);
            }
            finally
            {
                //アプリケーションを終了する
                Application.Exit();
            }
        }
    }
}