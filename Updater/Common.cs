﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace Updater
{
    class Common
    {
        public static System.Drawing.Color SelectCoror = System.Drawing.Color.FromArgb(183, 219, 255);
        public static System.Drawing.Color NoticeCoror = System.Drawing.Color.FromArgb(255, 255, 178);
        public static string ImgFolder;
        public const string MeesageAppName = "柔整閲覧システム";

        static Common()
        {
            try
            {
                var folName = Settings.ImageFolder;
                ImgFolder = folName == string.Empty ?
                    System.Windows.Forms.Application.StartupPath + "\\img" :
                    folName;
            }
            catch
            {
                //デザイナーのため
            }
        }

        public static void WarningMsg(string message)
        {
            System.Windows.Forms.MessageBox.Show(message, MeesageAppName,
                System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }

        public static void InfoMsg(string message)
        {
            System.Windows.Forms.MessageBox.Show(message, MeesageAppName,
                System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }

        public static bool OKCancelMsg(string message)
        {
            var res = System.Windows.Forms.MessageBox.Show(message, MeesageAppName,
                System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.Question);
            return res == System.Windows.Forms.DialogResult.OK;
        }

        public static bool OKCancelExMsg(string message)
        {
            var res = System.Windows.Forms.MessageBox.Show(message, MeesageAppName,
                System.Windows.Forms.MessageBoxButtons.OKCancel, System.Windows.Forms.MessageBoxIcon.Exclamation);
            return res == System.Windows.Forms.DialogResult.OK;
        }

        public static bool YesNoMsg(string message)
        {
            var res = System.Windows.Forms.MessageBox.Show(message, MeesageAppName,
                System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
            return res == System.Windows.Forms.DialogResult.Yes;
        }
    }
}
