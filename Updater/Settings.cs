﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Drawing;

namespace Updater
{
    public static class Settings
    {
        static string fileName = System.Windows.Forms.Application.StartupPath + "\\settings.xml";
        static XDocument xdoc = new XDocument();

        static Settings()
        {
            if (!System.IO.File.Exists(fileName)) create();
            xdoc = XDocument.Load(fileName);
        }

        static void create()
        {
            XDocument xdoc = new XDocument();
            xdoc.Add(new XElement("Settings"));
            xdoc.Save(fileName);
        }

        public static string PrinterName
        {
            get
            {
                if (xdoc.Element("Settings").Element("PrinterName") == null) return string.Empty;
                return xdoc.Element("Settings").Element("PrinterName").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("PrinterName", value);
            }
        }

        public static int PrintMargin
        {
            get
            {
                if (xdoc.Element("Settings").Element("PrintMargin") == null) return 0;
                return int.Parse(xdoc.Element("Settings").Element("PrintMargin").Value);
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("PrintMargin", value);
            }
        }

        public static string DBName
        {
            get
            {
                if (xdoc.Element("Settings").Element("DBName") == null) return string.Empty;
                return xdoc.Element("Settings").Element("DBName").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("DBName", value);
            }
        }

        public static string ImageFolder
        {
            get
            {
                if (xdoc.Element("Settings").Element("ImageFolder") == null) return string.Empty;
                return xdoc.Element("Settings").Element("ImageFolder").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("ImageFolder", value);
            }
        }

        public static string NameFont
        {
            get
            {
                if (xdoc.Element("Settings").Element("NameFont") == null) return string.Empty;
                return xdoc.Element("Settings").Element("NameFont").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("NameFont", value);
            }
        }

        public static Size WindowSize
        {
            get
            {
                if (xdoc.Element("Settings").Element("WindowSize") == null) return new Size(1400, 600);
                var pt = xdoc.Element("Settings").Element("WindowSize").Value.Split(',');
                return new Size(int.Parse(pt[0]), int.Parse(pt[1]));
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("WindowSize", $"{value.Width},{value.Height}");
            }
        }

        public static int ImageWidth
        {
            get
            {
                if (xdoc.Element("Settings").Element("ImageWidth") == null) return 620;
                return int.Parse(xdoc.Element("Settings").Element("ImageWidth").Value);
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("ImageWidth", value.ToString());
            }
        }

        public static bool FullWindow
        {
            get
            {
                if (xdoc.Element("Settings").Element("FullWindow") == null) return false;
                return xdoc.Element("Settings").Element("FullWindow").Value =="True";
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("FullWindow", value ? "True" : "False");
            }
        }

    }
}