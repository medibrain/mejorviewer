﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MejorViewer
{
    /// <summary>
    /// 表示設定画面
    /// </summary>
    public partial class ViewSelttingForm : Form
    {
        BindingSource bs = new BindingSource();

        public ViewSelttingForm()
        {
            InitializeComponent();

            var l = ViewSettings.dic.Values.ToList();
            l.Sort((x, y) => x.Index.CompareTo(y.Index));
            for (int i = 0; i < l.Count; i++) l[i].Index = i;
            bs.DataSource = l;
            ctGrid1.DataSource = bs;

            ctGrid1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            ctGrid1.Columns["PropertyName"].Visible = false;
            ctGrid1.Columns["Index"].HeaderText = "表示順";
            ctGrid1.Columns["Index"].Width = 80;
            ctGrid1.Columns["Header"].HeaderText = "項目";
            ctGrid1.Columns["Header"].Width = 130;
            ctGrid1.Columns["Visible"].HeaderText = "表示";
            ctGrid1.Columns["Visible"].Width = 70;
            ctGrid1.Columns["Width"].Visible = false;
            ctGrid1.Columns["Index"].DisplayIndex = 0;
            ctGrid1.Columns["Header"].DisplayIndex = 1;
            ctGrid1.Columns["Visible"].DisplayIndex = 2;
            ctGrid1.Columns["Index"].ReadOnly = true;
            ctGrid1.Columns["Header"].ReadOnly = true;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            ViewSettings.Load();
            this.Close();
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            if (ctGrid1.CurrentCell == null) return;
            var ri = ctGrid1.CurrentCell.RowIndex;
            if (ri <= 0) return;

            int index = (int)ctGrid1["Index", ri].Value;
            ctGrid1["Index", ri - 1].Value = index;
            ctGrid1["Index", ri].Value = index - 1;

            var l = (List<ViewSettings>)bs.DataSource;
            l.Sort((x, y) => x.Index.CompareTo(y.Index));
            bs.ResetBindings(false);
            ctGrid1.CurrentCell = ctGrid1["Index", ri - 1];
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            if (ctGrid1.CurrentCell == null) return;
            var ri = ctGrid1.CurrentCell.RowIndex;
            if (ri >= ctGrid1.RowCount - 1) return;

            int index = (int)ctGrid1["Index", ri].Value;
            ctGrid1["Index", ri + 1].Value = index;
            ctGrid1["Index", ri].Value = index + 1;

            var l = (List<ViewSettings>)bs.DataSource;
            l.Sort((x, y) => x.Index.CompareTo(y.Index));
            bs.ResetBindings(false);
            ctGrid1.CurrentCell = ctGrid1["Index", ri + 1];
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            ViewSettings.Save();
            this.Close();
        }
    }
}
