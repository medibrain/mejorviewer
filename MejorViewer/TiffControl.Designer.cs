﻿
partial class TiffControl
{
    /// <summary> 
    /// 必要なデザイナー変数です。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// 使用中のリソースをすべてクリーンアップします。
    /// </summary>
    /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region コンポーネント デザイナーで生成されたコード

    /// <summary> 
    /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
    /// コード エディターで変更しないでください。
    /// </summary>
    private void InitializeComponent()
    {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.buttonTiffPrev = new System.Windows.Forms.Button();
            this.buttonTiffNext = new System.Windows.Forms.Button();
            this.buttonShokai = new System.Windows.Forms.Button();
            this.textBoxPageno = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(0, 0);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(370, 271);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 2;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseClick);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // buttonTiffPrev
            // 
            this.buttonTiffPrev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTiffPrev.Location = new System.Drawing.Point(3, 3);
            this.buttonTiffPrev.Name = "buttonTiffPrev";
            this.buttonTiffPrev.Size = new System.Drawing.Size(24, 22);
            this.buttonTiffPrev.TabIndex = 7;
            this.buttonTiffPrev.TabStop = false;
            this.buttonTiffPrev.Text = "<";
            this.buttonTiffPrev.UseVisualStyleBackColor = true;
            this.buttonTiffPrev.Visible = false;
            this.buttonTiffPrev.Click += new System.EventHandler(this.buttonTiffPrev_Click);
            // 
            // buttonTiffNext
            // 
            this.buttonTiffNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTiffNext.Location = new System.Drawing.Point(29, 3);
            this.buttonTiffNext.Name = "buttonTiffNext";
            this.buttonTiffNext.Size = new System.Drawing.Size(24, 22);
            this.buttonTiffNext.TabIndex = 8;
            this.buttonTiffNext.TabStop = false;
            this.buttonTiffNext.Text = ">";
            this.buttonTiffNext.UseVisualStyleBackColor = true;
            this.buttonTiffNext.Visible = false;
            this.buttonTiffNext.Click += new System.EventHandler(this.buttonTiffNext_Click);
            // 
            // buttonShokai
            // 
            this.buttonShokai.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShokai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShokai.Location = new System.Drawing.Point(314, 3);
            this.buttonShokai.Name = "buttonShokai";
            this.buttonShokai.Size = new System.Drawing.Size(53, 22);
            this.buttonShokai.TabIndex = 8;
            this.buttonShokai.TabStop = false;
            this.buttonShokai.Text = "照会";
            this.buttonShokai.UseVisualStyleBackColor = true;
            this.buttonShokai.Visible = false;
            this.buttonShokai.Click += new System.EventHandler(this.buttonShokai_Click);
            // 
            // textBoxPageno
            // 
            this.textBoxPageno.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPageno.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxPageno.Location = new System.Drawing.Point(59, 7);
            this.textBoxPageno.Name = "textBoxPageno";
            this.textBoxPageno.Size = new System.Drawing.Size(50, 16);
            this.textBoxPageno.TabIndex = 9;
            this.textBoxPageno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TiffControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.textBoxPageno);
            this.Controls.Add(this.buttonTiffPrev);
            this.Controls.Add(this.buttonShokai);
            this.Controls.Add(this.buttonTiffNext);
            this.Controls.Add(this.pictureBox);
            this.Name = "TiffControl";
            this.Size = new System.Drawing.Size(370, 271);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pictureBox;
    private System.Windows.Forms.Button buttonTiffPrev;
    private System.Windows.Forms.Button buttonTiffNext;
    private System.Windows.Forms.Button buttonShokai;
    private System.Windows.Forms.TextBox textBoxPageno;
}
