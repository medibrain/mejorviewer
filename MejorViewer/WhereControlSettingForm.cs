﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MejorViewer
{
    /// <summary>
    /// 検索項目設定画面
    /// </summary>
    public partial class WhereControlSettingForm : Form
    {
        BindingSource bs = new BindingSource();

        public WhereControlSettingForm()
        {
            InitializeComponent();

            var l = WhereControlSetting.GetSettings();
            l.Sort((x, y) => x.Index.CompareTo(y.Index));
            for (int i = 0; i < l.Count; i++) l[i].Index = i;
            bs.DataSource = l;
            ctGrid1.DataSource = bs;

            ctGrid1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            ctGrid1.Columns[nameof(WhereControlSetting.Name)].Visible = false;
            ctGrid1.Columns[nameof(WhereControlSetting.Index)].HeaderText = "表示順";
            ctGrid1.Columns[nameof(WhereControlSetting.Index)].Width = 80;
            ctGrid1.Columns[nameof(WhereControlSetting.Text)].HeaderText = "項目";
            ctGrid1.Columns[nameof(WhereControlSetting.Text)].Width = 130;
            ctGrid1.Columns[nameof(WhereControlSetting.MainVisible)].HeaderText = "通常表示";
            ctGrid1.Columns[nameof(WhereControlSetting.MainVisible)].Width = 60;
            ctGrid1.Columns[nameof(WhereControlSetting.SubVisible)].HeaderText = "拡張表示";
            ctGrid1.Columns[nameof(WhereControlSetting.SubVisible)].Width = 60;
            ctGrid1.Columns[nameof(WhereControlSetting.Index)].DisplayIndex = 1;
            ctGrid1.Columns[nameof(WhereControlSetting.Text)].DisplayIndex = 2;
            ctGrid1.Columns[nameof(WhereControlSetting.MainVisible)].DisplayIndex = 3;
            ctGrid1.Columns[nameof(WhereControlSetting.SubVisible)].DisplayIndex = 4;
            ctGrid1.Columns[nameof(WhereControlSetting.Text)].ReadOnly = true;
            ctGrid1.Columns[nameof(WhereControlSetting.Index)].ReadOnly = true;
            ctGrid1.CurrentCellDirtyStateChanged += CtGrid1_CurrentCellDirtyStateChanged;
        }

        private void CtGrid1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            ctGrid1.EndEdit();
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            if (ctGrid1.CurrentCell == null) return;
            var ri = ctGrid1.CurrentCell.RowIndex;
            if (ri <= 0) return;

            int index = (int)ctGrid1["Index", ri].Value;
            ctGrid1["Index", ri - 1].Value = index;
            ctGrid1["Index", ri].Value = index - 1;

            var l = (List<WhereControlSetting>)bs.DataSource;
            l.Sort((x, y) => x.Index.CompareTo(y.Index));
            bs.ResetBindings(false);
            ctGrid1.CurrentCell = ctGrid1["Index", ri - 1];
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            if (ctGrid1.CurrentCell == null) return;
            var ri = ctGrid1.CurrentCell.RowIndex;
            if (ri >= ctGrid1.RowCount - 1) return;

            int index = (int)ctGrid1["Index", ri].Value;
            ctGrid1["Index", ri + 1].Value = index;
            ctGrid1["Index", ri].Value = index + 1;

            var l = (List<WhereControlSetting>)bs.DataSource;
            l.Sort((x, y) => x.Index.CompareTo(y.Index));
            bs.ResetBindings(false);
            ctGrid1.CurrentCell = ctGrid1["Index", ri + 1];

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            WhereControlSetting.Save();
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            WhereControlSetting.Load();
            this.Close();
        }
    }
}
