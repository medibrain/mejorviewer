﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Permissions;
using System.ComponentModel;

public class NumBox : TextBox
{
    const int WM_PASTE = 0x302;
    bool copyPaste = false;

    [Category("動作")]
    [DefaultValue(false)]
    [Description("マイナス(ハイフン)を入力できるかどうかを取得または設定します。")]
    public bool Hyphen { get; set; }

    [Category("動作")]
    [DefaultValue(false)]
    [Description("スペースを入力できるかどうかを取得または設定します。")]
    public bool Space { get; set; }

    public NumBox()
    {
        this.ImeMode = System.Windows.Forms.ImeMode.Disable;
        this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumberBox_KeyPress);
        this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumBox_KeyDown);
        this.Hyphen = false;
        this.Space = false;
    }

    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    protected override void WndProc(ref Message m)
    {
        if (m.Msg == WM_PASTE)
        {
            IDataObject iData = Clipboard.GetDataObject();
            //文字列がクリップボードにあるか
            if (iData != null && iData.GetDataPresent(DataFormats.Text))
            {
                string clipStr = (string)iData.GetData(DataFormats.Text);
                //クリップボードの文字列が数字か調べる
                foreach (var c in clipStr)
                {
                    if ((c < '0' || c > '9') && c != '\b' && c != '\r' && c != '\n' && (!Hyphen || c != '-') && (!Space || c != ' '))
                    {
                        return;
                    }
                }
                //if (!System.Text.RegularExpressions.Regex.IsMatch(clipStr, @"^[0-9]+$"))
                //    return;
            }
        }

        base.WndProc(ref m);
    }

    private void NumberBox_KeyPress(object sender, KeyPressEventArgs e)
    {
        if ((e.KeyChar < '0' || e.KeyChar > '9')
            && e.KeyChar != '\b'
            && (!Hyphen || e.KeyChar != '-')
            && (!Space || e.KeyChar != ' ')
            && !copyPaste)
            e.Handled = true;
        copyPaste = false;
    }

    private void InitializeComponent()
    {
        this.SuspendLayout();
        // 
        // NumBox
        // 
        this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumBox_KeyDown);
        this.ResumeLayout(false);

    }

    private void NumBox_KeyDown(object sender, KeyEventArgs e)
    {
        if ((e.KeyCode == Keys.V && e.Control) ||
            (e.KeyCode == Keys.C && e.Control) ||
            (e.KeyCode == Keys.X && e.Control))
        {
            copyPaste = true;
        }
        else
        {
            copyPaste = false;
        }
    }

    /// <summary>
    /// 失敗した場合、0が返ります。
    /// </summary>
    /// <returns></returns>
    public int GetIntValue()
    {
        int i;
        int.TryParse(Text, out i);
        return i;
    }

    /// <summary>
    /// カンマ付きテキストから数値を取得します。失敗した場合、0が返ります。
    /// </summary>
    /// <returns></returns>
    public int GetIntValueDot()
    {
        int i;
        int.TryParse(Text.Replace(",", ""), out i);
        return i;
    }

    /// <summary>
    /// カンマ付きテキストから数値を取得します。失敗した場合、0が返ります。
    /// </summary>
    /// <returns></returns>
    public bool TryGetIntValueDot(out int i)
    {
        return int.TryParse(Text.Replace(",", ""), out i);
    }

    /// <summary>
    /// 失敗した場合、0が返ります。
    /// </summary>
    /// <returns></returns>
    public short GetShortValue()
    {
        short s;
        short.TryParse(Text, out s);
        return s;
    }

    /// <summary>
    /// 失敗した場合、nullが返ります。
    /// </summary>
    /// <returns></returns>
    public int? TryGetIntValue()
    {
        int i;
        if (!int.TryParse(Text, out i)) return null;
        return i;
    }

    /// <summary>
    /// 失敗した場合、nullが返ります。
    /// </summary>
    /// <returns></returns>
    public short? TryGetShortValue()
    {
        short s;
        if (!short.TryParse(Text, out s)) return null;
        return s;
    }
}