﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorViewer
{
    class ShokaiUpdate
    {
        public int AID { get; set; }
        public string ShokaiID { get; set; } = string.Empty;
        public ShokaiReason ShokaiReason { get; set; } = ShokaiReason.なし;
        public ShokaiResult ShokaiResult { get; set; } = ShokaiResult.なし;
        //public ShokaiStatus ShokaiStatus { get; set; } = ShokaiStatus.なし;
        public KagoReason KagoReason { get; set; } = KagoReason.なし;
        public SaishinsaReason SaishinsaReason { get; set; } = SaishinsaReason.なし;
        public string Note { get; set; } = string.Empty;
        public string ShokaiFile { get; set; } = string.Empty;
        public bool HenreiFlag { get; set; } = false;

        public static bool Update(string csvName)
        {
            var wf = new WaitForm();
            Task.Factory.StartNew(() => wf.ShowDialog());
            while (!wf.Visible) System.Threading.Thread.Sleep(10);

            var csv = Common.ImportUTF8(csvName);

            var sql = "UPDATE rece SET " +
                "ShokaiID=@ShokaiID, ShokaiReason=@ShokaiReason, ShokaiResult=@ShokaiResult, " +
                "ShokaiStatus=@ShokaiStatus, KagoReason=@KagoReason, " +
                "SaishinsaReason=@SaishinsaReason, Note=@Note, ShokaiFile=@ShokaiFile, " +
                "HenreiFlag=@HenreiFlag " +
                "WHERE aid=@aid;";

            var header = csv[0];
            var indexes = new Dictionary<string, int>();
            for (int i = 0; i < header.Length; i++) indexes.Add(header[i], i);
            var getValue = new Func<string[], string, string>((line, name) =>
                indexes.ContainsKey(name) ? line[indexes[name]] : null);

            var l = new List<ShokaiUpdate>();
            for (int i = 1; i < csv.Count; i++)
            {
                var line = csv[i];
                var s = new ShokaiUpdate();
                s.AID = int.Parse(getValue(line, nameof(AID)) ?? "0");
                s.ShokaiID = getValue(line, nameof(ShokaiID)) ?? string.Empty;
                s.ShokaiReason = (ShokaiReason)int.Parse(getValue(line, nameof(ShokaiReason)) ?? "0");
                s.ShokaiResult = (ShokaiResult)int.Parse(getValue(line, nameof(ShokaiResult)) ?? "0");
                //s.ShokaiStatus = (ShokaiStatus)int.Parse(getValue(line, nameof(ShokaiStatus)) ?? "0");
                s.Note = getValue(line, nameof(Note)) ?? string.Empty;
                s.ShokaiFile = getValue(line, nameof(ShokaiFile)) ?? string.Empty;
                s.HenreiFlag = (getValue(line, nameof(HenreiFlag)) ?? string.Empty) == "True";
                l.Add(s);
            }

            using (var tran = DB.Main.CreateTran())
            {
                wf.LogPrint("照会データの更新中です…");
                if (!DB.Main.Excute(sql, l, tran))
                {
                    tran.Rollback();
                    return false;
                }

                //画像コピー
                wf.LogPrint("照会画像のコピー中です…");
                var sorceDir = System.IO.Path.GetDirectoryName(csvName) + "\\ShokaiImg";
                var imgDir = Common.ImgFolder + "\\s";
                int imgCount = 0;

                try
                {
                    System.IO.Directory.CreateDirectory(imgDir);
                    var fc = new FastCopy();
                    var fs = System.IO.Directory.GetFiles(sorceDir);
                    wf.InvokeMax = fs.Count();
                    wf.InvokeValue = 0;
                    wf.InvokeBarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    foreach (var item in fs)
                    {
                        if (wf.Cancel)
                        {
                            var res = System.Windows.Forms.MessageBox.Show("データの取り込みは完了しています。インポートデータを残しますか？",
                                "データ確認", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                            if (res == System.Windows.Forms.DialogResult.Yes)
                            {
                                tran.Commit();
                                System.Windows.Forms.MessageBox.Show("画像はインポートしきれていません。ご注意ください。");
                                return false;
                            }
                            else
                            {
                                tran.Rollback();
                                return false;
                            }
                        }

                        var fileaName = imgDir + "\\" + System.IO.Path.GetFileName(item);
                        fc.FileCopy(item, fileaName);

                        imgCount++;
                        wf.InvokeValue++;
                        if (imgCount % 5000 == 0) wf.LogPrint(imgCount.ToString() + "件の画像コピーが終了しました");
                    }
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    Log.ErrorWrite(ex);
                    return false;
                }
                finally
                {
                    wf.InvokeCloseDispose();
                }

                tran.Commit();
                return true;
            }
        }
    }
}
