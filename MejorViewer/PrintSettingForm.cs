﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace MejorViewer
{
    public partial class PrintSettingForm : Form
    {
        public PrintSettingForm()
        {
            InitializeComponent();

            string defPrinterName;
            using (var pd = new PrintDocument())
            {
                defPrinterName = pd.PrinterSettings.PrinterName;
            }

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                comboBox1.Items.Add(PrinterSettings.InstalledPrinters[i]);
                if (PrinterSettings.InstalledPrinters[i] == defPrinterName)
                    comboBox1.SelectedIndex = i;
            }

            var pn = Settings.PrinterName;
            if (comboBox1.Items.Contains(pn)) comboBox1.SelectedItem = pn;

            //20221201 ito st 設定項目追加
            textBoxPrintMargin.Text = Settings.PrintMargin.ToString();
            checkBoxDoublePage.Checked = Settings.DoublePageBlank;
            textBoxImageFolder.Text = Settings.ImageFolder;
            textBoxOldSystem.Text = Settings.ImageFolderOldSystem;
            checkBoxMemoColor.Checked = Settings.MemoColorVisible;
            textBoxInsurer.Text = Settings.InsulerSetting.ToString();
            //20221201 ito end 設定項目追加
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Settings.PrinterName = (string)comboBox1.SelectedItem;

            //20221201 ito st 設定項目追加
            Settings.PrintMargin = int.Parse(textBoxPrintMargin.Text.Trim());
            Settings.DoublePageBlank = checkBoxDoublePage.Checked;
            Settings.ImageFolder = textBoxImageFolder.Text.Trim();
            Settings.ImageFolderOldSystem = textBoxOldSystem.Text.Trim();
            Settings.MemoColorVisible = checkBoxMemoColor.Checked;
            Settings.InsulerSetting = int.Parse(textBoxInsurer.Text.Trim());
            //20221201 ito end 設定項目追加

            Settings.Save();
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
