﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MejorViewer
{
    /// <summary>
    /// 表示設定の設定内容
    /// </summary>
    class ViewSettings
    {
        public static Dictionary<string, ViewSettings> dic = new Dictionary<string, ViewSettings>();

        public string PropertyName { get; set; }
        public bool Visible { get; set; }
        public string Header{get;set;}
        public int Index { get; set; }
        public int Width { get; set; }

        ViewSettings(string name, string header)
        {
            this.PropertyName = name;
            this.Header = header;
            this.Index = dic.Count;
            this.Visible = true;
            this.Width = 100;
            dic.Add(name, this);
        }

        static ViewSettings()
        {
            new ViewSettings(nameof(ReceView.Select), "選択");
            new ViewSettings(nameof(ReceView.ImportID), "IID");
            new ViewSettings(nameof(ReceView.AID), "AID");
            new ViewSettings(nameof(ReceView.RrID), "RrID");
            new ViewSettings(nameof(ReceView.CYM), "処理年月(西");
            new ViewSettings(nameof(ReceView.JpCYM), "処理年月");
            new ViewSettings(nameof(ReceView.YM), "診療年月(西");
            new ViewSettings(nameof(ReceView.JpYM), "診療年月");
            new ViewSettings(nameof(ReceView.AppType), "種類");
            new ViewSettings(nameof(ReceView.InsNum), "保険者番号");
            new ViewSettings(nameof(ReceView.Num), "被保番");
            new ViewSettings(nameof(ReceView.Name), "氏名");
            new ViewSettings(nameof(ReceView.Kana), "カナ");
            new ViewSettings(nameof(ReceView.Sex), "性別");
            new ViewSettings(nameof(ReceView.Birth), "生年月日(西");
            new ViewSettings(nameof(ReceView.JpBirth), "生年月日");
            new ViewSettings(nameof(ReceView.ShokenDate), "初検日(西");
            new ViewSettings(nameof(ReceView.JpShokenDate), "初検日");
            new ViewSettings(nameof(ReceView.StartDate), "開始日(西");
            new ViewSettings(nameof(ReceView.JpStartDate), "開始日");
            new ViewSettings(nameof(ReceView.FinishDate), "終了日(西");
            new ViewSettings(nameof(ReceView.JpFinishDate), "終了日");
            new ViewSettings(nameof(ReceView.Family), "家族");
            new ViewSettings(nameof(ReceView.Fusho1), "負傷1");
            new ViewSettings(nameof(ReceView.Fusho2), "負傷2");
            new ViewSettings(nameof(ReceView.Fusho3), "負傷3");
            new ViewSettings(nameof(ReceView.Fusho4), "負傷4");
            new ViewSettings(nameof(ReceView.Fusho5), "負傷5");
            new ViewSettings(nameof(ReceView.FushoCount), "負傷数");
            new ViewSettings(nameof(ReceView.AnmaCount), "あんま施術数");
            new ViewSettings(nameof(ReceView.AnmaBody), "体幹");
            new ViewSettings(nameof(ReceView.AnmaRightUpper), "右上");
            new ViewSettings(nameof(ReceView.AnmaLeftUpper), "左上");
            new ViewSettings(nameof(ReceView.AnmaRightLower), "右下");
            new ViewSettings(nameof(ReceView.AnmaLeftLower), "左下");
            new ViewSettings(nameof(ReceView.NewCont), "新規/継続");
            new ViewSettings(nameof(ReceView.Total), "合計");
            new ViewSettings(nameof(ReceView.Ratio), "負担割合");
            new ViewSettings(nameof(ReceView.Partial), "負担額");
            new ViewSettings(nameof(ReceView.Charge), "請求額");
            new ViewSettings(nameof(ReceView.Days), "日数");
            new ViewSettings(nameof(ReceView.VisitFee), "往療");
            new ViewSettings(nameof(ReceView.DouiDate), "同意日(西");
            new ViewSettings(nameof(ReceView.JpDouiDate), "同意日");

            new ViewSettings(nameof(ReceView.DrNum), "施術師コード");
            new ViewSettings(nameof(ReceView.DrName), "施術師名");
            new ViewSettings(nameof(ReceView.ClinicNum), "施術所コード");
            new ViewSettings(nameof(ReceView.ClinicName), "施術所名");
            new ViewSettings(nameof(ReceView.GroupNum), "グループコード");
            new ViewSettings(nameof(ReceView.GroupName), "グループ名");
            new ViewSettings(nameof(ReceView.Zip), "〒");
            new ViewSettings(nameof(ReceView.Adds), "住所");
            new ViewSettings(nameof(ReceView.DestName), "送付先氏名");
            new ViewSettings(nameof(ReceView.DestKana), "送付先カナ");
            new ViewSettings(nameof(ReceView.DestZip), "送付〒");
            new ViewSettings(nameof(ReceView.DestAdds), "送付住所");
            new ViewSettings(nameof(ReceView.Numbering), "ナンバリング");
            new ViewSettings(nameof(ReceView.ComNum), "電算番号");
            new ViewSettings(nameof(ReceView.ImageFile), "ファイル名");

            new ViewSettings(nameof(ReceView.ShokaiID), "照会ID");
            new ViewSettings(nameof(ReceView.ShokaiReason), "照会理由");
            new ViewSettings(nameof(ReceView.ShokaiResult), "照会結果");
            new ViewSettings(nameof(ReceView.KagoReasons), "過誤理由");
            new ViewSettings(nameof(ReceView.SaishinsaReasons), "再審査理由");
            new ViewSettings(nameof(ReceView.Henrei), "返戻");
            new ViewSettings(nameof(ReceView.HenreiReasons), "返戻理由");
            new ViewSettings(nameof(ReceView.Note), "メモ");
            new ViewSettings(nameof(ReceView.ShokaiFile), "照会ファイル名");
            new ViewSettings(nameof(ReceView.ImageFullPath), "ファイルパス");
            new ViewSettings(nameof(ReceView.ShokaiImageFullPath), "照会ファイルパス");
            new ViewSettings(nameof(ReceView.HasDouisyo), "同意書");
            new ViewSettings(nameof(ReceView.HasSejutsuHoukokusyo), "施術報告書");
            new ViewSettings(nameof(ReceView.HasJoutaikinyusyo), "状態記入書");
            new ViewSettings(nameof(ReceView.ViewerMemo), "ビューア内メモ");

            //20221101 ito st 兵庫広域対応
            new ViewSettings(nameof(ReceView.ViewerMemoColor), "ビューア内メモ色");
            new ViewSettings(nameof(ReceView.Fushomei), "あはき負傷名");
            //20221101 ito end 兵庫広域対応

            Load();
        }

        static string fileName = System.Windows.Forms.Application.StartupPath + "\\ViewSetting";

        public static void Save()
        {
            using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
            {
                var l = dic.Values.ToList();
                l.Sort((x, y) => x.Index.CompareTo(y.Index));

                foreach (var item in l)
                {
                    var s = new string[5];
                    s[0] = item.Index.ToString();
                    s[1] = item.PropertyName;
                    s[2] = item.Header;
                    s[3] = item.Visible.ToString();
                    s[4] = item.Width.ToString();
                    sw.WriteLine(string.Join(",", s));
                }
            }
        }

        public static void Load()
        {
            if (!System.IO.File.Exists(fileName)) return;
            using (var sr = new System.IO.StreamReader(fileName, Encoding.GetEncoding("Shift_JIS")))
            {
                while (sr.Peek() > 0)
                {
                    var s = sr.ReadLine().Split(',');
                    if (s.Length < 3) continue;
                    var name = s[1];
                    if (!dic.ContainsKey(name)) continue;
                    dic[name].Index = int.Parse(s[0]); ;
                    dic[name].Visible = bool.Parse(s[3]);
                    dic[name].Width = int.Parse(s[4]);
                }
            }
        }

        public static void FromSetting(DataGridView gv)
        {
            var l = dic.Values.ToList();
            l.Sort((x, y) => x.Index.CompareTo(y.Index));

            foreach (var item in l)
            {
                gv.Columns[item.PropertyName].HeaderText = item.Header;
                gv.Columns[item.PropertyName].Visible = item.Visible;
                gv.Columns[item.PropertyName].DisplayIndex = item.Index;
                gv.Columns[item.PropertyName].Width = item.Width;
                gv.Columns[item.PropertyName].ReadOnly = true;
            }

            gv.Columns["Select"].ReadOnly = false;
        }

        public static void WidthToSetting(DataGridView gv)
        {
            foreach (DataGridViewColumn item in gv.Columns)
            {
                try
                {
                    dic[item.DataPropertyName].Width = item.Width;
                }
                catch
                {
                    continue;
                }
            }
        }
    
    }
}
