﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace MejorViewer
{
    /// <summary>
    /// 縦スクロールバー常に表示　Flex3Dのみ！
    /// </summary>
    public class CtGrid : DataGridView
    {
        VScrollBar vs = new VScrollBar();

        public CtGrid()
        {
            //縦スクロールバーを表示
            vs.Enabled = false;
            this.VerticalScrollBar.VisibleChanged += ShowScrollBars;
            this.Layout += ShowScrollBars;
            this.Controls.Add(vs);
            this.ColumnHeaderMouseClick += CtGrid_ColumnHeaderMouseClick;

            this.BackgroundColor = System.Drawing.SystemColors.Window;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DoubleBuffered = true;

            this.DefaultCellStyle.SelectionForeColor = SystemColors.WindowText;
            this.DefaultCellStyle.SelectionBackColor = Common.SelectCoror;
            this.RowHeadersVisible = false;
            this.AllowUserToAddRows = false;
            //this.AllowUserToResizeColumns = false;
            this.AllowUserToResizeRows = false;
            this.AllowUserToOrderColumns = false;
        }

        string sortCellName;
        bool sortASC = true;
        private void CtGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (DataSource == null || !(DataSource is BindingSource)) return;
            if (e.ColumnIndex < 0) return;

            var bs = (BindingSource)DataSource;
            var name = Columns[e.ColumnIndex].Name;
            if (name == sortCellName) sortASC = !sortASC;
            else sortASC = true;
            sortCellName = name;

            var ps = typeof(ReceView).GetProperties();
            var p = Array.Find(ps, pp => pp.Name == name);
            if (p == null) return;
            
            ((List<ReceView>)(bs.DataSource)).Sort((x, y) =>
            {
                var xp = p.GetValue(x);
                var yp = p.GetValue(y);

                //nullを最小に
                if (xp == null && yp == null) return 0;
                if (xp == null) return sortASC ? -1 : 1;
                if (yp == null) return sortASC ? 1 : -1;
                
                int compareRes = 0;
                if (p.PropertyType == typeof(int?)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(int)) compareRes = ((int)xp).CompareTo(((int)yp));
                else if (p.PropertyType == typeof(DateTime)) compareRes = ((DateTime)xp).CompareTo(((DateTime)yp));
                else if (p.PropertyType == typeof(string)) compareRes = ((string)xp).CompareTo(((string)yp));
                else compareRes = (xp.ToString()).CompareTo(yp.ToString());

                return sortASC ? compareRes : -compareRes;
            });

            bs.ResetBindings(false);
        }

        private void ShowScrollBars(object sender, EventArgs e)
        {
            if (this.VerticalScrollBar.Visible)
            {
                vs.Visible = false;
            }
            else
            {
                if (this.BorderStyle == System.Windows.Forms.BorderStyle.None)
                {
                    int width = vs.Width;
                    vs.Location = new System.Drawing.Point(this.ClientRectangle.Width - width, 0);
                    vs.Size = new System.Drawing.Size(width, this.ClientRectangle.Height);
                }
                else
                {
                    int width = vs.Width;
                    vs.Location = new System.Drawing.Point(this.ClientRectangle.Width - width - 1, 1);
                    vs.Size = new System.Drawing.Size(width, this.ClientRectangle.Height - 2);
                }
                vs.Visible = true;
            }
        }

        public int VerticalScrollBarWidth
        {
            get
            {
                return this.VerticalScrollBar.Width;
            }
        }
    }
}
