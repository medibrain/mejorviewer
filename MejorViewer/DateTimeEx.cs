﻿using System;
using System.Collections.Generic;

public static class DateTimeEx
{
    public static DateTime DateTimeNull = DateTime.MinValue;
    public static string ErrorMessage = string.Empty;
    public static string EraString { get; private set; }
    static List<EraJ> EraList = new List<EraJ>();
    public static System.Globalization.CultureInfo culture;

    static DateTimeEx()
    {
        culture = new System.Globalization.CultureInfo("ja-JP", true);
        culture.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();
        GetEraList();
    }

    internal class EraJ
    {
        internal DateTime SDate = DateTime.MaxValue;
        internal DateTime EDate = DateTime.MaxValue;
        internal string Name = string.Empty;
        internal string ShortName = string.Empty;
        internal string Initial = string.Empty;
        internal int Number = 0;
        internal int Difference = 0;
    }

    /// <summary>
    /// 和暦年号一覧を指定します
    /// </summary>
    /// <returns></returns>
    private static void GetEraList()
    {
        var m = new EraJ();
        m.SDate = new DateTime(1868, 9, 8);
        m.EDate = new DateTime(1912, 7, 29);
        m.Name = "明治";
        m.ShortName = "明";
        m.Number = 1;
        m.Initial = "M";
        m.Difference = 1867;
        EraList.Add(m);

        var t = new EraJ();
        t.SDate = new DateTime(1912, 7, 30);
        t.EDate = new DateTime(1926, 12, 24);
        t.Name = "大正";
        t.ShortName = "大";
        t.Number = 2;
        t.Initial = "T";
        t.Difference = 1911;
        EraList.Add(t);

        var s = new EraJ();
        s.SDate = new DateTime(1926, 12, 25);
        s.EDate = new DateTime(1989, 1, 7);
        s.Name = "昭和";
        s.ShortName = "昭";
        s.Number = 3;
        s.Initial = "S";
        s.Difference = 1925;
        EraList.Add(s);

        var h = new EraJ();
        h.SDate = new DateTime(1989, 1, 8);
        h.EDate = new DateTime(2019, 4, 30);
        h.Name = "平成";
        h.ShortName = "平";
        h.Number = 4;
        h.Initial = "H";
        h.Difference = 1988;
        EraList.Add(h);

        //令和追加
        var r = new EraJ();
        r.SDate = new DateTime(2019, 5, 1);
        r.EDate = DateTime.MaxValue;
        r.Name = "令和";
        r.ShortName = "令";
        r.Number = 5;
        r.Initial = "R";
        r.Difference = 2018;
        EraList.Add(r);

        //年号を表す文字列を整備
        foreach (var item in EraList)
        {
            EraString += item.ShortName;
            EraString += item.Initial;
            EraString += item.Initial.ToLower();
        }
    }

    /// <summary>
    /// 対応する和暦の年を返します。なかった場合、0が返ります。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return dt.Year - EraList[i].Difference;
        }

        return 0;
    }

    /// <summary>
    /// 対応する和暦の年を英頭文字付きで返します。なかった場合、0が返ります。
    /// </summary>
    public static string GetShortEraJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Initial + (dt.Year - EraList[i].Difference).ToString("00");
        }

        return string.Empty;
    }

    /// <summary>
    /// 正しい日付かどうかチェックします。
    /// </summary>
    public static bool IsDate(int iYear, int iMonth, int iDay)
    {
        if ((DateTime.MinValue.Year > iYear) || (iYear > DateTime.MaxValue.Year))
        {
            return false;
        }

        if ((DateTime.MinValue.Month > iMonth) || (iMonth > DateTime.MaxValue.Month))
        {
            return false;
        }

        int iLastDay = DateTime.DaysInMonth(iYear, iMonth);

        if ((DateTime.MinValue.Day > iDay) || (iDay > iLastDay))
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// 和暦頭文字で表示される日付文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetShortJpDateStr(int intDt)
    {
        var dt = intDt.ToDateTime();
        if (dt.IsNullDate()) return string.Empty;
        var year = GetShortEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");
    }

    /// <summary>
    /// 元号を除く和暦日付文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetShortJpDateStrWithoutEra(int intDt)
    {
        var dt = intDt.ToDateTime();
        if (dt.IsNullDate()) return string.Empty;
        var year = GetShortEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");
    }

    /// <summary>
    /// 数字8ケタからなる西暦をDateTimeに変換します。
    /// </summary>
    /// <param name="dateInt8">8桁のint</param>
    /// <returns></returns>
    public static DateTime ToDateTime(this int dateInt8)
    {
        int year = dateInt8 / 10000;
        int month = (dateInt8 % 10000) / 100;
        int day = dateInt8 % 100;
        if (!IsDate(year, month, day)) return DateTimeNull;
        return new DateTime(year, month, day);
    }

    /// <summary>
    /// 数字8ケタのIntに変換します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int ToInt(this DateTime dt)
    {
        int di = dt.Day;
        di = di + dt.Month * 100;
        di = di + dt.Year * 10000;
        return di;
    }

    /// <summary>
    /// 現在挿入されている日付がNULLかどうかを返します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static bool IsNullDate(this DateTime dt)
    {
        return dt == DateTimeNull;
    }

    /// <summary>
    /// 西暦年月から和暦年月を取得します
    /// </summary>
    public static int GetYymmFromAdYM(int adYearMonth)
    {
        var y = adYearMonth / 100;
        var m = adYearMonth % 100;
        var dt = new DateTime(y, m, 1);
        //var g = GetEraNumber(dt);
        var jy = GetJpYear(dt);

        return jy * 100 + m;
    }

}
