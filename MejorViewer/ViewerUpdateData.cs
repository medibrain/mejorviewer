﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorViewer
{
    [DB.DbAttribute.DifferentTableName("Rece2")]
    class ViewerUpdateData
    {
        [DB.DbAttribute.PrimaryKey]
        public int AID { get; set; }
        public int ImportID { get; set; }

        public string ShokaiID { get; set; } = string.Empty;
        public ShokaiReason ShokaiReasons { get; set; } = ShokaiReason.なし;
        public ShokaiResult ShokaiResult { get; set; } = ShokaiResult.なし;
        public KagoReason KagoReasons { get; set; } = KagoReason.なし;
        public SaishinsaReason SaishinsaReasons { get; set; } = SaishinsaReason.なし;
        public bool HenreiFlag { get; set; } = false;
        public HenreiReason HenreiReasons { get; set; } = HenreiReason.なし;
        public string Note { get; set; } = string.Empty;
        public string ShokaiFile { get; set; } = string.Empty;
        [DB.DbAttribute.Ignore]
        public string ShokaiImageFullPath => 
            $"{Common.ImgFolder}\\{ImportID}\\{ShokaiFile}";


        public static bool Update(string csvName)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();

                var csv = Common.ImportUTF8(csvName);

                var header = csv[0];
                var indexes = new Dictionary<string, int>();
                for (int i = 0; i < header.Length; i++) indexes.Add(header[i], i);
                var getValue = new Func<string[], string, string>((line, name) =>
                    indexes.ContainsKey(name) ? line[indexes[name]] : null);

                var l = new List<ViewerUpdateData>();
                for (int i = 1; i < csv.Count; i++)
                {
                    var line = csv[i];
                    var aid = int.Parse(getValue(line, nameof(AID)) ?? "0");

                    var vud = DB.Main.Select<ViewerUpdateData>(new { aid }).SingleOrDefault();
                    if (vud == null) continue;
                    
                    vud.ShokaiID = getValue(line, nameof(ShokaiID)) ?? string.Empty;
                    vud.ShokaiReasons = (ShokaiReason)int.Parse(getValue(line, nameof(ShokaiReasons)) ?? "0");
                    vud.ShokaiResult = (ShokaiResult)int.Parse(getValue(line, nameof(ShokaiResult)) ?? "0");
                    vud.KagoReasons = (KagoReason)int.Parse(getValue(line, nameof(KagoReasons)) ?? "0");
                    vud.SaishinsaReasons = (SaishinsaReason)int.Parse(getValue(line, nameof(SaishinsaReasons)) ?? "0");
                    vud.HenreiFlag = (getValue(line, nameof(HenreiFlag)) ?? string.Empty) == "True";
                    vud.HenreiReasons = (HenreiReason)int.Parse(getValue(line, nameof(HenreiReasons)) ?? "0");
                    vud.Note = getValue(line, nameof(Note)) ?? string.Empty;
                    vud.ShokaiFile = getValue(line, nameof(ShokaiFile)) ?? string.Empty;
                    l.Add(vud);
                }

                using (var tran = DB.Main.CreateTran())
                {
                    wf.LogPrint("照会データの更新中です…");
                    if (!DB.Main.Updates(l, tran)) return false;

                    //画像コピー
                    wf.LogPrint("照会画像のコピー中です…");
                    var sorceDir = System.IO.Path.GetDirectoryName(csvName) + "\\ShokaiImg\\";
                    int imgCount = 0;

                    try
                    {
                        var fc = new FastCopy();
                        wf.InvokeMax = l.Count;
                        wf.InvokeValue = 0;
                        wf.InvokeBarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                        foreach (var item in l)
                        {
                            if (wf.Cancel)
                            {
                                var res = System.Windows.Forms.MessageBox.Show("データの取り込みは完了しています。インポートデータを残しますか？",
                                    "データ確認", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                                if (res == System.Windows.Forms.DialogResult.Yes)
                                {
                                    tran.Commit();
                                    System.Windows.Forms.MessageBox.Show("画像はインポートしきれていません。ご注意ください。");
                                    return false;
                                }
                                else
                                {
                                    tran.Rollback();
                                    return false;
                                }
                            }

                            var sorce = sorceDir + item.ShokaiFile;
                            fc.FileCopy(sorce, item.ShokaiImageFullPath);

                            imgCount++;
                            wf.InvokeValue++;
                            if (imgCount % 5000 == 0) wf.LogPrint(imgCount.ToString() + "件の画像コピーが終了しました");
                        }
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        Log.ErrorWrite(ex);
                        return false;
                    }
                    finally
                    {
                        wf.InvokeCloseDispose();
                    }

                    tran.Commit();
                    return true;
                }
            }
        }
    }
}
