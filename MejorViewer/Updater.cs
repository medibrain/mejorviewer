﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace MejorViewer
{
    class Updater
    {
        public static bool UpdateCheck(string dir)
        {
            var dirFn = dir + "\\Update\\MejorViewer";
            var thisFn = System.Windows.Forms.Application.StartupPath + "\\MejorViewer.exe";
            if (!System.IO.File.Exists(dirFn)) return false;

            var dirVer = System.Diagnostics.FileVersionInfo.GetVersionInfo(dirFn).ProductVersion.Split('.');
            var thisVer = System.Diagnostics.FileVersionInfo.GetVersionInfo(thisFn).ProductVersion.Split('.');

            for (int i = 0; i < 4; i++)
            {
                if (int.Parse(dirVer[i]) < int.Parse(thisVer[i])) return false;
                if (int.Parse(dirVer[i]) > int.Parse(thisVer[i]))
                {
                    var res = MessageBox.Show("新しいバージョンが同封されています。アップデートしてもよろしいですか？",
                        "アップデート確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    return res == DialogResult.Yes;
                }
            }
            return false;
        }

        public static bool Update(string dir)
        {
            return false;
        }
    }
}
