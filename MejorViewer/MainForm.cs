﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MejorViewer
{
    /// <summary>
    /// メイン画面
    /// </summary>
    public partial class MainForm : Form
    {
        BindingSource bs = new BindingSource();
        DataGridViewCellStyle nameFontStyle;

        public MainForm()
        {
            InitializeComponent();
            bs.CurrentChanged += Bs_CurrentChanged;

            nameFontStyle = ctGrid1.DefaultCellStyle.Clone();
            nameFontStyle.Font = new Font(Settings.NameFont, 9);
            ctGrid1.DefaultCellStyle = nameFontStyle;

            bs.DataSource = new List<ReceView>();
            ctGrid1.DataSource = bs;
            ViewSettings.FromSetting(ctGrid1);
            tiffControl1.ShokaiClick += TiffControl1_ShokaiClick;

            panelMemo.Visible = Settings.MemoVisible;
            //whereControl1.Adjust();

            //20221101 ito st 付箋色対応
            if (Settings.MemoColorVisible == true)
            {
                buttonMemoColor.Visible = true;
                comboBoxColor.Visible = true;
                comboBoxColor.DropDownStyle = ComboBoxStyle.DropDownList;
                comboBoxColor.Items.AddRange(new string[] { "無", "赤", "青", "黄", "緑" });
            }
            else
            {
                buttonMemoColor.Visible = false;
                comboBoxColor.Visible = false;
            }
            //20221101 ito end
        }

        private void TiffControl1_ShokaiClick(object sender, EventArgs e)
        {
            var r = (ReceView)bs.Current;
            if (r == null) return;

            if (tiffControl1.ShokaiButtonText == "照会")
            {
                string fileName = r.ShokaiImageFullPath;
                tiffControl1.SetTiff(fileName);
                tiffControl1.ShokaiButtonText = "申請書";
            }
            else
            {
                string fileName = r.ImageFullPath;
                tiffControl1.SetTiff(fileName);
                tiffControl1.ShokaiButtonText = "照会";
            }
        }

        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            var rece = (ReceView)bs.Current;
            if (rece == null)
            {
                tiffControl1.SetBlank();
                tiffControl1.ShokaiButtonVisible = false;
                textBoxMemo.Clear();
                buttonMemoUpdate.Enabled = false;
                buttonMemoCancel.Enabled = false;
                textBoxMemo.Enabled = false;

                comboBoxColor.Enabled = false;  //20221101 ito 付箋色対応

                return;
            }

            string fileName = rece.ImageFullPath;
            tiffControl1.SetTiff(fileName);
            tiffControl1.ShokaiButtonText = "照会";
            tiffControl1.ShokaiButtonVisible = rece.GetNeedShokaiButtonVisible();

            textBoxMemo.Text = rece.ViewerMemo;
            buttonMemoUpdate.Enabled = false;
            buttonMemoCancel.Enabled = false;
            textBoxMemo.Enabled = true;

            //20221101 ito st 付箋色対応            
            comboBoxColor.Enabled = true;
            if (comboBoxColor.Visible == true) comboBoxColor.SelectedIndex = rece.ViewerMemoColor;
            //20221101 ito end
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (Settings.FullWindow)
            {
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                Size = Settings.WindowSize;
            }

            tiffControl1.Width = Settings.ImageWidth;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.ImageWidth = tiffControl1.Width;
            Settings.WindowSize = Size;
            Settings.FullWindow = WindowState == FormWindowState.Maximized;
            ViewSettings.WidthToSetting(ctGrid1);
            ViewSettings.Save();
        }

        private void whereControl1_SearchClick(object sender, EventArgs e)
        {
            search();
        }

        private void search()
        {
            var where = whereControl1.CreateWhere();
            var l = Rece2.SelectByWhere(where);
            if (l == null)
            {
                bs.Clear();
                return;
            }

            bs.DataSource = ReceView.GetViewList(l);
            ctGrid1.DataSource = bs;
            bs.ResetBindings(false);

            buttonAll.Text = "全選択(F11)";
            labelCount.Text = $"件数:{bs.Count}";
            foreach (DataGridViewColumn item in ctGrid1.Columns)
            {
                item.ReadOnly = item.Name != "Select";
            }

            ctGrid1.Focus();    //20221201 ito
        }

        private void インポートToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new ImportForm()) f.ShowDialog();
        }

        private void buttonAll_Click(object sender, EventArgs e)
        {
            //20221201 ito st F11で全選択とするため
            searchResultAllSelect();
            //var l = ((IEnumerable<ReceView>)bs.DataSource);

            //if (buttonAll.Text == "全解除")
            //{
            //    foreach (var item in l) item.Select = false;
            //    buttonAll.Text = "全選択";
            //}
            //else
            //{
            //    foreach (var item in l)item.Select = true;
            //    buttonAll.Text = "全解除";
            //}
            //bs.ResetBindings(false);
            //20221201 ito end
        }

        private void buttonCSV_Click(object sender, EventArgs e)
        {
            var count = ((IEnumerable<ReceView>)bs.DataSource).Count();
            if (count == 0)
            {
                MessageBox.Show("出力対象となるデータがありません。");
                return;
            }

            if (!Common.YesNoMsg($"現在検索結果として表示されている{count}件のデータを出力します。よろしいですか？"))
                return;

            var fileName = "";
            using (var f = new SaveFileDialog())
            {
                f.Filter = "CSVファイル|*.csv";
                f.FileName = "柔整検索結果.csv";
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                fileName = f.FileName;
            }

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    var ss = new List<string>();

                    //ヘッダー出力
                    for (int c = 0; c < ctGrid1.Columns.Count; c++)
                    {
                        if (ctGrid1.Columns[c].Name == "Print") continue;
                        if (!ctGrid1.Columns[c].Visible) continue;
                        ss.Add(ctGrid1.Columns[c].HeaderText);
                    }
                    sw.WriteLine(string.Join(",", ss));
                    ss.Clear();

                    //データ出力
                    for (int r = 0; r < ctGrid1.RowCount; r++)
                    {
                        for (int c = 0; c < ctGrid1.Columns.Count; c++)
                        {
                            if (ctGrid1.Columns[c].Name == "Print") continue;
                            if (!ctGrid1.Columns[c].Visible) continue;
                            ss.Add(ctGrid1[c, r].Value?.ToString() ?? string.Empty);
                        }
                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CSV出力に失敗しました。\r\n" + ex.Message);
                Log.ErrorWrite(ex);
                return;
            }

            MessageBox.Show("CSV出力が完了しました。");
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            RecesPrint(true, false);
        }

        private void RecesPrint(bool recePrint, bool shokaiPrint)
        {
            var c = ((List<ReceView>)bs.DataSource).Count(r => r.Select);
            if (c == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            if (!Common.YesNoMsg($"現在チェックされている{c}件の画像を印刷します。よろしいですか？"))
                return;

            var reces = ((List<ReceView>)bs.DataSource).FindAll(r => r.Select);
            var ip = new ImagePrint();

            Task.Factory.StartNew(() =>
            {
                try
                {
                    ip.DoPrint(ReceView.GetReceList(reces), false, true, recePrint, shokaiPrint);
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    MessageBox.Show("ご迷惑をおかけします。印刷エラーが発生しました。繰り返す場合はシステム管理者までご連絡ください。");
                }
            });
        }

        private void buttonPreview_Click(object sender, EventArgs e)
        {
            var c = ((List<ReceView>)bs.DataSource).Count(r => r.Select);
            if (c == 0)
            {
                MessageBox.Show("現在チェックされているデータはありません。");
                return;
            }

            if (!Common.OKCancelMsg($"現在チェックされている{c}件の画像をプレビューします。よろしいですか？")) return;

            var reces = ((List<ReceView>)bs.DataSource).FindAll(r => r.Select);
            var ip = new ImagePrint();

            Task.Factory.StartNew(() =>
            {
                ip.DoPrint(ReceView.GetReceList(reces), true, true, true, false);
            });
        }

        private void 印刷設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new PrintSettingForm()) f.ShowDialog();
        }

        private void 表示設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new ViewSelttingForm()) f.ShowDialog();
            ViewSettings.FromSetting(ctGrid1);
        }

        private void 検索項目設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new WhereControlSettingForm()) f.ShowDialog();
            whereControl1.Adjust();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12) search();

            //20221201 ito st F11で全選択、左右キーでTiffページ移動
            if (e.KeyCode == Keys.F11) searchResultAllSelect();
            if (e.KeyCode == Keys.Left && this.ActiveControl == ctGrid1) tiffControl1.selectPage(tiffControl1.nowPageIndex - 1);
            if (e.KeyCode == Keys.Right && this.ActiveControl == ctGrid1) tiffControl1.selectPage(tiffControl1.nowPageIndex + 1);
            //20221201 ito end
        }

        private void フォント設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new FontSettingForm())
            {
                if (f.ShowDialog() != DialogResult.OK) return;

                nameFontStyle.Font = new Font(Settings.NameFont, 9);
                ctGrid1.DefaultCellStyle = nameFontStyle;
            }
        }

        private void メモMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMemo.Visible = !panelMemo.Visible;
            splitter2.Visible= panelMemo.Visible;

            Settings.MemoVisible= panelMemo.Visible;
            Settings.Save();
        }

        private void buttonMemoUpdate_Click(object sender, EventArgs e)
        {
            var r = (ReceView)bs.Current;
            if (r == null) return;

            //221101 ito st 兵庫広域対応
            //r.UpdateViewerMemo(textBoxMemo.Text.Trim());
            r.UpdateViewerMemo2(textBoxMemo.Text.Trim(), comboBoxColor.SelectedIndex);
            //221101 ito st 兵庫広域対応

            buttonMemoUpdate.Enabled = false;
            buttonMemoCancel.Enabled = false;
        }

        private void buttonMemoCancel_Click(object sender, EventArgs e)
        {
            var r = (ReceView)bs.Current;
            if (r == null) return;

            textBoxMemo.Text = r.ViewerMemo;
            buttonMemoUpdate.Enabled = false;
            buttonMemoCancel.Enabled = false;

            comboBoxColor.SelectedIndex = r.ViewerMemoColor;    //221101 ito 兵庫広域
        }

        private void textBoxMemo_TextChanged(object sender, EventArgs e)
        {
            var r = (ReceView)bs.Current;
            if (r == null) return;

            buttonMemoUpdate.Enabled = true;
            buttonMemoCancel.Enabled = true;
        }

        private void 申請書を印刷ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rv = (ReceView)bs.Current;
            if (rv == null)
            {
                MessageBox.Show("現在選択されているデータはありません。");
                return;
            }

            if (!Common.YesNoMsg($"現在選択されている申請書画像を印刷します。よろしいですか？"))
                return;


            var ip = new ImagePrint();
            var reces = new List<ReceView> { rv };

            Task.Factory.StartNew(() =>
            {
                try
                {
                    ip.DoPrint(ReceView.GetReceList(reces), false, true, true, false);
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    MessageBox.Show("ご迷惑をおかけします。印刷エラーが発生しました。繰り返す場合はシステム管理者までご連絡ください。");
                }
            });
        }

        private void 照会文書を印刷ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rv = (ReceView)bs.Current;
            if (rv == null)
            {
                MessageBox.Show("現在選択されているデータはありません。");
                return;
            }

            if (!Common.YesNoMsg($"現在選択されている照会画像を印刷します。よろしいですか？"))
                return;


            var ip = new ImagePrint();
            var reces = new List<ReceView> { rv };

            Task.Factory.StartNew(() =>
            {
                try
                {
                    ip.DoPrint(ReceView.GetReceList(reces), false, true, false, true);
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    MessageBox.Show("ご迷惑をおかけします。印刷エラーが発生しました。繰り返す場合はシステム管理者までご連絡ください。");
                }
            });
        }

        private void 申請書と照会文書を印刷ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rv = (ReceView)bs.Current;
            if (rv == null)
            {
                MessageBox.Show("現在選択されているデータはありません。");
                return;
            }

            if (!Common.YesNoMsg($"現在選択されている申請書と照会画像を印刷します。よろしいですか？"))
                return;


            var ip = new ImagePrint();
            var reces = new List<ReceView> { rv };

            Task.Factory.StartNew(() =>
            {
                try
                {
                    ip.DoPrint(ReceView.GetReceList(reces), false, true, true, true);
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    MessageBox.Show("ご迷惑をおかけします。印刷エラーが発生しました。繰り返す場合はシステム管理者までご連絡ください。");
                }
            });

        }

        private void contextMenuStrip1_Opened(object sender, EventArgs e)
        {
            var enable = bs.Current != null;
            照会文書を印刷ToolStripMenuItem.Enabled = enable;
            申請書と照会文書を印刷ToolStripMenuItem.Enabled = enable;
            申請書を印刷ToolStripMenuItem.Enabled = enable;
        }

        private void 照会画像を含めて印刷ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RecesPrint(true, true);
        }

        private void 照会画像のみを印刷ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RecesPrint(false, true);
        }


        //20190725165822 furukawa st ////////////////////////
        //終了させる        
        private void 終了XToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //20190725165822 furukawa ed ////////////////////////

        //221101 ito st 兵庫広域対応
        private void comboBoxColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            var r = (ReceView)bs.Current;
            if (r == null) return;

            if (r.ViewerMemoColor == comboBoxColor.SelectedIndex)
            {
                buttonMemoUpdate.Enabled = false;
                buttonMemoCancel.Enabled = false;
            }
            else
            {
                buttonMemoUpdate.Enabled = true;
                buttonMemoCancel.Enabled = true;
            }

            if (comboBoxColor.SelectedIndex == 0) buttonMemoColor.BackColor = SystemColors.ControlLight;
            else if (comboBoxColor.SelectedIndex == 1) buttonMemoColor.BackColor = Color.Red;
            else if (comboBoxColor.SelectedIndex == 2) buttonMemoColor.BackColor = Color.Blue;
            else if (comboBoxColor.SelectedIndex == 3) buttonMemoColor.BackColor = Color.Yellow;
            else if (comboBoxColor.SelectedIndex == 4) buttonMemoColor.BackColor = Color.Green;
        }

        private void searchResultAllSelect()
        {
            var l = ((IEnumerable<ReceView>)bs.DataSource);

            if (buttonAll.Text == "全解除(F11)")
            {
                foreach (var item in l) item.Select = false;
                buttonAll.Text = "全選択(F11)";
            }
            else
            {
                foreach (var item in l) item.Select = true;
                buttonAll.Text = "全解除(F11)";
            }
            bs.ResetBindings(false);

        }
        //221101 ito end 兵庫広域対応
    }
}
