﻿namespace MejorViewer
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ファイルFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.インポートToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.終了XToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ツールTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.表示設定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.検索項目設定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.印刷設定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.フォント設定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.メモMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelPrint = new System.Windows.Forms.Panel();
            this.labelCount = new System.Windows.Forms.Label();
            this.buttonPreview = new System.Windows.Forms.Button();
            this.buttonCSV = new System.Windows.Forms.Button();
            this.buttonAll = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.紹介画像を含めて印刷ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.紹介画像のみを印刷ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelMemo = new System.Windows.Forms.Panel();
            this.buttonMemoColor = new System.Windows.Forms.Button();
            this.comboBoxColor = new System.Windows.Forms.ComboBox();
            this.buttonMemoCancel = new System.Windows.Forms.Button();
            this.buttonMemoUpdate = new System.Windows.Forms.Button();
            this.textBoxMemo = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.申請書を印刷ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.照会文書を印刷ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.申請書と照会文書を印刷ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctGrid1 = new MejorViewer.CtGrid();
            this.whereControl1 = new MejorViewer.WhereControl();
            this.tiffControl1 = new TiffControl();
            this.menuStrip1.SuspendLayout();
            this.panelPrint.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelMemo.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(579, 24);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 538);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ファイルFToolStripMenuItem,
            this.ツールTToolStripMenuItem,
            this.メモMToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1384, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ファイルFToolStripMenuItem
            // 
            this.ファイルFToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.インポートToolStripMenuItem,
            this.toolStripSeparator,
            this.終了XToolStripMenuItem});
            this.ファイルFToolStripMenuItem.Name = "ファイルFToolStripMenuItem";
            this.ファイルFToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.ファイルFToolStripMenuItem.Text = "ファイル(&F)";
            // 
            // インポートToolStripMenuItem
            // 
            this.インポートToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("インポートToolStripMenuItem.Image")));
            this.インポートToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.インポートToolStripMenuItem.Name = "インポートToolStripMenuItem";
            this.インポートToolStripMenuItem.ShowShortcutKeys = false;
            this.インポートToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.インポートToolStripMenuItem.Text = "インポート";
            this.インポートToolStripMenuItem.Click += new System.EventHandler(this.インポートToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(110, 6);
            // 
            // 終了XToolStripMenuItem
            // 
            this.終了XToolStripMenuItem.Name = "終了XToolStripMenuItem";
            this.終了XToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.終了XToolStripMenuItem.Text = "終了(&X)";
            this.終了XToolStripMenuItem.Click += new System.EventHandler(this.終了XToolStripMenuItem_Click);
            // 
            // ツールTToolStripMenuItem
            // 
            this.ツールTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.表示設定ToolStripMenuItem,
            this.検索項目設定ToolStripMenuItem,
            this.印刷設定ToolStripMenuItem,
            this.フォント設定ToolStripMenuItem});
            this.ツールTToolStripMenuItem.Name = "ツールTToolStripMenuItem";
            this.ツールTToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.ツールTToolStripMenuItem.Text = "ツール(&T)";
            // 
            // 表示設定ToolStripMenuItem
            // 
            this.表示設定ToolStripMenuItem.Name = "表示設定ToolStripMenuItem";
            this.表示設定ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.表示設定ToolStripMenuItem.Text = "表示設定";
            this.表示設定ToolStripMenuItem.Click += new System.EventHandler(this.表示設定ToolStripMenuItem_Click);
            // 
            // 検索項目設定ToolStripMenuItem
            // 
            this.検索項目設定ToolStripMenuItem.Name = "検索項目設定ToolStripMenuItem";
            this.検索項目設定ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.検索項目設定ToolStripMenuItem.Text = "検索項目設定";
            this.検索項目設定ToolStripMenuItem.Click += new System.EventHandler(this.検索項目設定ToolStripMenuItem_Click);
            // 
            // 印刷設定ToolStripMenuItem
            // 
            this.印刷設定ToolStripMenuItem.Name = "印刷設定ToolStripMenuItem";
            this.印刷設定ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.印刷設定ToolStripMenuItem.Text = "印刷設定";
            this.印刷設定ToolStripMenuItem.Click += new System.EventHandler(this.印刷設定ToolStripMenuItem_Click);
            // 
            // フォント設定ToolStripMenuItem
            // 
            this.フォント設定ToolStripMenuItem.Name = "フォント設定ToolStripMenuItem";
            this.フォント設定ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.フォント設定ToolStripMenuItem.Text = "フォント設定";
            this.フォント設定ToolStripMenuItem.Click += new System.EventHandler(this.フォント設定ToolStripMenuItem_Click);
            // 
            // メモMToolStripMenuItem
            // 
            this.メモMToolStripMenuItem.CheckOnClick = true;
            this.メモMToolStripMenuItem.Name = "メモMToolStripMenuItem";
            this.メモMToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.メモMToolStripMenuItem.Text = "メモ(&M)";
            this.メモMToolStripMenuItem.Click += new System.EventHandler(this.メモMToolStripMenuItem_Click);
            // 
            // panelPrint
            // 
            this.panelPrint.Controls.Add(this.labelCount);
            this.panelPrint.Controls.Add(this.buttonPreview);
            this.panelPrint.Controls.Add(this.buttonCSV);
            this.panelPrint.Controls.Add(this.buttonAll);
            this.panelPrint.Controls.Add(this.buttonPrint);
            this.panelPrint.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelPrint.Location = new System.Drawing.Point(582, 534);
            this.panelPrint.Name = "panelPrint";
            this.panelPrint.Size = new System.Drawing.Size(802, 28);
            this.panelPrint.TabIndex = 14;
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.Location = new System.Drawing.Point(100, 8);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(31, 12);
            this.labelCount.TabIndex = 4;
            this.labelCount.Text = "件数:";
            // 
            // buttonPreview
            // 
            this.buttonPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreview.Location = new System.Drawing.Point(637, 3);
            this.buttonPreview.Name = "buttonPreview";
            this.buttonPreview.Size = new System.Drawing.Size(75, 23);
            this.buttonPreview.TabIndex = 2;
            this.buttonPreview.Text = "プレビュー";
            this.buttonPreview.UseVisualStyleBackColor = true;
            this.buttonPreview.Click += new System.EventHandler(this.buttonPreview_Click);
            // 
            // buttonCSV
            // 
            this.buttonCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCSV.Location = new System.Drawing.Point(718, 3);
            this.buttonCSV.Name = "buttonCSV";
            this.buttonCSV.Size = new System.Drawing.Size(75, 23);
            this.buttonCSV.TabIndex = 3;
            this.buttonCSV.Text = "CSV出力";
            this.buttonCSV.UseVisualStyleBackColor = true;
            this.buttonCSV.Click += new System.EventHandler(this.buttonCSV_Click);
            // 
            // buttonAll
            // 
            this.buttonAll.Location = new System.Drawing.Point(8, 3);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(85, 23);
            this.buttonAll.TabIndex = 0;
            this.buttonAll.Text = "全選択(F11)";
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrint.ContextMenuStrip = this.contextMenuStrip2;
            this.buttonPrint.Location = new System.Drawing.Point(556, 3);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(75, 23);
            this.buttonPrint.TabIndex = 1;
            this.buttonPrint.Text = "印刷";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.紹介画像を含めて印刷ToolStripMenuItem,
            this.紹介画像のみを印刷ToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(187, 48);
            // 
            // 紹介画像を含めて印刷ToolStripMenuItem
            // 
            this.紹介画像を含めて印刷ToolStripMenuItem.Name = "紹介画像を含めて印刷ToolStripMenuItem";
            this.紹介画像を含めて印刷ToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.紹介画像を含めて印刷ToolStripMenuItem.Text = "照会画像を含めて印刷";
            this.紹介画像を含めて印刷ToolStripMenuItem.Click += new System.EventHandler(this.照会画像を含めて印刷ToolStripMenuItem_Click);
            // 
            // 紹介画像のみを印刷ToolStripMenuItem
            // 
            this.紹介画像のみを印刷ToolStripMenuItem.Name = "紹介画像のみを印刷ToolStripMenuItem";
            this.紹介画像のみを印刷ToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.紹介画像のみを印刷ToolStripMenuItem.Text = "照会画像のみを印刷";
            this.紹介画像のみを印刷ToolStripMenuItem.Click += new System.EventHandler(this.照会画像のみを印刷ToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tiffControl1);
            this.panel1.Controls.Add(this.splitter2);
            this.panel1.Controls.Add(this.panelMemo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(579, 538);
            this.panel1.TabIndex = 15;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Location = new System.Drawing.Point(0, 451);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(579, 3);
            this.splitter2.TabIndex = 3;
            this.splitter2.TabStop = false;
            // 
            // panelMemo
            // 
            this.panelMemo.Controls.Add(this.buttonMemoColor);
            this.panelMemo.Controls.Add(this.comboBoxColor);
            this.panelMemo.Controls.Add(this.buttonMemoCancel);
            this.panelMemo.Controls.Add(this.buttonMemoUpdate);
            this.panelMemo.Controls.Add(this.textBoxMemo);
            this.panelMemo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMemo.Location = new System.Drawing.Point(0, 454);
            this.panelMemo.Name = "panelMemo";
            this.panelMemo.Size = new System.Drawing.Size(579, 84);
            this.panelMemo.TabIndex = 2;
            this.panelMemo.Visible = false;
            // 
            // buttonMemoColor
            // 
            this.buttonMemoColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMemoColor.Location = new System.Drawing.Point(501, 3);
            this.buttonMemoColor.Name = "buttonMemoColor";
            this.buttonMemoColor.Size = new System.Drawing.Size(30, 22);
            this.buttonMemoColor.TabIndex = 3;
            this.buttonMemoColor.UseVisualStyleBackColor = true;
            // 
            // comboBoxColor
            // 
            this.comboBoxColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxColor.FormattingEnabled = true;
            this.comboBoxColor.Location = new System.Drawing.Point(533, 4);
            this.comboBoxColor.Name = "comboBoxColor";
            this.comboBoxColor.Size = new System.Drawing.Size(43, 20);
            this.comboBoxColor.TabIndex = 2;
            this.comboBoxColor.SelectedIndexChanged += new System.EventHandler(this.comboBoxColor_SelectedIndexChanged);
            // 
            // buttonMemoCancel
            // 
            this.buttonMemoCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMemoCancel.Enabled = false;
            this.buttonMemoCancel.Location = new System.Drawing.Point(501, 57);
            this.buttonMemoCancel.Name = "buttonMemoCancel";
            this.buttonMemoCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonMemoCancel.TabIndex = 1;
            this.buttonMemoCancel.Text = "キャンセル";
            this.buttonMemoCancel.UseVisualStyleBackColor = true;
            this.buttonMemoCancel.Click += new System.EventHandler(this.buttonMemoCancel_Click);
            // 
            // buttonMemoUpdate
            // 
            this.buttonMemoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMemoUpdate.Enabled = false;
            this.buttonMemoUpdate.Location = new System.Drawing.Point(501, 33);
            this.buttonMemoUpdate.Name = "buttonMemoUpdate";
            this.buttonMemoUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonMemoUpdate.TabIndex = 1;
            this.buttonMemoUpdate.Text = "更新";
            this.buttonMemoUpdate.UseVisualStyleBackColor = true;
            this.buttonMemoUpdate.Click += new System.EventHandler(this.buttonMemoUpdate_Click);
            // 
            // textBoxMemo
            // 
            this.textBoxMemo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMemo.Enabled = false;
            this.textBoxMemo.Location = new System.Drawing.Point(3, 3);
            this.textBoxMemo.Multiline = true;
            this.textBoxMemo.Name = "textBoxMemo";
            this.textBoxMemo.Size = new System.Drawing.Size(496, 78);
            this.textBoxMemo.TabIndex = 0;
            this.textBoxMemo.TextChanged += new System.EventHandler(this.textBoxMemo_TextChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.申請書を印刷ToolStripMenuItem,
            this.照会文書を印刷ToolStripMenuItem,
            this.申請書と照会文書を印刷ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(201, 70);
            this.contextMenuStrip1.Opened += new System.EventHandler(this.contextMenuStrip1_Opened);
            // 
            // 申請書を印刷ToolStripMenuItem
            // 
            this.申請書を印刷ToolStripMenuItem.Name = "申請書を印刷ToolStripMenuItem";
            this.申請書を印刷ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.申請書を印刷ToolStripMenuItem.Text = "申請書を印刷";
            this.申請書を印刷ToolStripMenuItem.Click += new System.EventHandler(this.申請書を印刷ToolStripMenuItem_Click);
            // 
            // 照会文書を印刷ToolStripMenuItem
            // 
            this.照会文書を印刷ToolStripMenuItem.Name = "照会文書を印刷ToolStripMenuItem";
            this.照会文書を印刷ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.照会文書を印刷ToolStripMenuItem.Text = "照会文書を印刷";
            this.照会文書を印刷ToolStripMenuItem.Click += new System.EventHandler(this.照会文書を印刷ToolStripMenuItem_Click);
            // 
            // 申請書と照会文書を印刷ToolStripMenuItem
            // 
            this.申請書と照会文書を印刷ToolStripMenuItem.Name = "申請書と照会文書を印刷ToolStripMenuItem";
            this.申請書と照会文書を印刷ToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.申請書と照会文書を印刷ToolStripMenuItem.Text = "申請書と照会文書を印刷";
            this.申請書と照会文書を印刷ToolStripMenuItem.Click += new System.EventHandler(this.申請書と照会文書を印刷ToolStripMenuItem_Click);
            // 
            // ctGrid1
            // 
            this.ctGrid1.AllowUserToAddRows = false;
            this.ctGrid1.AllowUserToResizeRows = false;
            this.ctGrid1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.ctGrid1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ctGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ctGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ctGrid1.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ctGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.ctGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctGrid1.Location = new System.Drawing.Point(582, 71);
            this.ctGrid1.Name = "ctGrid1";
            this.ctGrid1.RowHeadersVisible = false;
            this.ctGrid1.RowTemplate.Height = 21;
            this.ctGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ctGrid1.Size = new System.Drawing.Size(802, 463);
            this.ctGrid1.TabIndex = 2;
            // 
            // whereControl1
            // 
            this.whereControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.whereControl1.Location = new System.Drawing.Point(582, 24);
            this.whereControl1.Name = "whereControl1";
            this.whereControl1.Size = new System.Drawing.Size(802, 47);
            this.whereControl1.TabIndex = 3;
            this.whereControl1.SearchClick += new System.EventHandler(this.whereControl1_SearchClick);
            // 
            // tiffControl1
            // 
            this.tiffControl1.AutoScroll = true;
            this.tiffControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tiffControl1.Location = new System.Drawing.Point(0, 0);
            this.tiffControl1.Name = "tiffControl1";
            this.tiffControl1.ShokaiButtonText = "照会";
            this.tiffControl1.ShokaiButtonVisible = false;
            this.tiffControl1.Size = new System.Drawing.Size(579, 451);
            this.tiffControl1.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 562);
            this.Controls.Add(this.ctGrid1);
            this.Controls.Add(this.panelPrint);
            this.Controls.Add(this.whereControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MejorViewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelPrint.ResumeLayout(false);
            this.panelPrint.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelMemo.ResumeLayout(false);
            this.panelMemo.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ctGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private TiffControl tiffControl1;
        private CtGrid ctGrid1;
        private System.Windows.Forms.Splitter splitter1;
        private WhereControl whereControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel panelPrint;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.Button buttonPreview;
        private System.Windows.Forms.Button buttonCSV;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.ToolStripMenuItem ファイルFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem インポートToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem 終了XToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ツールTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 表示設定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 印刷設定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem フォント設定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 検索項目設定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem メモMToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panelMemo;
        private System.Windows.Forms.Button buttonMemoCancel;
        private System.Windows.Forms.Button buttonMemoUpdate;
        private System.Windows.Forms.TextBox textBoxMemo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 照会文書を印刷ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 申請書を印刷ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 申請書と照会文書を印刷ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem 紹介画像を含めて印刷ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 紹介画像のみを印刷ToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxColor;
        private System.Windows.Forms.Button buttonMemoColor;
    }
}

