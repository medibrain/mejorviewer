﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;


namespace MejorViewer
{
    class ImagePrint
    {
        int receIndex;
        List<Rece2> reces;
        List<Image> multiImages = new List<Image>();
        bool recePrint = false;
        bool shokaiPrint = false;

        private List<Image> getMultiImage(string fileName)
        {
            var l = new List<Image>();

            if (!System.IO.File.Exists(fileName)) return null;
            using (var ms = new System.IO.MemoryStream())
            using (var tifFS = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                try
                {
                    tifFS.CopyTo(ms);
                    using (var img = Image.FromStream(ms))
                    {
                        var fd = new FrameDimension(img.FrameDimensionsList[0]);
                        int pageCount = img.GetFrameCount(fd);

                        for (int i = 0; i < pageCount; i++)
                        {
                            img.SelectActiveFrame(fd, i);
                            var frame = new Bitmap(img.Width, img.Height);
                            using (var g = Graphics.FromImage(frame))
                            {
                                g.DrawImage(img, 0, 0, img.Width, img.Height);
                            }
                            l.Add(frame);
                        }

                        //20221201 ito st 両面印刷時に奇数マルチページTIFFに空白ページを付加する
                        if (Settings.DoublePageBlank == true && pageCount % 2 == 1) l.Add(new Bitmap(100,100));
                    }
                }
                catch
                {
                    return null;
                }
            }

            return l;
        }

        public void DoPrint(List<Rece2> reces, bool preview, bool addMargin, bool recePrint, bool shokaiPrint)
        {
            receIndex = 0;
            this.reces = reces;
            this.recePrint = recePrint;
            this.shokaiPrint = shokaiPrint;

            using (var printDocument = new PrintDocument())
            {
                printDocument.PrintPage += printDocument_PrintPage;

                //　プリンタ名と余白設定を読み込む
                string printerName = Settings.PrinterName;
                int printMargin = Settings.PrintMargin;

                // 印刷ドキュメントに、プリンタを設定する。
                if (printerName.Length > 0) printDocument.PrinterSettings.PrinterName = printerName;

                // 用紙の向きを設定(横：true、縦：false)
                printDocument.DefaultPageSettings.Landscape = false;

                // 余白を0.01インチ単位に変換  (＝ 余白ミリ/ 25.4ミリ * 100) し、設定する
                printMargin = addMargin ? (int)((double)printMargin * 100.0 / 25.4) : 0;
                printDocument.DefaultPageSettings.Margins.Top = printMargin;
                //20221205 ito printDocument_PrintPage()　にて【印刷位置のセンタリング (余白を調整する)】をしてしまっているため、以下3行は意味が無いためゼロ指定に。
                printDocument.DefaultPageSettings.Margins.Bottom = 0;// printMargin;
                printDocument.DefaultPageSettings.Margins.Left = 0;// printMargin;
                printDocument.DefaultPageSettings.Margins.Right = 0;// printMargin;

                // 用紙設定（A4）
                foreach (PaperSize ps in printDocument.PrinterSettings.PaperSizes)
                {
                    if (ps.Kind == PaperKind.A4)
                    {
                        printDocument.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                        break;
                    }
                }

                if (preview)
                {
                    using (var printPreviewDialog = new System.Windows.Forms.PrintPreviewDialog())
                    {
                        // 印刷プレビューの実行
                        printPreviewDialog.Document = printDocument;
                        printPreviewDialog.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                        printPreviewDialog.ShowDialog();
                    }
                }
                else
                {
                    // 印刷の実行
                    printDocument.Print();
                }
            }
        }

        /** 印刷イベント処理 */
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Image img = null;

            try
            {
                List<Image> imgs = new List<Image>();
                // 画像を取得
                if (multiImages.Count == 0)
                {
                    if (recePrint)
                    {
                        imgs.AddRange(getMultiImage(reces[receIndex].ImageFullPath));
                    }

                    if (shokaiPrint)
                    {
                        imgs.AddRange(getMultiImage(reces[receIndex].ShokaiImageFullPath));
                    }

                    if (imgs == null || imgs.Count == 0)
                    {
                        receIndex++;
                        e.HasMorePages = reces.Count > receIndex;
                        return;
                    }

                    if (imgs.Count > 1)
                    {
                        //マルチページの場合
                        multiImages = imgs;
                        img = multiImages[0];
                        multiImages.RemoveAt(0);
                    }
                    else
                    {
                        //単独の場合
                        img = imgs[0];
                    }
                    receIndex++;
                    e.HasMorePages = multiImages.Count > 0 || reces.Count > receIndex;
                }
                else
                {
                    img = multiImages[0];
                    multiImages.RemoveAt(0);

                    e.HasMorePages = multiImages.Count == 0 ? reces.Count > receIndex : true;
                }

                var rect = e.PageBounds;
                var pw = rect.Width - e.PageSettings.Margins.Left - e.PageSettings.Margins.Right;
                var ph = rect.Height - e.PageSettings.Margins.Top - e.PageSettings.Margins.Bottom;

                // 単位を0.01インチ単位に合わせる
                float imageW = img.Width / 2;         // 画像幅 （ピクセル数 / 200dpi）インチ x100
                float imageH = img.Height / 2;        // 画像高 （ピクセル数 / 200dpi）インチ x100


                // 縦・横の調整 
                // 画像が横向き（幅>長）&& 画像の横がページ内（余白より内側）に収まらないとき、90度回転させる！
                if (imageW > imageH && imageW > e.MarginBounds.Width)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);    // 反転させずに時計回りに回転させる
                    var tmp = imageW;
                    imageW = imageH;
                    imageH = tmp;
                }

                // まず、サイズ合わせを行う！ （画像サイズか、ページサイズのうちの最小値）
                float w = Math.Min(imageW, e.MarginBounds.Width);         // 印刷幅 = 画像幅 or ページ幅 の最小
                float h = Math.Min(imageH, e.MarginBounds.Height);        // 印刷高 = 画像高 or ページ高 の最小

                // 縦横比の調整 (印刷範囲の縦横比を、元の画像と同じにする)
                float rate = Math.Min(w / imageW, h / imageH);          // 縮尺 = 印刷幅/画像幅 or 印刷高/画像高 の最小
                w = imageW * rate;
                h = imageH * rate;

                // 印刷位置のセンタリング (余白を調整する)
                // 余白 = 中心位置の差分 = (ページサイズ - 印刷サイズ ) / 2.0
                float x = (e.PageBounds.Width - w) / 2.0f;
                float y = (e.PageBounds.Height - h) / 2.0f;

                // プレビューではなく実際の印刷の時はプリンタの物理的な余白分ずらす
                if (((PrintDocument)sender).PrintController.IsPreview == false)
                {
                    x -= e.PageSettings.HardMarginX;
                    y -= e.PageSettings.HardMarginY;
                }

                // 印刷対象の画像のリストの現在ページを出力
                //e.Graphics.PageUnit = GraphicsUnit.Inch;
                e.Graphics.DrawImage(img, x, y, w, h);
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
            }
            finally
            {
                if (img != null) img.Dispose();

                if (!e.HasMorePages) receIndex = 0; //20221201 ito プレビュー画面から印刷ボタンを押した際、印刷できない問題の解消
            }
        }
    }
}
