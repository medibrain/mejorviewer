﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MejorViewer
{
    public partial class WaitFormSimple : Form
    {
        public bool Cancel { get; private set; } = false;
        public event EventHandler Canceling;
        public bool handleClreated = false;
        public bool disposed = false;

        public WaitFormSimple()
        {
            InitializeComponent();
            this.VisibleChanged += WaitFormSimple_VisibleChanged;
            
            this.HandleCreated += ((s, e) => handleClreated = true);
            this.Disposed += ((s, e) => disposed = true);
            this.Load += WaitFormSimple_Load;
        }

        private void WaitFormSimple_VisibleChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void WaitFormSimple_Load(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Canceling?.Invoke(this, e);
            Cancel = true;
        }

        Action dispose() => new Action(() =>
        {
            this.Close();
            this.Dispose();
        });

        public void InvokeDispose()
        {
            if (disposed) return;
            
            while (!handleClreated) System.Threading.Thread.Sleep(5);
            while (Disposing) System.Threading.Thread.Sleep(5);

            Invoke(dispose());
        }
    }
}
