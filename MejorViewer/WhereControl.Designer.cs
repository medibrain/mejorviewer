﻿namespace MejorViewer
{
    partial class WhereControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxYM = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxCYM = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxTotal = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxClinicNum = new System.Windows.Forms.GroupBox();
            this.textBoxClinicNum = new System.Windows.Forms.TextBox();
            this.groupBoxOryo = new System.Windows.Forms.GroupBox();
            this.checkBoxOshinNashi = new System.Windows.Forms.CheckBox();
            this.checkBoxOshinAri = new System.Windows.Forms.CheckBox();
            this.groupBoxDrName = new System.Windows.Forms.GroupBox();
            this.textBoxDrName = new System.Windows.Forms.TextBox();
            this.groupBoxDrCode = new System.Windows.Forms.GroupBox();
            this.groupBoxType = new System.Windows.Forms.GroupBox();
            this.checkBoxMs = new System.Windows.Forms.CheckBox();
            this.checkBoxJyu = new System.Windows.Forms.CheckBox();
            this.checkBoxSin = new System.Windows.Forms.CheckBox();
            this.groupBoxShokai = new System.Windows.Forms.GroupBox();
            this.checkBoxShokaiNashi = new System.Windows.Forms.CheckBox();
            this.checkBoxShokaiTaisho = new System.Windows.Forms.CheckBox();
            this.groupBoxNum = new System.Windows.Forms.GroupBox();
            this.groupBoxHenrei = new System.Windows.Forms.GroupBox();
            this.checkBoxHenNashi = new System.Windows.Forms.CheckBox();
            this.checkBoxHenAri = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBoxInsNum = new System.Windows.Forms.GroupBox();
            this.groupBoxName = new System.Windows.Forms.GroupBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.groupBoxKana = new System.Windows.Forms.GroupBox();
            this.textBoxKana = new System.Windows.Forms.TextBox();
            this.groupBoxClinicName = new System.Windows.Forms.GroupBox();
            this.textBoxClinicName = new System.Windows.Forms.TextBox();
            this.groupBoxGroupNum = new System.Windows.Forms.GroupBox();
            this.groupBoxFusho = new System.Windows.Forms.GroupBox();
            this.textBoxFusho = new System.Windows.Forms.TextBox();
            this.groupBoxCharge = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBoxDays = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxFushoCount = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxNewCont = new System.Windows.Forms.GroupBox();
            this.checkBoxNewContNew = new System.Windows.Forms.CheckBox();
            this.checkBoxNewContCont = new System.Windows.Forms.CheckBox();
            this.groupBoxAnmaCount = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBoxAnma = new System.Windows.Forms.GroupBox();
            this.checkBoxLeftLower = new System.Windows.Forms.CheckBox();
            this.checkBoxRightLower = new System.Windows.Forms.CheckBox();
            this.checkBoxRightUpper = new System.Windows.Forms.CheckBox();
            this.checkBoxLeftUpper = new System.Windows.Forms.CheckBox();
            this.checkBoxBody = new System.Windows.Forms.CheckBox();
            this.groupBoxShokaiResult = new System.Windows.Forms.GroupBox();
            this.checkBoxShokaiSouiNashi = new System.Windows.Forms.CheckBox();
            this.checkBoxShokaiSouiAri = new System.Windows.Forms.CheckBox();
            this.groupBoxDouisyo = new System.Windows.Forms.GroupBox();
            this.checkBoxDouisyoNashi = new System.Windows.Forms.CheckBox();
            this.checkBoxDouisyoAri = new System.Windows.Forms.CheckBox();
            this.groupBoxSejutsuHoukokusyo = new System.Windows.Forms.GroupBox();
            this.checkBoxSejutsuHoukokusyoNashi = new System.Windows.Forms.CheckBox();
            this.checkBoxSejutsuHoukokusyoAri = new System.Windows.Forms.CheckBox();
            this.groupBoxJoutaikinyusyo = new System.Windows.Forms.GroupBox();
            this.checkBoxJoutaikinyusyoNashi = new System.Windows.Forms.CheckBox();
            this.checkBoxJoutaikinyusyoAri = new System.Windows.Forms.CheckBox();
            this.groupBoxDual = new System.Windows.Forms.GroupBox();
            this.checkBoxDual = new System.Windows.Forms.CheckBox();
            this.groupBoxStartDate = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBoxFinishDate = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBoxDouiDate = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panelSeparator = new System.Windows.Forms.Panel();
            this.monthBoxS2 = new MonthBox();
            this.monthBoxE2 = new MonthBox();
            this.monthBoxS1 = new MonthBox();
            this.monthBoxE1 = new MonthBox();
            this.numBoxNum = new NumBox();
            this.numBoxInsNum = new NumBox();
            this.numBoxDr = new NumBox();
            this.numBoxGroup = new NumBox();
            this.numBoxTotalMax = new NumBox();
            this.numBoxTotalMin = new NumBox();
            this.numBoxChargeMax = new NumBox();
            this.numBoxChargeMin = new NumBox();
            this.numBoxDaysMax = new NumBox();
            this.numBoxDaysMin = new NumBox();
            this.numBoxFushoMax = new NumBox();
            this.numBoxFushoMin = new NumBox();
            this.numBoxAnmaMax = new NumBox();
            this.numBoxAnmaMin = new NumBox();
            this.dateBoxStartTo = new MejorViewer.DateBox();
            this.dateBoxStartFrom = new MejorViewer.DateBox();
            this.dateBoxFinishTo = new MejorViewer.DateBox();
            this.dateBoxFinishFrom = new MejorViewer.DateBox();
            this.dateBoxDouiTo = new MejorViewer.DateBox();
            this.dateBoxDouiFrom = new MejorViewer.DateBox();
            this.groupBoxYM.SuspendLayout();
            this.groupBoxCYM.SuspendLayout();
            this.groupBoxTotal.SuspendLayout();
            this.groupBoxClinicNum.SuspendLayout();
            this.groupBoxOryo.SuspendLayout();
            this.groupBoxDrName.SuspendLayout();
            this.groupBoxDrCode.SuspendLayout();
            this.groupBoxType.SuspendLayout();
            this.groupBoxShokai.SuspendLayout();
            this.groupBoxNum.SuspendLayout();
            this.groupBoxHenrei.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBoxInsNum.SuspendLayout();
            this.groupBoxName.SuspendLayout();
            this.groupBoxKana.SuspendLayout();
            this.groupBoxClinicName.SuspendLayout();
            this.groupBoxGroupNum.SuspendLayout();
            this.groupBoxFusho.SuspendLayout();
            this.groupBoxCharge.SuspendLayout();
            this.groupBoxDays.SuspendLayout();
            this.groupBoxFushoCount.SuspendLayout();
            this.groupBoxNewCont.SuspendLayout();
            this.groupBoxAnmaCount.SuspendLayout();
            this.groupBoxAnma.SuspendLayout();
            this.groupBoxShokaiResult.SuspendLayout();
            this.groupBoxDouisyo.SuspendLayout();
            this.groupBoxSejutsuHoukokusyo.SuspendLayout();
            this.groupBoxJoutaikinyusyo.SuspendLayout();
            this.groupBoxDual.SuspendLayout();
            this.groupBoxStartDate.SuspendLayout();
            this.groupBoxFinishDate.SuspendLayout();
            this.groupBoxDouiDate.SuspendLayout();
            this.panelSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxYM
            // 
            this.groupBoxYM.Controls.Add(this.monthBoxS1);
            this.groupBoxYM.Controls.Add(this.monthBoxE1);
            this.groupBoxYM.Controls.Add(this.label2);
            this.groupBoxYM.Location = new System.Drawing.Point(189, 3);
            this.groupBoxYM.Name = "groupBoxYM";
            this.groupBoxYM.Size = new System.Drawing.Size(180, 46);
            this.groupBoxYM.TabIndex = 22;
            this.groupBoxYM.TabStop = false;
            this.groupBoxYM.Text = "診療年月";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "～";
            // 
            // groupBoxCYM
            // 
            this.groupBoxCYM.Controls.Add(this.label6);
            this.groupBoxCYM.Controls.Add(this.monthBoxS2);
            this.groupBoxCYM.Controls.Add(this.monthBoxE2);
            this.groupBoxCYM.Location = new System.Drawing.Point(3, 3);
            this.groupBoxCYM.Name = "groupBoxCYM";
            this.groupBoxCYM.Size = new System.Drawing.Size(180, 46);
            this.groupBoxCYM.TabIndex = 23;
            this.groupBoxCYM.TabStop = false;
            this.groupBoxCYM.Text = "処理年月";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "～";
            // 
            // groupBoxTotal
            // 
            this.groupBoxTotal.Controls.Add(this.label7);
            this.groupBoxTotal.Controls.Add(this.numBoxTotalMax);
            this.groupBoxTotal.Controls.Add(this.numBoxTotalMin);
            this.groupBoxTotal.Location = new System.Drawing.Point(3, 211);
            this.groupBoxTotal.Name = "groupBoxTotal";
            this.groupBoxTotal.Size = new System.Drawing.Size(158, 46);
            this.groupBoxTotal.TabIndex = 24;
            this.groupBoxTotal.TabStop = false;
            this.groupBoxTotal.Text = "合計金額";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "～";
            // 
            // groupBoxClinicNum
            // 
            this.groupBoxClinicNum.Controls.Add(this.textBoxClinicNum);
            this.groupBoxClinicNum.Location = new System.Drawing.Point(435, 107);
            this.groupBoxClinicNum.Name = "groupBoxClinicNum";
            this.groupBoxClinicNum.Size = new System.Drawing.Size(210, 46);
            this.groupBoxClinicNum.TabIndex = 29;
            this.groupBoxClinicNum.TabStop = false;
            this.groupBoxClinicNum.Text = "施術所コード";
            // 
            // textBoxClinicNum
            // 
            this.textBoxClinicNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxClinicNum.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxClinicNum.Location = new System.Drawing.Point(12, 18);
            this.textBoxClinicNum.Name = "textBoxClinicNum";
            this.textBoxClinicNum.Size = new System.Drawing.Size(190, 20);
            this.textBoxClinicNum.TabIndex = 0;
            // 
            // groupBoxOryo
            // 
            this.groupBoxOryo.Controls.Add(this.checkBoxOshinNashi);
            this.groupBoxOryo.Controls.Add(this.checkBoxOshinAri);
            this.groupBoxOryo.Location = new System.Drawing.Point(535, 211);
            this.groupBoxOryo.Name = "groupBoxOryo";
            this.groupBoxOryo.Size = new System.Drawing.Size(101, 46);
            this.groupBoxOryo.TabIndex = 25;
            this.groupBoxOryo.TabStop = false;
            this.groupBoxOryo.Text = "往療料";
            // 
            // checkBoxOshinNashi
            // 
            this.checkBoxOshinNashi.AutoSize = true;
            this.checkBoxOshinNashi.Checked = true;
            this.checkBoxOshinNashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOshinNashi.Location = new System.Drawing.Point(50, 21);
            this.checkBoxOshinNashi.Name = "checkBoxOshinNashi";
            this.checkBoxOshinNashi.Size = new System.Drawing.Size(45, 17);
            this.checkBoxOshinNashi.TabIndex = 1;
            this.checkBoxOshinNashi.Text = "なし";
            this.checkBoxOshinNashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxOshinAri
            // 
            this.checkBoxOshinAri.AutoSize = true;
            this.checkBoxOshinAri.Checked = true;
            this.checkBoxOshinAri.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOshinAri.Location = new System.Drawing.Point(5, 21);
            this.checkBoxOshinAri.Name = "checkBoxOshinAri";
            this.checkBoxOshinAri.Size = new System.Drawing.Size(44, 17);
            this.checkBoxOshinAri.TabIndex = 0;
            this.checkBoxOshinAri.Text = "あり";
            this.checkBoxOshinAri.UseVisualStyleBackColor = true;
            // 
            // groupBoxDrName
            // 
            this.groupBoxDrName.Controls.Add(this.textBoxDrName);
            this.groupBoxDrName.Location = new System.Drawing.Point(219, 107);
            this.groupBoxDrName.Name = "groupBoxDrName";
            this.groupBoxDrName.Size = new System.Drawing.Size(210, 46);
            this.groupBoxDrName.TabIndex = 30;
            this.groupBoxDrName.TabStop = false;
            this.groupBoxDrName.Text = "施術師名";
            // 
            // textBoxDrName
            // 
            this.textBoxDrName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxDrName.Location = new System.Drawing.Point(12, 18);
            this.textBoxDrName.Name = "textBoxDrName";
            this.textBoxDrName.Size = new System.Drawing.Size(190, 20);
            this.textBoxDrName.TabIndex = 0;
            // 
            // groupBoxDrCode
            // 
            this.groupBoxDrCode.Controls.Add(this.numBoxDr);
            this.groupBoxDrCode.Location = new System.Drawing.Point(3, 107);
            this.groupBoxDrCode.Name = "groupBoxDrCode";
            this.groupBoxDrCode.Size = new System.Drawing.Size(210, 46);
            this.groupBoxDrCode.TabIndex = 27;
            this.groupBoxDrCode.TabStop = false;
            this.groupBoxDrCode.Text = "施術師コード";
            // 
            // groupBoxType
            // 
            this.groupBoxType.Controls.Add(this.checkBoxMs);
            this.groupBoxType.Controls.Add(this.checkBoxJyu);
            this.groupBoxType.Controls.Add(this.checkBoxSin);
            this.groupBoxType.Location = new System.Drawing.Point(533, 55);
            this.groupBoxType.Name = "groupBoxType";
            this.groupBoxType.Size = new System.Drawing.Size(173, 46);
            this.groupBoxType.TabIndex = 28;
            this.groupBoxType.TabStop = false;
            this.groupBoxType.Text = "種別";
            // 
            // checkBoxMs
            // 
            this.checkBoxMs.AutoSize = true;
            this.checkBoxMs.Checked = true;
            this.checkBoxMs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMs.Location = new System.Drawing.Point(116, 20);
            this.checkBoxMs.Name = "checkBoxMs";
            this.checkBoxMs.Size = new System.Drawing.Size(55, 17);
            this.checkBoxMs.TabIndex = 2;
            this.checkBoxMs.Text = "あんま";
            this.checkBoxMs.UseVisualStyleBackColor = true;
            // 
            // checkBoxJyu
            // 
            this.checkBoxJyu.AutoSize = true;
            this.checkBoxJyu.Checked = true;
            this.checkBoxJyu.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxJyu.Location = new System.Drawing.Point(10, 20);
            this.checkBoxJyu.Name = "checkBoxJyu";
            this.checkBoxJyu.Size = new System.Drawing.Size(50, 17);
            this.checkBoxJyu.TabIndex = 0;
            this.checkBoxJyu.Text = "柔整";
            this.checkBoxJyu.UseVisualStyleBackColor = true;
            // 
            // checkBoxSin
            // 
            this.checkBoxSin.AutoSize = true;
            this.checkBoxSin.Checked = true;
            this.checkBoxSin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSin.Location = new System.Drawing.Point(63, 20);
            this.checkBoxSin.Name = "checkBoxSin";
            this.checkBoxSin.Size = new System.Drawing.Size(50, 17);
            this.checkBoxSin.TabIndex = 1;
            this.checkBoxSin.Text = "鍼灸";
            this.checkBoxSin.UseVisualStyleBackColor = true;
            // 
            // groupBoxShokai
            // 
            this.groupBoxShokai.Controls.Add(this.checkBoxShokaiNashi);
            this.groupBoxShokai.Controls.Add(this.checkBoxShokaiTaisho);
            this.groupBoxShokai.Location = new System.Drawing.Point(3, 315);
            this.groupBoxShokai.Name = "groupBoxShokai";
            this.groupBoxShokai.Size = new System.Drawing.Size(138, 46);
            this.groupBoxShokai.TabIndex = 31;
            this.groupBoxShokai.TabStop = false;
            this.groupBoxShokai.Text = "照会対象";
            // 
            // checkBoxShokaiNashi
            // 
            this.checkBoxShokaiNashi.AutoSize = true;
            this.checkBoxShokaiNashi.Checked = true;
            this.checkBoxShokaiNashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShokaiNashi.Location = new System.Drawing.Point(9, 22);
            this.checkBoxShokaiNashi.Name = "checkBoxShokaiNashi";
            this.checkBoxShokaiNashi.Size = new System.Drawing.Size(62, 17);
            this.checkBoxShokaiNashi.TabIndex = 0;
            this.checkBoxShokaiNashi.Text = "対象外";
            this.checkBoxShokaiNashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxShokaiTaisho
            // 
            this.checkBoxShokaiTaisho.AutoSize = true;
            this.checkBoxShokaiTaisho.Checked = true;
            this.checkBoxShokaiTaisho.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShokaiTaisho.Location = new System.Drawing.Point(82, 22);
            this.checkBoxShokaiTaisho.Name = "checkBoxShokaiTaisho";
            this.checkBoxShokaiTaisho.Size = new System.Drawing.Size(50, 17);
            this.checkBoxShokaiTaisho.TabIndex = 1;
            this.checkBoxShokaiTaisho.Text = "対象";
            this.checkBoxShokaiTaisho.UseVisualStyleBackColor = true;
            // 
            // groupBoxNum
            // 
            this.groupBoxNum.Controls.Add(this.numBoxNum);
            this.groupBoxNum.Location = new System.Drawing.Point(375, 3);
            this.groupBoxNum.Name = "groupBoxNum";
            this.groupBoxNum.Size = new System.Drawing.Size(210, 46);
            this.groupBoxNum.TabIndex = 26;
            this.groupBoxNum.TabStop = false;
            this.groupBoxNum.Text = "被保険者番号";
            // 
            // groupBoxHenrei
            // 
            this.groupBoxHenrei.Controls.Add(this.checkBoxHenNashi);
            this.groupBoxHenrei.Controls.Add(this.checkBoxHenAri);
            this.groupBoxHenrei.Location = new System.Drawing.Point(3, 263);
            this.groupBoxHenrei.Name = "groupBoxHenrei";
            this.groupBoxHenrei.Size = new System.Drawing.Size(101, 46);
            this.groupBoxHenrei.TabIndex = 32;
            this.groupBoxHenrei.TabStop = false;
            this.groupBoxHenrei.Text = "返戻";
            // 
            // checkBoxHenNashi
            // 
            this.checkBoxHenNashi.AutoSize = true;
            this.checkBoxHenNashi.Checked = true;
            this.checkBoxHenNashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHenNashi.Location = new System.Drawing.Point(51, 21);
            this.checkBoxHenNashi.Name = "checkBoxHenNashi";
            this.checkBoxHenNashi.Size = new System.Drawing.Size(45, 17);
            this.checkBoxHenNashi.TabIndex = 1;
            this.checkBoxHenNashi.Text = "なし";
            this.checkBoxHenNashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenAri
            // 
            this.checkBoxHenAri.AutoSize = true;
            this.checkBoxHenAri.Checked = true;
            this.checkBoxHenAri.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHenAri.Location = new System.Drawing.Point(6, 21);
            this.checkBoxHenAri.Name = "checkBoxHenAri";
            this.checkBoxHenAri.Size = new System.Drawing.Size(44, 17);
            this.checkBoxHenAri.TabIndex = 0;
            this.checkBoxHenAri.Text = "あり";
            this.checkBoxHenAri.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.groupBoxCYM);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxYM);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxNum);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxInsNum);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxName);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxKana);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxType);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxDrCode);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxDrName);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxClinicNum);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxClinicName);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxGroupNum);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxFusho);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxTotal);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxCharge);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxDays);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxFushoCount);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxOryo);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxHenrei);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxNewCont);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxAnmaCount);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxAnma);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxShokai);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxShokaiResult);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxDouisyo);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxSejutsuHoukokusyo);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxJoutaikinyusyo);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxDual);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxStartDate);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxFinishDate);
            this.flowLayoutPanel1.Controls.Add(this.groupBoxDouiDate);
            this.flowLayoutPanel1.Controls.Add(this.panelSearch);
            this.flowLayoutPanel1.Controls.Add(this.panelSeparator);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(732, 466);
            this.flowLayoutPanel1.TabIndex = 33;
            // 
            // groupBoxInsNum
            // 
            this.groupBoxInsNum.Controls.Add(this.numBoxInsNum);
            this.groupBoxInsNum.Location = new System.Drawing.Point(3, 55);
            this.groupBoxInsNum.Name = "groupBoxInsNum";
            this.groupBoxInsNum.Size = new System.Drawing.Size(210, 46);
            this.groupBoxInsNum.TabIndex = 26;
            this.groupBoxInsNum.TabStop = false;
            this.groupBoxInsNum.Text = "保険者番号";
            // 
            // groupBoxName
            // 
            this.groupBoxName.Controls.Add(this.textBoxName);
            this.groupBoxName.Location = new System.Drawing.Point(219, 55);
            this.groupBoxName.Name = "groupBoxName";
            this.groupBoxName.Size = new System.Drawing.Size(151, 46);
            this.groupBoxName.TabIndex = 30;
            this.groupBoxName.TabStop = false;
            this.groupBoxName.Text = "漢字氏名";
            // 
            // textBoxName
            // 
            this.textBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxName.Location = new System.Drawing.Point(12, 16);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(132, 20);
            this.textBoxName.TabIndex = 36;
            // 
            // groupBoxKana
            // 
            this.groupBoxKana.Controls.Add(this.textBoxKana);
            this.groupBoxKana.Location = new System.Drawing.Point(376, 55);
            this.groupBoxKana.Name = "groupBoxKana";
            this.groupBoxKana.Size = new System.Drawing.Size(151, 46);
            this.groupBoxKana.TabIndex = 30;
            this.groupBoxKana.TabStop = false;
            this.groupBoxKana.Text = "カナ氏名";
            // 
            // textBoxKana
            // 
            this.textBoxKana.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxKana.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxKana.Location = new System.Drawing.Point(12, 16);
            this.textBoxKana.Name = "textBoxKana";
            this.textBoxKana.Size = new System.Drawing.Size(132, 20);
            this.textBoxKana.TabIndex = 36;
            // 
            // groupBoxClinicName
            // 
            this.groupBoxClinicName.Controls.Add(this.textBoxClinicName);
            this.groupBoxClinicName.Location = new System.Drawing.Point(3, 159);
            this.groupBoxClinicName.Name = "groupBoxClinicName";
            this.groupBoxClinicName.Size = new System.Drawing.Size(210, 46);
            this.groupBoxClinicName.TabIndex = 29;
            this.groupBoxClinicName.TabStop = false;
            this.groupBoxClinicName.Text = "施術所名";
            // 
            // textBoxClinicName
            // 
            this.textBoxClinicName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxClinicName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxClinicName.Location = new System.Drawing.Point(12, 18);
            this.textBoxClinicName.Name = "textBoxClinicName";
            this.textBoxClinicName.Size = new System.Drawing.Size(190, 20);
            this.textBoxClinicName.TabIndex = 0;
            // 
            // groupBoxGroupNum
            // 
            this.groupBoxGroupNum.Controls.Add(this.numBoxGroup);
            this.groupBoxGroupNum.Location = new System.Drawing.Point(219, 159);
            this.groupBoxGroupNum.Name = "groupBoxGroupNum";
            this.groupBoxGroupNum.Size = new System.Drawing.Size(210, 46);
            this.groupBoxGroupNum.TabIndex = 30;
            this.groupBoxGroupNum.TabStop = false;
            this.groupBoxGroupNum.Text = "グループ番号";
            // 
            // groupBoxFusho
            // 
            this.groupBoxFusho.Controls.Add(this.textBoxFusho);
            this.groupBoxFusho.Location = new System.Drawing.Point(435, 159);
            this.groupBoxFusho.Name = "groupBoxFusho";
            this.groupBoxFusho.Size = new System.Drawing.Size(210, 46);
            this.groupBoxFusho.TabIndex = 30;
            this.groupBoxFusho.TabStop = false;
            this.groupBoxFusho.Text = "負傷名/病名";
            // 
            // textBoxFusho
            // 
            this.textBoxFusho.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFusho.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBoxFusho.Location = new System.Drawing.Point(7, 18);
            this.textBoxFusho.Name = "textBoxFusho";
            this.textBoxFusho.Size = new System.Drawing.Size(197, 20);
            this.textBoxFusho.TabIndex = 1;
            // 
            // groupBoxCharge
            // 
            this.groupBoxCharge.Controls.Add(this.label3);
            this.groupBoxCharge.Controls.Add(this.numBoxChargeMax);
            this.groupBoxCharge.Controls.Add(this.numBoxChargeMin);
            this.groupBoxCharge.Location = new System.Drawing.Point(167, 211);
            this.groupBoxCharge.Name = "groupBoxCharge";
            this.groupBoxCharge.Size = new System.Drawing.Size(158, 46);
            this.groupBoxCharge.TabIndex = 24;
            this.groupBoxCharge.TabStop = false;
            this.groupBoxCharge.Text = "請求金額";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "～";
            // 
            // groupBoxDays
            // 
            this.groupBoxDays.Controls.Add(this.label1);
            this.groupBoxDays.Controls.Add(this.numBoxDaysMax);
            this.groupBoxDays.Controls.Add(this.numBoxDaysMin);
            this.groupBoxDays.Location = new System.Drawing.Point(331, 211);
            this.groupBoxDays.Name = "groupBoxDays";
            this.groupBoxDays.Size = new System.Drawing.Size(102, 46);
            this.groupBoxDays.TabIndex = 24;
            this.groupBoxDays.TabStop = false;
            this.groupBoxDays.Text = "実日数";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "～";
            // 
            // groupBoxFushoCount
            // 
            this.groupBoxFushoCount.Controls.Add(this.label4);
            this.groupBoxFushoCount.Controls.Add(this.numBoxFushoMax);
            this.groupBoxFushoCount.Controls.Add(this.numBoxFushoMin);
            this.groupBoxFushoCount.Location = new System.Drawing.Point(439, 211);
            this.groupBoxFushoCount.Name = "groupBoxFushoCount";
            this.groupBoxFushoCount.Size = new System.Drawing.Size(90, 46);
            this.groupBoxFushoCount.TabIndex = 24;
            this.groupBoxFushoCount.TabStop = false;
            this.groupBoxFushoCount.Text = "負傷/病名数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "～";
            // 
            // groupBoxNewCont
            // 
            this.groupBoxNewCont.Controls.Add(this.checkBoxNewContNew);
            this.groupBoxNewCont.Controls.Add(this.checkBoxNewContCont);
            this.groupBoxNewCont.Location = new System.Drawing.Point(110, 263);
            this.groupBoxNewCont.Name = "groupBoxNewCont";
            this.groupBoxNewCont.Size = new System.Drawing.Size(118, 46);
            this.groupBoxNewCont.TabIndex = 31;
            this.groupBoxNewCont.TabStop = false;
            this.groupBoxNewCont.Text = "新規継続";
            // 
            // checkBoxNewContNew
            // 
            this.checkBoxNewContNew.AutoSize = true;
            this.checkBoxNewContNew.Checked = true;
            this.checkBoxNewContNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNewContNew.Location = new System.Drawing.Point(9, 22);
            this.checkBoxNewContNew.Name = "checkBoxNewContNew";
            this.checkBoxNewContNew.Size = new System.Drawing.Size(50, 17);
            this.checkBoxNewContNew.TabIndex = 0;
            this.checkBoxNewContNew.Text = "新規";
            this.checkBoxNewContNew.UseVisualStyleBackColor = true;
            // 
            // checkBoxNewContCont
            // 
            this.checkBoxNewContCont.AutoSize = true;
            this.checkBoxNewContCont.Checked = true;
            this.checkBoxNewContCont.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNewContCont.Location = new System.Drawing.Point(64, 22);
            this.checkBoxNewContCont.Name = "checkBoxNewContCont";
            this.checkBoxNewContCont.Size = new System.Drawing.Size(50, 17);
            this.checkBoxNewContCont.TabIndex = 1;
            this.checkBoxNewContCont.Text = "継続";
            this.checkBoxNewContCont.UseVisualStyleBackColor = true;
            // 
            // groupBoxAnmaCount
            // 
            this.groupBoxAnmaCount.Controls.Add(this.label5);
            this.groupBoxAnmaCount.Controls.Add(this.numBoxAnmaMax);
            this.groupBoxAnmaCount.Controls.Add(this.numBoxAnmaMin);
            this.groupBoxAnmaCount.Location = new System.Drawing.Point(234, 263);
            this.groupBoxAnmaCount.Name = "groupBoxAnmaCount";
            this.groupBoxAnmaCount.Size = new System.Drawing.Size(90, 46);
            this.groupBoxAnmaCount.TabIndex = 24;
            this.groupBoxAnmaCount.TabStop = false;
            this.groupBoxAnmaCount.Text = "あんま局所数";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "～";
            // 
            // groupBoxAnma
            // 
            this.groupBoxAnma.Controls.Add(this.checkBoxLeftLower);
            this.groupBoxAnma.Controls.Add(this.checkBoxRightLower);
            this.groupBoxAnma.Controls.Add(this.checkBoxRightUpper);
            this.groupBoxAnma.Controls.Add(this.checkBoxLeftUpper);
            this.groupBoxAnma.Controls.Add(this.checkBoxBody);
            this.groupBoxAnma.Location = new System.Drawing.Point(330, 263);
            this.groupBoxAnma.Name = "groupBoxAnma";
            this.groupBoxAnma.Size = new System.Drawing.Size(269, 46);
            this.groupBoxAnma.TabIndex = 32;
            this.groupBoxAnma.TabStop = false;
            this.groupBoxAnma.Text = "あんま局所";
            // 
            // checkBoxLeftLower
            // 
            this.checkBoxLeftLower.AutoSize = true;
            this.checkBoxLeftLower.Checked = true;
            this.checkBoxLeftLower.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBoxLeftLower.Location = new System.Drawing.Point(218, 21);
            this.checkBoxLeftLower.Name = "checkBoxLeftLower";
            this.checkBoxLeftLower.Size = new System.Drawing.Size(50, 17);
            this.checkBoxLeftLower.TabIndex = 1;
            this.checkBoxLeftLower.Text = "左下";
            this.checkBoxLeftLower.ThreeState = true;
            this.checkBoxLeftLower.UseVisualStyleBackColor = true;
            // 
            // checkBoxRightLower
            // 
            this.checkBoxRightLower.AutoSize = true;
            this.checkBoxRightLower.Checked = true;
            this.checkBoxRightLower.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBoxRightLower.Location = new System.Drawing.Point(166, 21);
            this.checkBoxRightLower.Name = "checkBoxRightLower";
            this.checkBoxRightLower.Size = new System.Drawing.Size(50, 17);
            this.checkBoxRightLower.TabIndex = 1;
            this.checkBoxRightLower.Text = "右下";
            this.checkBoxRightLower.ThreeState = true;
            this.checkBoxRightLower.UseVisualStyleBackColor = true;
            // 
            // checkBoxRightUpper
            // 
            this.checkBoxRightUpper.AutoSize = true;
            this.checkBoxRightUpper.Checked = true;
            this.checkBoxRightUpper.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBoxRightUpper.Location = new System.Drawing.Point(62, 21);
            this.checkBoxRightUpper.Name = "checkBoxRightUpper";
            this.checkBoxRightUpper.Size = new System.Drawing.Size(50, 17);
            this.checkBoxRightUpper.TabIndex = 1;
            this.checkBoxRightUpper.Text = "右上";
            this.checkBoxRightUpper.ThreeState = true;
            this.checkBoxRightUpper.UseVisualStyleBackColor = true;
            // 
            // checkBoxLeftUpper
            // 
            this.checkBoxLeftUpper.AutoSize = true;
            this.checkBoxLeftUpper.Checked = true;
            this.checkBoxLeftUpper.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBoxLeftUpper.Location = new System.Drawing.Point(114, 21);
            this.checkBoxLeftUpper.Name = "checkBoxLeftUpper";
            this.checkBoxLeftUpper.Size = new System.Drawing.Size(50, 17);
            this.checkBoxLeftUpper.TabIndex = 0;
            this.checkBoxLeftUpper.Text = "左上";
            this.checkBoxLeftUpper.ThreeState = true;
            this.checkBoxLeftUpper.UseVisualStyleBackColor = true;
            // 
            // checkBoxBody
            // 
            this.checkBoxBody.AutoSize = true;
            this.checkBoxBody.Checked = true;
            this.checkBoxBody.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBoxBody.Location = new System.Drawing.Point(10, 21);
            this.checkBoxBody.Name = "checkBoxBody";
            this.checkBoxBody.Size = new System.Drawing.Size(50, 17);
            this.checkBoxBody.TabIndex = 0;
            this.checkBoxBody.Text = "体幹";
            this.checkBoxBody.ThreeState = true;
            this.checkBoxBody.UseVisualStyleBackColor = true;
            // 
            // groupBoxShokaiResult
            // 
            this.groupBoxShokaiResult.Controls.Add(this.checkBoxShokaiSouiNashi);
            this.groupBoxShokaiResult.Controls.Add(this.checkBoxShokaiSouiAri);
            this.groupBoxShokaiResult.Location = new System.Drawing.Point(147, 315);
            this.groupBoxShokaiResult.Name = "groupBoxShokaiResult";
            this.groupBoxShokaiResult.Size = new System.Drawing.Size(155, 46);
            this.groupBoxShokaiResult.TabIndex = 31;
            this.groupBoxShokaiResult.TabStop = false;
            this.groupBoxShokaiResult.Text = "照会結果";
            // 
            // checkBoxShokaiSouiNashi
            // 
            this.checkBoxShokaiSouiNashi.AutoSize = true;
            this.checkBoxShokaiSouiNashi.Checked = true;
            this.checkBoxShokaiSouiNashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShokaiSouiNashi.Location = new System.Drawing.Point(81, 22);
            this.checkBoxShokaiSouiNashi.Name = "checkBoxShokaiSouiNashi";
            this.checkBoxShokaiSouiNashi.Size = new System.Drawing.Size(69, 17);
            this.checkBoxShokaiSouiNashi.TabIndex = 3;
            this.checkBoxShokaiSouiNashi.Text = "相違なし";
            this.checkBoxShokaiSouiNashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxShokaiSouiAri
            // 
            this.checkBoxShokaiSouiAri.AutoSize = true;
            this.checkBoxShokaiSouiAri.Checked = true;
            this.checkBoxShokaiSouiAri.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShokaiSouiAri.Location = new System.Drawing.Point(9, 22);
            this.checkBoxShokaiSouiAri.Name = "checkBoxShokaiSouiAri";
            this.checkBoxShokaiSouiAri.Size = new System.Drawing.Size(68, 17);
            this.checkBoxShokaiSouiAri.TabIndex = 2;
            this.checkBoxShokaiSouiAri.Text = "相違あり";
            this.checkBoxShokaiSouiAri.UseVisualStyleBackColor = true;
            // 
            // groupBoxDouisyo
            // 
            this.groupBoxDouisyo.Controls.Add(this.checkBoxDouisyoNashi);
            this.groupBoxDouisyo.Controls.Add(this.checkBoxDouisyoAri);
            this.groupBoxDouisyo.Location = new System.Drawing.Point(308, 315);
            this.groupBoxDouisyo.Name = "groupBoxDouisyo";
            this.groupBoxDouisyo.Size = new System.Drawing.Size(101, 46);
            this.groupBoxDouisyo.TabIndex = 25;
            this.groupBoxDouisyo.TabStop = false;
            this.groupBoxDouisyo.Text = "同意書あり";
            // 
            // checkBoxDouisyoNashi
            // 
            this.checkBoxDouisyoNashi.AutoSize = true;
            this.checkBoxDouisyoNashi.Checked = true;
            this.checkBoxDouisyoNashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDouisyoNashi.Location = new System.Drawing.Point(51, 21);
            this.checkBoxDouisyoNashi.Name = "checkBoxDouisyoNashi";
            this.checkBoxDouisyoNashi.Size = new System.Drawing.Size(45, 17);
            this.checkBoxDouisyoNashi.TabIndex = 1;
            this.checkBoxDouisyoNashi.Text = "なし";
            this.checkBoxDouisyoNashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxDouisyoAri
            // 
            this.checkBoxDouisyoAri.AutoSize = true;
            this.checkBoxDouisyoAri.Checked = true;
            this.checkBoxDouisyoAri.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDouisyoAri.Location = new System.Drawing.Point(6, 21);
            this.checkBoxDouisyoAri.Name = "checkBoxDouisyoAri";
            this.checkBoxDouisyoAri.Size = new System.Drawing.Size(44, 17);
            this.checkBoxDouisyoAri.TabIndex = 0;
            this.checkBoxDouisyoAri.Text = "あり";
            this.checkBoxDouisyoAri.UseVisualStyleBackColor = true;
            // 
            // groupBoxSejutsuHoukokusyo
            // 
            this.groupBoxSejutsuHoukokusyo.Controls.Add(this.checkBoxSejutsuHoukokusyoNashi);
            this.groupBoxSejutsuHoukokusyo.Controls.Add(this.checkBoxSejutsuHoukokusyoAri);
            this.groupBoxSejutsuHoukokusyo.Location = new System.Drawing.Point(415, 315);
            this.groupBoxSejutsuHoukokusyo.Name = "groupBoxSejutsuHoukokusyo";
            this.groupBoxSejutsuHoukokusyo.Size = new System.Drawing.Size(101, 46);
            this.groupBoxSejutsuHoukokusyo.TabIndex = 25;
            this.groupBoxSejutsuHoukokusyo.TabStop = false;
            this.groupBoxSejutsuHoukokusyo.Text = "施術報告書あり";
            // 
            // checkBoxSejutsuHoukokusyoNashi
            // 
            this.checkBoxSejutsuHoukokusyoNashi.AutoSize = true;
            this.checkBoxSejutsuHoukokusyoNashi.Checked = true;
            this.checkBoxSejutsuHoukokusyoNashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSejutsuHoukokusyoNashi.Location = new System.Drawing.Point(51, 21);
            this.checkBoxSejutsuHoukokusyoNashi.Name = "checkBoxSejutsuHoukokusyoNashi";
            this.checkBoxSejutsuHoukokusyoNashi.Size = new System.Drawing.Size(45, 17);
            this.checkBoxSejutsuHoukokusyoNashi.TabIndex = 1;
            this.checkBoxSejutsuHoukokusyoNashi.Text = "なし";
            this.checkBoxSejutsuHoukokusyoNashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxSejutsuHoukokusyoAri
            // 
            this.checkBoxSejutsuHoukokusyoAri.AutoSize = true;
            this.checkBoxSejutsuHoukokusyoAri.Checked = true;
            this.checkBoxSejutsuHoukokusyoAri.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSejutsuHoukokusyoAri.Location = new System.Drawing.Point(7, 21);
            this.checkBoxSejutsuHoukokusyoAri.Name = "checkBoxSejutsuHoukokusyoAri";
            this.checkBoxSejutsuHoukokusyoAri.Size = new System.Drawing.Size(44, 17);
            this.checkBoxSejutsuHoukokusyoAri.TabIndex = 0;
            this.checkBoxSejutsuHoukokusyoAri.Text = "あり";
            this.checkBoxSejutsuHoukokusyoAri.UseVisualStyleBackColor = true;
            // 
            // groupBoxJoutaikinyusyo
            // 
            this.groupBoxJoutaikinyusyo.Controls.Add(this.checkBoxJoutaikinyusyoNashi);
            this.groupBoxJoutaikinyusyo.Controls.Add(this.checkBoxJoutaikinyusyoAri);
            this.groupBoxJoutaikinyusyo.Location = new System.Drawing.Point(522, 315);
            this.groupBoxJoutaikinyusyo.Name = "groupBoxJoutaikinyusyo";
            this.groupBoxJoutaikinyusyo.Size = new System.Drawing.Size(101, 46);
            this.groupBoxJoutaikinyusyo.TabIndex = 25;
            this.groupBoxJoutaikinyusyo.TabStop = false;
            this.groupBoxJoutaikinyusyo.Text = "状態記入書あり";
            // 
            // checkBoxJoutaikinyusyoNashi
            // 
            this.checkBoxJoutaikinyusyoNashi.AutoSize = true;
            this.checkBoxJoutaikinyusyoNashi.Checked = true;
            this.checkBoxJoutaikinyusyoNashi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxJoutaikinyusyoNashi.Location = new System.Drawing.Point(51, 21);
            this.checkBoxJoutaikinyusyoNashi.Name = "checkBoxJoutaikinyusyoNashi";
            this.checkBoxJoutaikinyusyoNashi.Size = new System.Drawing.Size(45, 17);
            this.checkBoxJoutaikinyusyoNashi.TabIndex = 1;
            this.checkBoxJoutaikinyusyoNashi.Text = "なし";
            this.checkBoxJoutaikinyusyoNashi.UseVisualStyleBackColor = true;
            // 
            // checkBoxJoutaikinyusyoAri
            // 
            this.checkBoxJoutaikinyusyoAri.AutoSize = true;
            this.checkBoxJoutaikinyusyoAri.Checked = true;
            this.checkBoxJoutaikinyusyoAri.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxJoutaikinyusyoAri.Location = new System.Drawing.Point(7, 21);
            this.checkBoxJoutaikinyusyoAri.Name = "checkBoxJoutaikinyusyoAri";
            this.checkBoxJoutaikinyusyoAri.Size = new System.Drawing.Size(44, 17);
            this.checkBoxJoutaikinyusyoAri.TabIndex = 0;
            this.checkBoxJoutaikinyusyoAri.Text = "あり";
            this.checkBoxJoutaikinyusyoAri.UseVisualStyleBackColor = true;
            // 
            // groupBoxDual
            // 
            this.groupBoxDual.Controls.Add(this.checkBoxDual);
            this.groupBoxDual.Location = new System.Drawing.Point(629, 315);
            this.groupBoxDual.Name = "groupBoxDual";
            this.groupBoxDual.Size = new System.Drawing.Size(83, 46);
            this.groupBoxDual.TabIndex = 35;
            this.groupBoxDual.TabStop = false;
            this.groupBoxDual.Text = "複数種別";
            // 
            // checkBoxDual
            // 
            this.checkBoxDual.AutoSize = true;
            this.checkBoxDual.Location = new System.Drawing.Point(5, 22);
            this.checkBoxDual.Name = "checkBoxDual";
            this.checkBoxDual.Size = new System.Drawing.Size(74, 17);
            this.checkBoxDual.TabIndex = 9;
            this.checkBoxDual.Text = "複数受診";
            this.checkBoxDual.UseVisualStyleBackColor = true;
            // 
            // groupBoxStartDate
            // 
            this.groupBoxStartDate.Controls.Add(this.dateBoxStartTo);
            this.groupBoxStartDate.Controls.Add(this.dateBoxStartFrom);
            this.groupBoxStartDate.Controls.Add(this.label9);
            this.groupBoxStartDate.Location = new System.Drawing.Point(3, 367);
            this.groupBoxStartDate.Name = "groupBoxStartDate";
            this.groupBoxStartDate.Size = new System.Drawing.Size(228, 46);
            this.groupBoxStartDate.TabIndex = 22;
            this.groupBoxStartDate.TabStop = false;
            this.groupBoxStartDate.Text = "開始年月日";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(107, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "～";
            // 
            // groupBoxFinishDate
            // 
            this.groupBoxFinishDate.Controls.Add(this.dateBoxFinishTo);
            this.groupBoxFinishDate.Controls.Add(this.dateBoxFinishFrom);
            this.groupBoxFinishDate.Controls.Add(this.label10);
            this.groupBoxFinishDate.Location = new System.Drawing.Point(237, 367);
            this.groupBoxFinishDate.Name = "groupBoxFinishDate";
            this.groupBoxFinishDate.Size = new System.Drawing.Size(228, 46);
            this.groupBoxFinishDate.TabIndex = 22;
            this.groupBoxFinishDate.TabStop = false;
            this.groupBoxFinishDate.Text = "終了年月日";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(107, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "～";
            // 
            // groupBoxDouiDate
            // 
            this.groupBoxDouiDate.Controls.Add(this.dateBoxDouiTo);
            this.groupBoxDouiDate.Controls.Add(this.dateBoxDouiFrom);
            this.groupBoxDouiDate.Controls.Add(this.label8);
            this.groupBoxDouiDate.Location = new System.Drawing.Point(471, 367);
            this.groupBoxDouiDate.Name = "groupBoxDouiDate";
            this.groupBoxDouiDate.Size = new System.Drawing.Size(228, 46);
            this.groupBoxDouiDate.TabIndex = 22;
            this.groupBoxDouiDate.TabStop = false;
            this.groupBoxDouiDate.Text = "同意年月日";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(107, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "～";
            // 
            // panelSearch
            // 
            this.panelSearch.Controls.Add(this.button1);
            this.panelSearch.Controls.Add(this.checkBox1);
            this.panelSearch.Location = new System.Drawing.Point(3, 419);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(149, 35);
            this.panelSearch.TabIndex = 34;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(67, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 25);
            this.button1.TabIndex = 33;
            this.button1.Text = "検索 (F12)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.Location = new System.Drawing.Point(8, 8);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(53, 23);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "拡張";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panelSeparator
            // 
            this.panelSeparator.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panelSeparator.Location = new System.Drawing.Point(3, 460);
            this.panelSeparator.Name = "panelSeparator";
            this.panelSeparator.Size = new System.Drawing.Size(700, 3);
            this.panelSeparator.TabIndex = 36;
            // 
            // monthBoxS2
            // 
            this.monthBoxS2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.monthBoxS2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.monthBoxS2.Location = new System.Drawing.Point(12, 18);
            this.monthBoxS2.Name = "monthBoxS2";
            this.monthBoxS2.SetIndexToMonth = false;
            this.monthBoxS2.Size = new System.Drawing.Size(70, 19);
            this.monthBoxS2.TabIndex = 0;
            // 
            // monthBoxE2
            // 
            this.monthBoxE2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.monthBoxE2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.monthBoxE2.Location = new System.Drawing.Point(102, 18);
            this.monthBoxE2.Name = "monthBoxE2";
            this.monthBoxE2.SetIndexToMonth = false;
            this.monthBoxE2.Size = new System.Drawing.Size(70, 19);
            this.monthBoxE2.TabIndex = 2;
            // 
            // monthBoxS1
            // 
            this.monthBoxS1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.monthBoxS1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.monthBoxS1.Location = new System.Drawing.Point(12, 18);
            this.monthBoxS1.Name = "monthBoxS1";
            this.monthBoxS1.SetIndexToMonth = false;
            this.monthBoxS1.Size = new System.Drawing.Size(70, 19);
            this.monthBoxS1.TabIndex = 0;
            // 
            // monthBoxE1
            // 
            this.monthBoxE1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.monthBoxE1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.monthBoxE1.Location = new System.Drawing.Point(102, 18);
            this.monthBoxE1.Name = "monthBoxE1";
            this.monthBoxE1.SetIndexToMonth = false;
            this.monthBoxE1.Size = new System.Drawing.Size(70, 19);
            this.monthBoxE1.TabIndex = 2;
            // 
            // numBoxNum
            // 
            this.numBoxNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numBoxNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxNum.Location = new System.Drawing.Point(12, 18);
            this.numBoxNum.Name = "numBoxNum";
            this.numBoxNum.Size = new System.Drawing.Size(190, 20);
            this.numBoxNum.Space = true;
            this.numBoxNum.TabIndex = 0;
            // 
            // numBoxInsNum
            // 
            this.numBoxInsNum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numBoxInsNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxInsNum.Location = new System.Drawing.Point(12, 18);
            this.numBoxInsNum.Name = "numBoxInsNum";
            this.numBoxInsNum.Size = new System.Drawing.Size(190, 20);
            this.numBoxInsNum.Space = true;
            this.numBoxInsNum.TabIndex = 0;
            // 
            // numBoxDr
            // 
            this.numBoxDr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numBoxDr.Hyphen = true;
            this.numBoxDr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxDr.Location = new System.Drawing.Point(12, 18);
            this.numBoxDr.Name = "numBoxDr";
            this.numBoxDr.Size = new System.Drawing.Size(190, 20);
            this.numBoxDr.Space = true;
            this.numBoxDr.TabIndex = 0;
            // 
            // numBoxGroup
            // 
            this.numBoxGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numBoxGroup.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxGroup.Location = new System.Drawing.Point(12, 18);
            this.numBoxGroup.Name = "numBoxGroup";
            this.numBoxGroup.Size = new System.Drawing.Size(190, 20);
            this.numBoxGroup.Space = true;
            this.numBoxGroup.TabIndex = 35;
            // 
            // numBoxTotalMax
            // 
            this.numBoxTotalMax.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxTotalMax.Location = new System.Drawing.Point(89, 18);
            this.numBoxTotalMax.MaxLength = 10;
            this.numBoxTotalMax.Name = "numBoxTotalMax";
            this.numBoxTotalMax.Size = new System.Drawing.Size(60, 20);
            this.numBoxTotalMax.Space = true;
            this.numBoxTotalMax.TabIndex = 2;
            // 
            // numBoxTotalMin
            // 
            this.numBoxTotalMin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxTotalMin.Location = new System.Drawing.Point(12, 18);
            this.numBoxTotalMin.MaxLength = 10;
            this.numBoxTotalMin.Name = "numBoxTotalMin";
            this.numBoxTotalMin.Size = new System.Drawing.Size(60, 20);
            this.numBoxTotalMin.Space = true;
            this.numBoxTotalMin.TabIndex = 0;
            // 
            // numBoxChargeMax
            // 
            this.numBoxChargeMax.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxChargeMax.Location = new System.Drawing.Point(89, 18);
            this.numBoxChargeMax.MaxLength = 10;
            this.numBoxChargeMax.Name = "numBoxChargeMax";
            this.numBoxChargeMax.Size = new System.Drawing.Size(60, 20);
            this.numBoxChargeMax.Space = true;
            this.numBoxChargeMax.TabIndex = 2;
            // 
            // numBoxChargeMin
            // 
            this.numBoxChargeMin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxChargeMin.Location = new System.Drawing.Point(12, 18);
            this.numBoxChargeMin.MaxLength = 10;
            this.numBoxChargeMin.Name = "numBoxChargeMin";
            this.numBoxChargeMin.Size = new System.Drawing.Size(60, 20);
            this.numBoxChargeMin.Space = true;
            this.numBoxChargeMin.TabIndex = 0;
            // 
            // numBoxDaysMax
            // 
            this.numBoxDaysMax.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxDaysMax.Location = new System.Drawing.Point(61, 18);
            this.numBoxDaysMax.MaxLength = 2;
            this.numBoxDaysMax.Name = "numBoxDaysMax";
            this.numBoxDaysMax.Size = new System.Drawing.Size(32, 20);
            this.numBoxDaysMax.Space = true;
            this.numBoxDaysMax.TabIndex = 2;
            // 
            // numBoxDaysMin
            // 
            this.numBoxDaysMin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxDaysMin.Location = new System.Drawing.Point(12, 18);
            this.numBoxDaysMin.MaxLength = 2;
            this.numBoxDaysMin.Name = "numBoxDaysMin";
            this.numBoxDaysMin.Size = new System.Drawing.Size(32, 20);
            this.numBoxDaysMin.Space = true;
            this.numBoxDaysMin.TabIndex = 0;
            // 
            // numBoxFushoMax
            // 
            this.numBoxFushoMax.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxFushoMax.Location = new System.Drawing.Point(55, 18);
            this.numBoxFushoMax.MaxLength = 1;
            this.numBoxFushoMax.Name = "numBoxFushoMax";
            this.numBoxFushoMax.Size = new System.Drawing.Size(26, 20);
            this.numBoxFushoMax.Space = true;
            this.numBoxFushoMax.TabIndex = 2;
            // 
            // numBoxFushoMin
            // 
            this.numBoxFushoMin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxFushoMin.Location = new System.Drawing.Point(12, 18);
            this.numBoxFushoMin.MaxLength = 1;
            this.numBoxFushoMin.Name = "numBoxFushoMin";
            this.numBoxFushoMin.Size = new System.Drawing.Size(26, 20);
            this.numBoxFushoMin.Space = true;
            this.numBoxFushoMin.TabIndex = 0;
            // 
            // numBoxAnmaMax
            // 
            this.numBoxAnmaMax.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxAnmaMax.Location = new System.Drawing.Point(55, 18);
            this.numBoxAnmaMax.MaxLength = 1;
            this.numBoxAnmaMax.Name = "numBoxAnmaMax";
            this.numBoxAnmaMax.Size = new System.Drawing.Size(26, 20);
            this.numBoxAnmaMax.Space = true;
            this.numBoxAnmaMax.TabIndex = 2;
            // 
            // numBoxAnmaMin
            // 
            this.numBoxAnmaMin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBoxAnmaMin.Location = new System.Drawing.Point(12, 18);
            this.numBoxAnmaMin.MaxLength = 1;
            this.numBoxAnmaMin.Name = "numBoxAnmaMin";
            this.numBoxAnmaMin.Size = new System.Drawing.Size(26, 20);
            this.numBoxAnmaMin.Space = true;
            this.numBoxAnmaMin.TabIndex = 0;
            // 
            // dateBoxStartTo
            // 
            this.dateBoxStartTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxStartTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxStartTo.Location = new System.Drawing.Point(125, 18);
            this.dateBoxStartTo.Name = "dateBoxStartTo";
            this.dateBoxStartTo.Size = new System.Drawing.Size(93, 19);
            this.dateBoxStartTo.TabIndex = 2;
            // 
            // dateBoxStartFrom
            // 
            this.dateBoxStartFrom.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxStartFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxStartFrom.Location = new System.Drawing.Point(12, 18);
            this.dateBoxStartFrom.Name = "dateBoxStartFrom";
            this.dateBoxStartFrom.Size = new System.Drawing.Size(93, 19);
            this.dateBoxStartFrom.TabIndex = 0;
            // 
            // dateBoxFinishTo
            // 
            this.dateBoxFinishTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxFinishTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxFinishTo.Location = new System.Drawing.Point(125, 18);
            this.dateBoxFinishTo.Name = "dateBoxFinishTo";
            this.dateBoxFinishTo.Size = new System.Drawing.Size(93, 19);
            this.dateBoxFinishTo.TabIndex = 2;
            // 
            // dateBoxFinishFrom
            // 
            this.dateBoxFinishFrom.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxFinishFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxFinishFrom.Location = new System.Drawing.Point(12, 18);
            this.dateBoxFinishFrom.Name = "dateBoxFinishFrom";
            this.dateBoxFinishFrom.Size = new System.Drawing.Size(93, 19);
            this.dateBoxFinishFrom.TabIndex = 0;
            // 
            // dateBoxDouiTo
            // 
            this.dateBoxDouiTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxDouiTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxDouiTo.Location = new System.Drawing.Point(125, 18);
            this.dateBoxDouiTo.Name = "dateBoxDouiTo";
            this.dateBoxDouiTo.Size = new System.Drawing.Size(93, 19);
            this.dateBoxDouiTo.TabIndex = 2;
            // 
            // dateBoxDouiFrom
            // 
            this.dateBoxDouiFrom.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxDouiFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxDouiFrom.Location = new System.Drawing.Point(12, 18);
            this.dateBoxDouiFrom.Name = "dateBoxDouiFrom";
            this.dateBoxDouiFrom.Size = new System.Drawing.Size(93, 19);
            this.dateBoxDouiFrom.TabIndex = 0;
            // 
            // WhereControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "WhereControl";
            this.Size = new System.Drawing.Size(732, 466);
            this.Load += new System.EventHandler(this.WhereControl_Load);
            this.groupBoxYM.ResumeLayout(false);
            this.groupBoxYM.PerformLayout();
            this.groupBoxCYM.ResumeLayout(false);
            this.groupBoxCYM.PerformLayout();
            this.groupBoxTotal.ResumeLayout(false);
            this.groupBoxTotal.PerformLayout();
            this.groupBoxClinicNum.ResumeLayout(false);
            this.groupBoxClinicNum.PerformLayout();
            this.groupBoxOryo.ResumeLayout(false);
            this.groupBoxOryo.PerformLayout();
            this.groupBoxDrName.ResumeLayout(false);
            this.groupBoxDrName.PerformLayout();
            this.groupBoxDrCode.ResumeLayout(false);
            this.groupBoxDrCode.PerformLayout();
            this.groupBoxType.ResumeLayout(false);
            this.groupBoxType.PerformLayout();
            this.groupBoxShokai.ResumeLayout(false);
            this.groupBoxShokai.PerformLayout();
            this.groupBoxNum.ResumeLayout(false);
            this.groupBoxNum.PerformLayout();
            this.groupBoxHenrei.ResumeLayout(false);
            this.groupBoxHenrei.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBoxInsNum.ResumeLayout(false);
            this.groupBoxInsNum.PerformLayout();
            this.groupBoxName.ResumeLayout(false);
            this.groupBoxName.PerformLayout();
            this.groupBoxKana.ResumeLayout(false);
            this.groupBoxKana.PerformLayout();
            this.groupBoxClinicName.ResumeLayout(false);
            this.groupBoxClinicName.PerformLayout();
            this.groupBoxGroupNum.ResumeLayout(false);
            this.groupBoxGroupNum.PerformLayout();
            this.groupBoxFusho.ResumeLayout(false);
            this.groupBoxFusho.PerformLayout();
            this.groupBoxCharge.ResumeLayout(false);
            this.groupBoxCharge.PerformLayout();
            this.groupBoxDays.ResumeLayout(false);
            this.groupBoxDays.PerformLayout();
            this.groupBoxFushoCount.ResumeLayout(false);
            this.groupBoxFushoCount.PerformLayout();
            this.groupBoxNewCont.ResumeLayout(false);
            this.groupBoxNewCont.PerformLayout();
            this.groupBoxAnmaCount.ResumeLayout(false);
            this.groupBoxAnmaCount.PerformLayout();
            this.groupBoxAnma.ResumeLayout(false);
            this.groupBoxAnma.PerformLayout();
            this.groupBoxShokaiResult.ResumeLayout(false);
            this.groupBoxShokaiResult.PerformLayout();
            this.groupBoxDouisyo.ResumeLayout(false);
            this.groupBoxDouisyo.PerformLayout();
            this.groupBoxSejutsuHoukokusyo.ResumeLayout(false);
            this.groupBoxSejutsuHoukokusyo.PerformLayout();
            this.groupBoxJoutaikinyusyo.ResumeLayout(false);
            this.groupBoxJoutaikinyusyo.PerformLayout();
            this.groupBoxDual.ResumeLayout(false);
            this.groupBoxDual.PerformLayout();
            this.groupBoxStartDate.ResumeLayout(false);
            this.groupBoxStartDate.PerformLayout();
            this.groupBoxFinishDate.ResumeLayout(false);
            this.groupBoxFinishDate.PerformLayout();
            this.groupBoxDouiDate.ResumeLayout(false);
            this.groupBoxDouiDate.PerformLayout();
            this.panelSearch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxYM;
        private MonthBox monthBoxS1;
        private MonthBox monthBoxE1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxCYM;
        private System.Windows.Forms.Label label6;
        private MonthBox monthBoxS2;
        private MonthBox monthBoxE2;
        private System.Windows.Forms.GroupBox groupBoxTotal;
        private System.Windows.Forms.Label label7;
        private NumBox numBoxTotalMax;
        private NumBox numBoxTotalMin;
        private System.Windows.Forms.GroupBox groupBoxClinicNum;
        private System.Windows.Forms.TextBox textBoxClinicNum;
        private System.Windows.Forms.GroupBox groupBoxOryo;
        private System.Windows.Forms.CheckBox checkBoxOshinNashi;
        private System.Windows.Forms.CheckBox checkBoxOshinAri;
        private System.Windows.Forms.GroupBox groupBoxDrName;
        private System.Windows.Forms.TextBox textBoxDrName;
        private System.Windows.Forms.GroupBox groupBoxDrCode;
        private NumBox numBoxDr;
        private System.Windows.Forms.GroupBox groupBoxType;
        private System.Windows.Forms.CheckBox checkBoxMs;
        private System.Windows.Forms.CheckBox checkBoxJyu;
        private System.Windows.Forms.CheckBox checkBoxSin;
        private System.Windows.Forms.GroupBox groupBoxShokai;
        private System.Windows.Forms.CheckBox checkBoxShokaiNashi;
        private System.Windows.Forms.CheckBox checkBoxShokaiTaisho;
        private System.Windows.Forms.GroupBox groupBoxNum;
        private NumBox numBoxNum;
        private System.Windows.Forms.GroupBox groupBoxHenrei;
        private System.Windows.Forms.CheckBox checkBoxHenNashi;
        private System.Windows.Forms.CheckBox checkBoxHenAri;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.GroupBox groupBoxGroupNum;
        private NumBox numBoxGroup;
        private System.Windows.Forms.GroupBox groupBoxKana;
        private System.Windows.Forms.GroupBox groupBoxDual;
        private System.Windows.Forms.CheckBox checkBoxDual;
        private System.Windows.Forms.GroupBox groupBoxDays;
        private System.Windows.Forms.Label label1;
        private NumBox numBoxDaysMax;
        private NumBox numBoxDaysMin;
        private System.Windows.Forms.TextBox textBoxKana;
        private System.Windows.Forms.GroupBox groupBoxFusho;
        private System.Windows.Forms.TextBox textBoxFusho;
        private System.Windows.Forms.GroupBox groupBoxCharge;
        private System.Windows.Forms.Label label3;
        private NumBox numBoxChargeMax;
        private NumBox numBoxChargeMin;
        private System.Windows.Forms.GroupBox groupBoxFushoCount;
        private System.Windows.Forms.Label label4;
        private NumBox numBoxFushoMax;
        private NumBox numBoxFushoMin;
        private System.Windows.Forms.GroupBox groupBoxAnmaCount;
        private System.Windows.Forms.Label label5;
        private NumBox numBoxAnmaMax;
        private NumBox numBoxAnmaMin;
        private System.Windows.Forms.GroupBox groupBoxAnma;
        private System.Windows.Forms.CheckBox checkBoxLeftLower;
        private System.Windows.Forms.CheckBox checkBoxRightLower;
        private System.Windows.Forms.CheckBox checkBoxRightUpper;
        private System.Windows.Forms.CheckBox checkBoxLeftUpper;
        private System.Windows.Forms.CheckBox checkBoxBody;
        private System.Windows.Forms.Panel panelSeparator;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBoxClinicName;
        private System.Windows.Forms.TextBox textBoxClinicName;
        private System.Windows.Forms.GroupBox groupBoxInsNum;
        private NumBox numBoxInsNum;
        private System.Windows.Forms.GroupBox groupBoxName;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.GroupBox groupBoxShokaiResult;
        private System.Windows.Forms.CheckBox checkBoxShokaiSouiNashi;
        private System.Windows.Forms.CheckBox checkBoxShokaiSouiAri;
        private System.Windows.Forms.GroupBox groupBoxDouiDate;
        private System.Windows.Forms.Label label8;
        private DateBox dateBoxDouiTo;
        private DateBox dateBoxDouiFrom;
        private System.Windows.Forms.GroupBox groupBoxStartDate;
        private DateBox dateBoxStartTo;
        private DateBox dateBoxStartFrom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBoxFinishDate;
        private DateBox dateBoxFinishTo;
        private DateBox dateBoxFinishFrom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBoxNewCont;
        private System.Windows.Forms.CheckBox checkBoxNewContNew;
        private System.Windows.Forms.CheckBox checkBoxNewContCont;
        private System.Windows.Forms.GroupBox groupBoxDouisyo;
        private System.Windows.Forms.CheckBox checkBoxDouisyoNashi;
        private System.Windows.Forms.CheckBox checkBoxDouisyoAri;
        private System.Windows.Forms.GroupBox groupBoxSejutsuHoukokusyo;
        private System.Windows.Forms.CheckBox checkBoxSejutsuHoukokusyoNashi;
        private System.Windows.Forms.CheckBox checkBoxSejutsuHoukokusyoAri;
        private System.Windows.Forms.GroupBox groupBoxJoutaikinyusyo;
        private System.Windows.Forms.CheckBox checkBoxJoutaikinyusyoNashi;
        private System.Windows.Forms.CheckBox checkBoxJoutaikinyusyoAri;
    }
}
