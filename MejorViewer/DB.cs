﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using Dapper;
using System.Data.SQLite;
using System.Reflection;

namespace MejorViewer
{
    class DB
    {
        public static DB Main { get; private set; }
        static DB()
        {
            var fn =System.Windows.Forms.Application.StartupPath;
            if (fn.Length > 1 && fn[0] == '\\' && fn[1]=='\\') fn = "\\\\" + fn;
            fn += "\\jyuviewer.db3";
            Main = new DB(fn);
        }

        string conStr = string.Empty;

        public DB(string fileName)
        {
            var sb = new SQLiteConnectionStringBuilder();
            sb.DataSource = fileName;

            conStr = sb.ToString();
        }

        private SQLiteConnection getOpenConn()
        {
            SQLiteConnection con = null;

            try
            {
                con = new SQLiteConnection(conStr);
                con.Open();
            }
            catch
            {
                try
                {
                    if (con != null) con.Dispose();
                    con = null;
                    con = new SQLiteConnection(conStr);
                    con.Open();
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteAndExit(ex);
                    throw ex;
                }
            }
            return con;
        }

        public class Transaction : IDisposable
        {
            bool disposed = false;
            public SQLiteTransaction tran { get; private set; }
            public SQLiteConnection conn { get; private set; }

            public Transaction(DB db)
            {
                conn = db.getOpenConn();
                tran = conn.BeginTransaction();
            }

            ~Transaction()
            {
                Dispose();
            }

            public void Dispose()
            {
                if (!disposed)
                {
                    conn.Dispose();
                    tran.Dispose();
                }
                conn = null;
                disposed = true;
            }

            public void Commit()
            {
                tran.Commit();
            }

            public void Rollback()
            {
                tran.Rollback();
            }
        }

        public Transaction CreateTran()
        {
            var tran = new Transaction(this);
            return tran;
        }

        public HashSet<string> GetColumnNames(string tableName)
        {
            var hs = new HashSet<string>();
            try
            {
                var sql = $"SELECT * FROM {tableName} LIMIT 1;";
                using (var con = getOpenConn())
                using (var cmd = new SQLiteCommand(sql, con))
                using (var dr = cmd.ExecuteReader())
                {
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        hs.Add(dr.GetName(i));
                    }
                    return hs;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }

        public IEnumerable<T> Query<T>(string sql, object ps)
        {
            try
            {
                using (var con = getOpenConn())
                {
                    var res = con.Query<T>(sql, ps);
                    return res;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }
        
        public IEnumerable<T> QueryAsync<T>(string sql, object ps)
        {
            try
            {
                var wf = new WaitFormSimple();
                bool showWairForm = false;

                using (var t = new System.Timers.Timer(500))
                using (var con = getOpenConn())
                {
                    wf.Canceling += ((fs, fe) => con.Cancel());

                    try
                    {
                        t.Start();
                        t.Elapsed += ((s, e) =>
                        {
                            t.Stop();
                            Task.Factory.StartNew(() =>
                            {
                                showWairForm = true;
                                wf.ShowDialog();
                            });
                        });

                        var res = con.QueryAsync<T>(sql, ps);
                        t.Stop();
                        return res.Result;
                    }
                    finally
                    {
                        t.Dispose();
                        if (showWairForm) wf.InvokeDispose();
                    }
                }
            }
            catch (Exception ex)
            {
                var ie = ex.InnerException;
                if (ie != null && ie is SQLiteException)
                {
                    var se = (SQLiteException)ie;
                    if (se.ResultCode == SQLiteErrorCode.Interrupt)
                    {
                        System.Windows.Forms.MessageBox.Show("DBアクセスを中止しました");
                        return null;
                    }
                }
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }

        public bool Excute(string sql, object obj)
        {
            try
            {
                using (var con = getOpenConn())
                {
                    con.Execute(sql, obj);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        public bool Excute(string sql, object obj, Transaction tran)
        {
            try
            {
                tran.conn.Execute(sql, obj, tran.tran);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// データベースのカラムに関連した属性を管理します
        /// </summary>
        public class DbAttribute
        {
            /// <summary>
            /// データベース上のテーブルでキーとなるプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class PrimaryKey : System.Attribute { }

            /// <summary>
            /// データベース上のテーブルでserialが設定されているプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class Serial : System.Attribute { }

            /// <summary>
            /// データベース上のテーブルでUPDATE時に更新しないプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class UpdateIgnore : System.Attribute { }

            /// <summary>
            /// データベース上のテーブルにないプロパティに指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
            public class Ignore : System.Attribute { }

            /// <summary>
            /// クラス名以外のテーブルに関連付けする場合に指定します
            /// </summary>
            [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
            public class DifferentTableName : System.Attribute
            {
                public readonly string TableName;
                public DifferentTableName(string tableName) { TableName = tableName; }
            }
        }

        /// <summary>
        /// SQL文をクラスから自動生成します
        /// </summary>
        class SqlBuilder
        {
            /// <summary>
            /// クラス情報をリフレクションにより取得し、キャッシュします
            /// </summary>
            class ClassCache
            {
                static Dictionary<Type, ClassCache> classes = new Dictionary<Type, ClassCache>();

                public string TableName { get; private set; }
                public List<string> SelectColumns { get; private set; } = new List<string>();
                public List<string> InsertColumns { get; private set; } = new List<string>();
                public List<string> UpdateColumns { get; private set; } = new List<string>();
                public List<string> Keys { get; private set; } = new List<string>();
                public List<string> SerialColumns { get; private set; } = new List<string>();
                PropertyInfo[] properties;

                ClassCache(Type t)
                {
                    var ta = Array.Find(System.Attribute.GetCustomAttributes(t), x => x is DbAttribute.DifferentTableName);
                    TableName = ((DbAttribute.DifferentTableName)ta)?.TableName ?? t.Name;
                    properties = t.GetProperties();

                    foreach (var item in properties)
                    {
                        //無視をすべてのコマンドから対象外
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Ignore)) continue;

                        //SELECT対象 すべて
                        SelectColumns.Add(item.Name);

                        //INSERT対象 serial/Readonly以外
                        if (!Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Serial))
                            InsertColumns.Add(item.Name);

                        //UPDATE対象 Key/Readonly以外
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.PrimaryKey))
                            Keys.Add(item.Name);
                        else if (!(Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.UpdateIgnore)))
                            UpdateColumns.Add(item.Name);

                        //RETURNING対象 serial属性が指定されたもの
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Serial))
                            SerialColumns.Add(item.Name);
                    }
                    classes.Add(t, this);
                }

                public static ClassCache GetCashe(Type t)
                {
                    return classes.ContainsKey(t) ?
                        classes[t] : new ClassCache(t);
                }

                public string CreateTableSql<T>()
                {
                    var cols = new List<string>();
                    var keys = new List<string>();
                    foreach (var item in properties)
                    {
                        //無視
                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.Ignore)) continue;

                        //各カラム
                        string colType;
                        if (item.PropertyType == typeof(int)) colType = "INTEGER NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(long)) colType = "BIGINT NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(string)) colType = "TEXT NOT NULL DEFAULT ''";
                        else if (item.PropertyType == typeof(DateTime)) colType = "DATETIME NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(float)) colType = "REAL NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(double)) colType = "REAL NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(decimal)) colType = "NUMERIC NOT NULL DEFAULT 0";
                        else if (item.PropertyType == typeof(Boolean)) colType = "BOOLEAN NOT NULL DEFAULT FALSE";
                        else if (item.PropertyType.IsEnum) colType = "INTEGER NOT NULL DEFAULT 0";
                        else throw new Exception("自動テーブル作成に対応していないデータ型です");
                        cols.Add(item.Name + " " + colType);

                        if (Array.Exists(Attribute.GetCustomAttributes(item), x => x is DbAttribute.PrimaryKey))
                            keys.Add(item.Name);

                    }

                    if (keys.Count == 0)
                    {
                        throw new Exception("PrimaryKey属性が指定されていません");
                    }

                    return $"CREATE TABLE "+ TableName + "(\r\n" + string.Join(",\r\n", cols) +
                        $" ,\r\nCONSTRAINT {TableName}_pkey PRIMARY KEY (" + string.Join(", ", keys) + "));";
                }

            }

            public static string CreateSelectSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                return $"SELECT {string.Join(",", c.SelectColumns)} FROM {c.TableName};";
            }

            public static string CreateSelectSql<T>(string where)
            {
                var c = ClassCache.GetCashe(typeof(T));
                return $"SELECT {string.Join(",", c.SelectColumns)} FROM {c.TableName} WHERE {where};";
            }

            public static string CreateSelectSql<T>(object obj)
            {
                var c = ClassCache.GetCashe(typeof(T));

                var t = obj.GetType();
                if (!t.IsClass) throw new Exception("指定されたオブジェクトはクラスではありません");
                var ps = t.GetProperties();

                return $"SELECT {string.Join(",", c.SelectColumns)} FROM {c.TableName} " +
                    $"WHERE {string.Join(" AND ", ps.Select(p => $"{p.Name}=@{p.Name}"))};";
            }

            public static string CreateInsertSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));

                return $"INSERT INTO {c.TableName}({string.Join(",", c.InsertColumns)})" +
                    $"VALUES(@{string.Join(",@", c.InsertColumns)})";
            }

            public static string CreateUpdateSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                if (c.Keys.Count == 0) throw new Exception("キー属性が指定されていません");

                return $"UPDATE {c.TableName} SET {string.Join(",", c.UpdateColumns.Select(p => $"{p}=@{p}"))} " +
                    $"WHERE {string.Join(" AND ", c.Keys.Select(p => $"{p}=@{p}"))};";
            }

            public static string CreateTableSql<T>()
            {
                var c = ClassCache.GetCashe(typeof(T));
                if (c.Keys.Count == 0) throw new Exception("キー属性が指定されていません");

                return c.CreateTableSql<T>();
            }

        }

        /// <summary>
        /// WHERE句を直接指定して、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where">WHERE句 "WHERE"は不要</param>
        /// <returns></returns>
        public IEnumerable<T> Select<T>(string where)
        {
            string sql = SqlBuilder.CreateSelectSql<T>(where);
            using (var con = getOpenConn())
            {
                return con.Query<T>(sql);
            }
        }

        /// <summary>
        /// 匿名クラスを利用し、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="whereClass">条件指定クラス　exsample: new { id = id, name = "test" }</param>
        /// <returns></returns>
        public IEnumerable<T> Select<T>(object whereClass)
        {
            string sql = SqlBuilder.CreateSelectSql<T>(whereClass);
            using (var con = getOpenConn())
            {
                return con.Query<T>(sql, whereClass);
            }
        }

        /// <summary>
        /// 匿名クラスを利用し、データをSELECTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="whereClass">条件指定クラス　exsample: new { id = id, name = "test" }</param>
        /// <returns></returns>
        public IEnumerable<T> SelectAll<T>()
        {
            string sql = SqlBuilder.CreateSelectSql<T>();
            using (var con = getOpenConn())
            {
                return con.Query<T>(sql);
            }
        }

        /// <summary>
        /// データをInsertします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Insert<T>(T obj, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateInsertSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, obj);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// データ群をINSERTします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objs"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Inserts<T>(IEnumerable<T> objs, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateInsertSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, objs);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// Key属性があらかじめ指定されたプロパティを条件にデータをUPDATEします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Update<T>(T obj, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateUpdateSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, obj);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        /// <summary>
        /// Key属性があらかじめ指定されたプロパティを条件にデータ群をUPDATEします
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Updates<T>(IEnumerable<T> objs, Transaction tran = null)
        {
            var sql = SqlBuilder.CreateUpdateSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql, objs);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        public bool CreateTable<T>(Transaction tran)
        {
            var sql = SqlBuilder.CreateTableSql<T>();
            var con = tran == null ? getOpenConn() : tran.conn;

            try
            {
                con.Execute(sql);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                if (tran == null) con.Dispose();
            }
        }

        public bool ExistsTable(string tableName)
        {
            var sql = $"SELECT name FROM sqlite_master " +
                $"WHERE type = 'table' AND name = '{tableName}' " +
                $"LIMIT 1;";

            var con = getOpenConn();
            try
            {
                var l = con.Query<string>(sql);
                return l.Count() != 0;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                con.Dispose();
            }
        }

        public bool ExistsColumn(string tableName, string columnName)
        {
            var sql = $"PRAGMA TABLE_INFO('{tableName}');";
            var con = getOpenConn();
            try
            {
                var ls = con.Query(sql);
                var c = ls.Select(r => r.name).FirstOrDefault(s => s == columnName);
                return c != null;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                con.Dispose();
            }
        }
    }
}
