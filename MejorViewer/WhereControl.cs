﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MejorViewer
{
    /// <summary>
    /// 検索条件の画面
    /// </summary>
    [DefaultEvent("SearchClick")]
    public partial class WhereControl : UserControl
    {
        public event EventHandler SearchClick;

        public WhereControl()
        {
            InitializeComponent();
            SizeChanged += WhereControl_SizeChanged;

            WhereControlSetting.Load(flowLayoutPanel1);
            WhereControlSetting.SetMainMode(flowLayoutPanel1);
        }

        public void SetCYM(int yyyymm)
        {
            var y = yyyymm / 100;
            var m = yyyymm % 100;
            monthBoxS2.SetDate(new DateTime(y, m, 1));
        }

        public void Adjust()
        {
            if (checkBox1.Checked) WhereControlSetting.SetSubMode(flowLayoutPanel1);
            else WhereControlSetting.SetMainMode(flowLayoutPanel1);
            controlLocationAdjust();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (SearchClick != null) SearchClick.Invoke(sender, e);
        }

        private void WhereControl_SizeChanged(object sender, EventArgs e)
        {
            controlLocationAdjust();
        }

        private void controlLocationAdjust()
        {
            int index = flowLayoutPanel1.Controls.GetChildIndex(panelSearch);
            Control c = panelSearch;

            for (int i = index - 1; i > 0; i--)
            {
                if (!flowLayoutPanel1.Controls[i].Visible) continue;
                c = flowLayoutPanel1.Controls[i];
                break;
            }

            var w = Width - c.Location.X - c.Width - flowLayoutPanel1.Margin.Horizontal * 2;
            if (w > 124) panelSearch.Size = new Size(w, 42);
            else panelSearch.Size = new Size(Width - Margin.Horizontal - 2, 27);

            panelSeparator.Width = flowLayoutPanel1.Width - flowLayoutPanel1.Margin.Horizontal;

            for (int i = flowLayoutPanel1.Controls.Count - 1; i >= 0; i--)
            {
                c = flowLayoutPanel1.Controls[i];
                if (c.Visible) break;
            }
            this.Height = c.Location.Y + c.Height + 2;
        }

        /// <summary>
        /// UIの状態からDBの検索条件を生成する
        /// </summary>
        /// <returns>where句</returns>
        public string CreateWhere()
        {
            long temp;
            string[] temps;
            var vals = new List<string>();

            var ws = new List<string>();
            string monthFunc(DateTime dt)
            {
                if (dt.IsNullDate()) return string.Empty;
                return dt.ToString("yyyyMM");
            }

            string dateFunc(DateTime dt)
            {
                if (dt.IsNullDate()) return string.Empty;
                return dt.ToString("yyyyMMdd");
            }

            //処理年月
            if (groupBoxCYM.Visible)
            {
                var syoriSYM = monthFunc(monthBoxS2.GetDate());
                var syoriEYM = monthFunc(monthBoxE2.GetDate());
                if (syoriSYM != "" && syoriEYM != "") ws.Add($"cym BETWEEN {syoriSYM} AND {syoriEYM}");
                else if (syoriSYM != "") ws.Add("cym >= " + syoriSYM);
                else if (syoriEYM != "") ws.Add("cym <= " + syoriEYM);
            }

            //診療年月
            if (groupBoxYM.Visible)
            {
                var sinryoSYM = monthFunc(monthBoxS1.GetDate());
                var sinryoEYM = monthFunc(monthBoxE1.GetDate());
                if (sinryoSYM != "" && sinryoEYM != "") ws.Add("ym BETWEEN " + sinryoSYM + " AND " + sinryoEYM);
                else if (sinryoSYM != "") ws.Add("ym >= " + sinryoSYM);
                else if (sinryoEYM != "") ws.Add("ym <= " + sinryoEYM);
            }

            //保険者番号
            if (groupBoxInsNum.Visible)
            {
                temps = numBoxInsNum.Text.Split(' ');
                vals.Clear();
                foreach (var item in temps)
                {
                    if (!long.TryParse(item, out temp)) continue;
                    vals.Add($"'{item.Trim()}'");
                }
                if (vals.Count > 0) ws.Add("InsNum IN ( " + string.Join(", ", vals) + " )");
            }
            
            //被保番
            if (groupBoxNum.Visible)
            {
                temps = numBoxNum.Text.Split(' ');
                vals.Clear();
                foreach (var item in temps)
                {
                    if (!long.TryParse(item, out temp)) continue;
                    //vals.Add($"'{item.Trim()}'"); //20221205 ito 兵庫広域・下位からの桁指定での検索
                    vals.Add($"{item.Trim()}");
                }
                //if (vals.Count > 0) ws.Add("num IN ( " + string.Join(", ", vals) + " )"); //20221205 ito 兵庫広域・下位からの桁指定での検索
                if (vals.Count > 0) ws.Add("num LIKE '%" + string.Join("' OR num LIKE '%", vals) + "'");
            }

            //氏名
            if (groupBoxName.Visible)
            {
                string name = textBoxName.Text.Trim();
                if (name.Length > 0) ws.Add($"name LIKE '%{esc(name)}%'");
            }

            //カナ
            if (groupBoxKana.Visible)
            {
                string kana = textBoxKana.Text.Trim();
                if (kana.Length > 0) ws.Add($"kana LIKE '%{esc(kana)}%'");
            }

            //合計
            if (groupBoxTotal.Visible)
            {
                int min = numBoxTotalMin.GetIntValue();
                int max = numBoxTotalMax.GetIntValue();
                if (min != 0 && max != 0) ws.Add($"total BETWEEN {min} AND {max}");
                else if (min != 0) ws.Add($"total >= {min}");
                else if (max != 0) ws.Add($"total <= {max}");
            }

            //請求
            if (groupBoxCharge.Visible)
            {
                int cmin = numBoxChargeMin.GetIntValue();
                int cmax = numBoxChargeMax.GetIntValue();
                if (cmin != 0 && cmax != 0) ws.Add($"Charge BETWEEN {cmin} AND {cmax}");
                else if (cmin != 0) ws.Add($"Charge >= {cmin}");
                else if (cmax != 0) ws.Add($"Charge <= {cmax}");
            }

            //負傷名
            if (groupBoxFusho.Visible)
            {
                temps = textBoxFusho.Text.Trim().Replace('　', ' ').Split(' ');
                foreach (var item in temps)
                {
                    if (item.Length < 2) continue;
                    var s = $"'%{esc(item.Trim())}%'";
                    ws.Add($"( Fusho1 LIKE {s} OR Fusho2 LIKE {s} OR Fusho3 LIKE {s} OR Fusho4 LIKE {s} OR Fusho5 LIKE {s} )");
                }
            }

            //負傷/病名数
            if (groupBoxFushoCount.Visible)
            {
                int fmin, fmax;
                int.TryParse(numBoxFushoMin.Text, out fmin);
                int.TryParse(numBoxFushoMax.Text, out fmax);
                if (fmin != 0 && fmax != 0) ws.Add($"FushoCount BETWEEN {fmin} AND {fmax}");
                else if (fmin != 0) ws.Add($"FushoCount >= {fmin}");
                else if (fmax != 0) ws.Add($"FushoCount <= {fmax}");
            }

            //種別
            if (groupBoxType.Visible)
            {
                vals.Clear();
                if (checkBoxJyu.Checked) vals.Add(((int)APP_TYPE.柔整).ToString());
                if (checkBoxSin.Checked) vals.Add(((int)APP_TYPE.鍼灸).ToString());
                if (checkBoxMs.Checked) vals.Add(((int)APP_TYPE.あんま).ToString());
                if (vals.Count == 0) ws.Add("AppType IS NULL");
                else if (vals.Count != 3) ws.Add("AppType IN ( " + string.Join(", ", vals) + " )");
            }

            //往診料
            if (groupBoxOryo.Visible)
            {
                if (!checkBoxOshinAri.Checked && !checkBoxOshinNashi.Checked) ws.Add("VisitFee IS NULL");
                else if (checkBoxOshinAri.Checked && !checkBoxOshinNashi.Checked) ws.Add("Visitfee");
                else if (!checkBoxOshinAri.Checked && checkBoxOshinNashi.Checked) ws.Add("NOT VisitFee");
            }

            //施術所コード
            if (groupBoxClinicNum.Visible)
            {
                temps = textBoxClinicNum.Text.Replace('　', ' ').Split(' ');
                vals.Clear();
                foreach (var item in temps)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    vals.Add($"'{item.Trim()}'");
                }
                if (vals.Count > 0) ws.Add("ClinicNum IN(" + string.Join(", ", vals) + " )");
            }

            //施術所名
            if (groupBoxClinicName.Visible)
            {
                temps = textBoxClinicName.Text.Replace('　', ' ').Split(' ');
                vals.Clear();
                foreach (var item in temps)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    vals.Add("ClinicName LIKE '%" + esc(item) + "%'");
                }
                if (vals.Count > 0) vals.ForEach(v => ws.Add(v));
            }

            //施術師コード
            if (groupBoxDrCode.Visible)
            {
                temps = numBoxDr.Text.Split(' ');
                vals.Clear();
                foreach (var item in temps)
                {
                    if (!long.TryParse(item.Trim(), out temp)) continue;
                    vals.Add($"'{item.Trim()}'");
                }
                if (vals.Count > 0) ws.Add("DrNum IN ( " + string.Join(", ", vals) + " )");
            }

            //施術師名
            if (groupBoxDrName.Visible)
            {
                temps = textBoxDrName.Text.Replace('　', ' ').Split(' ');
                vals.Clear();
                foreach (var item in temps)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    vals.Add("DrName LIKE '%" + esc(item) + "%'");
                }
                if (vals.Count > 0) vals.ForEach(v => ws.Add(v));
            }

            //グループコード
            if (groupBoxGroupNum.Visible)
            {
                temps = numBoxGroup.Text.Split(' ');
                vals.Clear();
                foreach (var item in temps)
                {
                    if (!long.TryParse(item.Trim(), out temp)) continue;
                    vals.Add("'" + item + "'");
                }
                if (vals.Count > 0) ws.Add("GroupNum IN ( " + string.Join(", ", vals) + " )");
            }

            //新規継続
            if (groupBoxNewCont.Visible)
            {
                if (!checkBoxNewContNew.Checked && !checkBoxNewContCont.Checked) ws.Add($"NewCont=0");
                else if (checkBoxNewContNew.Checked && !checkBoxNewContCont.Checked) ws.Add($"NewCont={(int)NewCont.新規}");
                else if (!checkBoxNewContNew.Checked && checkBoxNewContCont.Checked) ws.Add($"NewCont={(int)NewCont.継続}");
            }

            //あんま数
            if (groupBoxAnmaCount.Visible)
            {
                int amin, amax;
                int.TryParse(numBoxAnmaMin.Text, out amin);
                int.TryParse(numBoxAnmaMax.Text, out amax);
                if (amin != 0 && amax != 0) ws.Add($"AnmaCount BETWEEN {amin} AND {amax}");
                else if (amin != 0) ws.Add($"AnmaCount >= {amin}");
                else if (amax != 0) ws.Add($"AnmaCount <= {amax}");
            }

            //あんま局所
            if (groupBoxAnma.Visible)
            {
                var anmaAct = new Action<CheckBox, string>((cb, s) =>
              {
                  if (cb.CheckState == CheckState.Checked) ws.Add(s);
                  else if (cb.CheckState == CheckState.Unchecked) ws.Add("NOT " + s);
              });
                anmaAct(checkBoxBody, "AnmaBody");
                anmaAct(checkBoxRightUpper, "AnmaRightUpper");
                anmaAct(checkBoxLeftUpper, "AnmaLeftUpper");
                anmaAct(checkBoxRightLower, "AnmaRightLower");
                anmaAct(checkBoxLeftLower, "AnmaLeftLower");
            }

            // 照会対象
            if (groupBoxShokai.Visible)
            {
                vals.Clear();
                if (checkBoxShokaiNashi.Checked && !checkBoxShokaiTaisho.Checked) ws.Add("ShokaiID = ''");
                if (!checkBoxShokaiNashi.Checked && checkBoxShokaiTaisho.Checked) ws.Add("ShokaiID <> ''");
            }

            // 照会結果
            if (groupBoxShokaiResult.Visible)
            {
                vals.Clear();
                if (checkBoxShokaiSouiAri.Checked) vals.Add(((int)ShokaiResult.相違あり).ToString());
                if (checkBoxShokaiSouiNashi.Checked) vals.Add(((int)ShokaiResult.相違なし).ToString());
                if (vals.Count != 2) ws.Add("ShokaiResult IN(" + string.Join(", ", vals) + ")");
            }

            // 同意書あり
            if (groupBoxDouisyo.Visible)
            {
                if (checkBoxDouisyoAri.Checked != checkBoxDouisyoNashi.Checked)
                {
                    if (checkBoxDouisyoAri.Checked)
                    {
                        ws.Add("HasDouisyo = 1");
                    }
                    else if (checkBoxDouisyoNashi.Checked)
                    {
                        ws.Add("HasDouisyo = 0");
                    }
                }
            }
            // 施術報告書あり
            if (groupBoxSejutsuHoukokusyo.Visible)
            {
                if (checkBoxSejutsuHoukokusyoAri.Checked != checkBoxSejutsuHoukokusyoNashi.Checked)
                {
                    if (checkBoxSejutsuHoukokusyoAri.Checked)
                    {
                        ws.Add("HasSejutsuHoukokusyo = 1");
                    }
                    else if (checkBoxSejutsuHoukokusyoNashi.Checked)
                    {
                        ws.Add("HasSejutsuHoukokusyo = 0");
                    }
                }
            }
            // 状態記入書あり
            if (groupBoxJoutaikinyusyo.Visible)
            {
                if (checkBoxJoutaikinyusyoAri.Checked != checkBoxJoutaikinyusyoNashi.Checked)
                {
                    if (checkBoxJoutaikinyusyoAri.Checked)
                    {
                        ws.Add("HasJoutaikinyusyo = 1");
                    }
                    else if (checkBoxJoutaikinyusyoNashi.Checked)
                    {
                        ws.Add("HasJoutaikinyusyo = 0");
                    }
                }
            }

            //返戻
            if (groupBoxHenrei.Visible)
            {
                if (!checkBoxHenAri.Checked && !checkBoxHenNashi.Checked) ws.Add("HenreiFlag IS NULL");
                else if (checkBoxHenAri.Checked && !checkBoxHenNashi.Checked) ws.Add("HenreiFlag");
                else if (!checkBoxHenAri.Checked && checkBoxHenNashi.Checked) ws.Add("NOT HenreiFlag");
            }

            //実日数
            if (groupBoxDays.Visible)
            {
                int daysMin = numBoxDaysMin.GetIntValue();
                int daysMax = numBoxDaysMax.GetIntValue();
                if (daysMin != 0 && daysMax != 0) ws.Add($"Days BETWEEN {daysMin} AND {daysMax}");
                else if (daysMin != 0) ws.Add($"Days>={daysMin}");
                else if (daysMax != 0) ws.Add($"Days<={daysMax}");
            }

            //開始年月日
            if (groupBoxStartDate.Visible)
            {
                var start = dateFunc(dateBoxStartFrom.GetDate());
                var finish = dateFunc(dateBoxStartTo.GetDate());
                if (start != "" && finish != "") ws.Add($"startdate BETWEEN {start} AND {finish}");
                else if (start != "") ws.Add("startdate >= " + start);
                else if (finish != "") ws.Add("startdate <= " + finish);
            }

            //終了年月日
            if (groupBoxFinishDate.Visible)
            {
                var start = dateFunc(dateBoxFinishFrom.GetDate());
                var finish = dateFunc(dateBoxFinishTo.GetDate());
                if (start != "" && finish != "") ws.Add($"finishdate BETWEEN {start} AND {finish}");
                else if (start != "") ws.Add("finishdate >= " + start);
                else if (finish != "") ws.Add("finishdate <= " + finish);
            }

            //同意年月日
            if (groupBoxDouiDate.Visible)
            {
                var start = dateFunc(dateBoxDouiFrom.GetDate());
                var finish = dateFunc(dateBoxDouiTo.GetDate());
                if (start != "" && finish != "") ws.Add($"douidate BETWEEN {start} AND {finish}");
                else if (start != "") ws.Add("douidate >= " + start);
                else if (finish != "") ws.Add("douidate <= " + finish);
            }
            
            if (ws.Count == 0) return string.Empty;

            //重複チェック時
            if (groupBoxDual.Visible && checkBoxDual.Checked)
            {
                var sws = new List<string>();
                ws.ForEach(w =>
                {
                    if (w[0] == '(' && w.Contains(" Fusho1 LIKE "))
                        sws.Add(w.Replace(" Fusho", " r2.Fusho"));
                    else sws.Add("r2." + w);
                });

                var sql = string.Join(" AND ", ws) +
                    " AND EXISTS (SELECT c FROM (" +
                    " SELECT COUNT(t) AS c FROM (" +
                    " SELECT r2.apptype as t" +
                    " FROM rece AS r2" +
                    " WHERE " + string.Join(" AND ", sws) +
                    " AND r.num=r2.num " +
                    " GROUP BY t)) " +
                    " WHERE c>1)";
                return sql;
            }
            else
            {
                return string.Join(" AND ", ws) + " ";
            }
        }

        private string esc(string str)
        {
            return str.Replace("'", "''");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked) WhereControlSetting.SetSubMode(flowLayoutPanel1);
            else WhereControlSetting.SetMainMode(flowLayoutPanel1);
            controlLocationAdjust();
        }

        private void WhereControl_Load(object sender, EventArgs e)
        {

        }
    }
}
