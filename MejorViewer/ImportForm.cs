﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MejorViewer
{
    public partial class ImportForm : Form
    {
        public ImportForm()
        {
            InitializeComponent();
            getImport();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void getImport()
        {
            var l = Import.GetAllImport();
            ctGrid1.DataSource = l;
            ctGrid1.Columns[nameof(Import.ImportID)].HeaderText = "ID";
            ctGrid1.Columns[nameof(Import.ImportID)].Width = 80;
            ctGrid1.Columns[nameof(Import.ImportDate)].HeaderText = "取込日";
            ctGrid1.Columns[nameof(Import.ImportDate)].DefaultCellStyle.Format = "0000/00/00";
            ctGrid1.Columns[nameof(Import.ImportCount)].HeaderText = "件数";
            ctGrid1.Columns[nameof(Import.ImportCount)].Width = 80;
            ctGrid1.Columns[nameof(Import.ImportCount)].DefaultCellStyle.Format = "#,0";
            ctGrid1.Columns[nameof(Import.ImportCount)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            ctGrid1.Columns[nameof(Import.CYM)].HeaderText = "処理年月";
            ctGrid1.Columns[nameof(Import.CYM)].Width = 80;
            ctGrid1.Columns[nameof(Import.CYM)].DefaultCellStyle.Format = "00/00";
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            using (var f = new OpenFileDialog())
            {
                f.Filter = "Infoファイル(Info.txt)|Info.txt";
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                if (Import.StartImport(f.FileName))
                {
                    MessageBox.Show("インポートが終了しました");
                }
                else
                {
                    MessageBox.Show("インポートに失敗しました");
                }
                
                this.Invoke(new Action(() => getImport()));
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (ctGrid1.CurrentCell == null || ctGrid1.CurrentCell.RowIndex < 0)
                このインポートデータを削除ToolStripMenuItem.Enabled = false;
            else
                このインポートデータを削除ToolStripMenuItem.Enabled = true;
        }

        private void このインポートデータを削除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ctGrid1.CurrentCell == null || ctGrid1.CurrentCell.RowIndex < 0) return;
            int ri = ctGrid1.CurrentCell.RowIndex;
            int iid = (int)ctGrid1["importid", ri].Value;

            if (!Common.OKCancelMsg("対象のインポートデータを削除しますがよろしいですか？")) return;

            if(!Common.OKCancelExMsg("対象のインポートデータを削除します。元には戻せませんが本当によろしいですか？")) return;

            if (Rece2.DeleteImport(iid))
                MessageBox.Show("削除しました");
            else
                MessageBox.Show("削除に失敗しました");

            getImport();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            string fileName;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "CSVファイル(*.csv)|*.csv|すべてのファイル(*.*)|*.*";
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                fileName = f.FileName;
            }


            if (ViewerUpdateData.Update(fileName))
            {
                MessageBox.Show("アップデートが終了しました");
            }
            else
            {
                MessageBox.Show("アップデートに失敗しました");
            }

            this.Invoke(new Action(() => getImport()));
        }
    }
}
