﻿using System;
using System.Linq;

namespace MejorViewer
{
    /// <summary>
    /// 起動時にDBのマイグレーションを行うクラス
    /// </summary>
    class DbUpdater
    {
        public static bool CheckAndUpdateSchema()
        {
            bool backuped = false;
            void back()
            {
                var fn = System.Windows.Forms.Application.StartupPath;
                if (fn.Length > 1 && fn[0] == '\\' && fn[1] == '\\') fn = "\\\\" + fn;
                fn += "\\jyuviewer.db3";

                System.IO.File.Copy(fn, fn + $"_{DateTime.Now.ToString("yyMMddHHmmss")}.back");
                backuped = true;
            }

            if (DB.Main.ExistsTable("Imports"))
            {
                //リネーム
                if (!backuped) back();
                using (var t = DB.Main.CreateTran())
                {
                    var sql = "ALTER TABLE Imports RENAME TO Import;";
                    if (!DB.Main.Excute(sql, null, t)) throw new Exception("Index作成に失敗しました");
                    t.Commit();
                }
            }

            if (!DB.Main.ExistsTable(nameof(Import)))
            {
                if (!backuped) back();
                using (var t = DB.Main.CreateTran())
                {
                    DB.Main.CreateTable<Import>(t);
                    t.Commit();
                }
            }

            if (!DB.Main.ExistsTable("Rece2"))
            {
                if (!backuped) back();
                using (var t = DB.Main.CreateTran())
                using (var f = new WaitForm())
                {
                    f.ShowDialogOtherTask();
                    f.LogPrint("DB構造のアップデートを行なっています…");

                    DB.Main.CreateTable<Rece2>(t);

                    //Index作成
                    var sql = "CREATE INDEX Rece2Num_Idx ON Rece2(Num)";
                    if (!DB.Main.Excute(sql, null, t)) throw new Exception("Index作成に失敗しました");

                    sql = "CREATE INDEX Rece2Total_Idx ON Rece2(Total)";
                    if (!DB.Main.Excute(sql, null, t)) throw new Exception("Index作成に失敗しました");


                    //データ変換
                    if (!DB.Main.ExistsTable("Rece"))
                    {
                        t.Commit();
                        return true;
                    }
                    var l = DB.Main.SelectAll<Rece>();

                    f.LogPrint("データの変換を行なっています…");
                    f.InvokeMax = l.Count();
                    f.InvokeBarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    //テーブル作成
                    foreach (var item in l)
                    {
                        f.InvokeValue++;

                        var r = new Rece2();
                        r.AID = item.AID;
                        r.ImportID = item.ImportID;
                        r.RrID = item.RrID;
                        r.CYM = item.CYM;
                        r.YM = item.YM;
                        r.AppType = item.AppType;
                        r.InsNum = item.InsNum;
                        r.Num = item.Num;
                        r.Name = item.Name;
                        r.Kana = item.Kana;
                        r.Sex = item.Sex;
                        r.Birth = item.Birth;
                        r.ShokenDate = item.ShokenDate;
                        r.StartDate = item.StartDate;
                        r.Family = item.Family;
                        r.Fusho1 = item.Fusho1;
                        r.Fusho2 = item.Fusho2;
                        r.Fusho3 = item.Fusho3;
                        r.Fusho4 = item.Fusho4;
                        r.Fusho5 = item.Fusho5;
                        r.FushoCount = item.FushoCount;
                        r.AnmaCount = item.AnmaCount;
                        r.AnmaBody = item.AnmaBody;
                        r.AnmaRightUpper = item.AnmaRightUpper;
                        r.AnmaLeftUpper = item.AnmaLeftUpper;
                        r.AnmaRightLower = item.AnmaRightLower;
                        r.AnmaLeftLower = item.AnmaLeftLower;
                        r.Total = item.Total;
                        r.Ratio = item.Ratio;
                        r.Partial = item.Partial;
                        r.Charge = item.Charge;
                        r.Days = item.Days;
                        r.VisitFee = item.VisitFee;
                        r.DrNum = item.DrNum;
                        r.DrName = item.DrName;
                        r.ClinicNum = item.ClinicNum;
                        r.ClinicName = item.ClinicName;
                        r.GroupNum = item.GroupNum;
                        r.GroupName = item.GroupName;
                        r.Zip = item.Zip;
                        r.Adds = item.Adds;
                        r.DestName = item.DestName;
                        r.DestKana = item.DestKana;
                        r.DestZip = item.DestZip;
                        r.DestAdds = item.DestAdds;
                        r.Numbering = item.Numbering;
                        r.ComNum = item.ComNum;
                        r.ImageFile = item.ImageFile;
                        r.ShokaiID = item.ShokaiID;
                        r.ShokaiReasons = (ShokaiReason)(int)item.ShokaiReason;

                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.施術部位相違)) r.ShokaiResult |= ShokaiResult.相違あり;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.署名欄疑義)) r.ShokaiResult |= ShokaiResult.相違あり;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.負傷原因相違)) r.ShokaiResult |= ShokaiResult.相違あり;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.負傷年月相違)) r.ShokaiResult |= ShokaiResult.相違あり;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.その他疑義)) r.ShokaiResult |= ShokaiResult.相違あり;

                        if (item.KagoReason.HasFlag(KAGO_REASON.原因なし)) r.KagoReasons |= KagoReason.原因なし;
                        if (item.KagoReason.HasFlag(KAGO_REASON.家族同一筆跡)) r.KagoReasons |= KagoReason.家族同一筆跡;
                        if (item.KagoReason.HasFlag(KAGO_REASON.筆跡違い)) r.KagoReasons |= KagoReason.筆跡違い;
                        if (item.KagoReason.HasFlag(KAGO_REASON.署名違い)) r.KagoReasons |= KagoReason.署名違い;
                        if (item.KagoReason.HasFlag(KAGO_REASON.長期理由なし)) r.KagoReasons |= KagoReason.長期理由なし;
                        if (item.KagoReason.HasFlag(KAGO_REASON.署名なし)) r.KagoReasons |= KagoReason.その他;
                        r.SaishinsaReasons = (SaishinsaReason)(int)item.SaishinsaReason;

                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.施術部位相違)) r.HenreiReasons |= HenreiReason.負傷部位;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.署名欄疑義)) r.HenreiReasons |= HenreiReason.その他;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.負傷原因相違)) r.HenreiReasons |= HenreiReason.負傷原因;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.負傷年月相違)) r.HenreiReasons |= HenreiReason.負傷時期;
                        if (item.ShokaiResult.HasFlag(SHOKAI_RESULT.その他疑義)) r.HenreiReasons |= HenreiReason.その他;
                        r.HenreiFlag = item.HenreiFlag;
                        if (!item.HenreiFlag) r.HenreiReasons = HenreiReason.なし;

                        r.Note = item.Note;
                        r.ShokaiFile = item.ShokaiFile;

                        DB.Main.Insert(r, t);
                    }
                    t.Commit();

                    f.LogPrint("旧データを削除しています…");
                    sql = "DROP TABLE Rece;";
                    DB.Main.Excute(sql, null);

                    f.LogPrint("再構築しています…");
                    sql = "VACUUM;";
                    DB.Main.Excute(sql, null);
                }
            }

            if (!DB.Main.ExistsColumn("Rece2", "ViewerMemo"))
            {
                var sql = "ALTER TABLE Rece2 ADD COLUMN ViewerMemo text NOT NULL DEFAULT '';";
                DB.Main.Excute(sql, null);
            }

            if (!DB.Main.ExistsColumn("Rece2", "DouiDate"))
            {
                var sql = "ALTER TABLE Rece2 ADD COLUMN DouiDate text NOT NULL DEFAULT 0;";
                DB.Main.Excute(sql, null);
            }

            if (!DB.Main.ExistsColumn("Rece2", "FinishDate"))
            {
                var sql = "ALTER TABLE Rece2 ADD COLUMN FinishDate text NOT NULL DEFAULT 0;";
                DB.Main.Excute(sql, null);
            }

            if (!DB.Main.ExistsColumn("Rece2", "NewCont"))
            {
                var sql = "ALTER TABLE Rece2 ADD COLUMN NewCont text NOT NULL DEFAULT 0;";
                DB.Main.Excute(sql, null);
            }

            // 大阪広域用の続紙フラグ追加　ここから
            if (!DB.Main.ExistsColumn("Rece2", "HasDouisyo"))
            {
                var sql = "ALTER TABLE Rece2 ADD COLUMN HasDouisyo boolean NOT NULL default 0;";
                DB.Main.Excute(sql, null);
            }

            if (!DB.Main.ExistsColumn("Rece2", "HasSejutsuHoukokusyo"))
            {
                var sql = "ALTER TABLE Rece2 ADD COLUMN HasSejutsuHoukokusyo boolean NOT NULL default 0;";
                DB.Main.Excute(sql, null);
            }

            if (!DB.Main.ExistsColumn("Rece2", "HasJoutaikinyusyo"))
            {
                var sql = "ALTER TABLE Rece2 ADD COLUMN HasJoutaikinyusyo boolean NOT NULL default 0;";
                DB.Main.Excute(sql, null);
            }
            // 大阪広域用の続紙フラグ追加　ここまで

            return true;
        }
    }
}
