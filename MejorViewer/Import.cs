﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorViewer
{
    class Import
    {
        [DB.DbAttribute.PrimaryKey]
        public int ImportID { get; private set; }
        public int ImportDate { get; private set; }
        public int CYM { get; private set; }
        public int ImportCount { get; private set; }
        public string Path { get; private set; }

        public static IEnumerable<Import> GetAllImport()
        {
            var sql = "SELECT * FROM import ORDER BY importid;";
            return DB.Main.Query<Import>(sql, null);
        }

        public static int GetMaxID()
        {
            var ex = "SELECT * FROM import LIMIT 1";
            var exres = DB.Main.Query<Import>(ex, null);
            if (exres.Count() == 0) return 0;

            var sql = "SELECT MAX(importid) FROM import;";
            var res = DB.Main.Query<int>(sql, null);
            if (res == null) return 0;
            return res.FirstOrDefault();
        }

        public bool Insert(DB.Transaction tran)
        {
            var sql = "INSERT INTO import " +
                "(ImportID, ImportDate, CYM, ImportCount, Path) " +
                "VALUES " +
                "(@ImportID, @ImportDate, @CYM, @ImportCount, @Path);";

            return DB.Main.Excute(sql, this, tran);
        }

        public static bool StartImport(string fileName)
        {
            var wf = new WaitForm();

            try
            {
                Task.Factory.StartNew(() => wf.ShowDialog());
                while (!wf.Visible) System.Threading.Thread.Sleep(10);

                wf.LogPrint("インポート情報を確認しています");
                var ip = new Import();
                ip.ImportID = GetMaxID() + 1;
                ip.Path = fileName;
                ip.ImportDate = DateTimeEx.ToInt(DateTime.Today);

                int var1 = 0, var2 = 0, var3 = 0, var4 = 0;

                using (var sr = new System.IO.StreamReader(fileName, Encoding.UTF8))
                {
                    while (sr.Peek() > 0)
                    {
                        var ss = sr.ReadLine().Split(':');
                        if (ss.Length < 2) continue;
                        if (ss[0] == "件数") ip.ImportCount = int.Parse(ss[1]);
                        if (ss[0] == "処理年月") ip.CYM = int.Parse(ss[1]);
                        if (ss[0] == "必要バージョン")
                        {
                            var vs = ss[1].Split('.');
                            var1 = vs.Length > 0 ? int.Parse(vs[0]) : 0;
                            var2 = vs.Length > 1 ? int.Parse(vs[1]) : 0;
                            var3 = vs.Length > 2 ? int.Parse(vs[2]) : 0;
                            var4 = vs.Length > 3 ? int.Parse(vs[3]) : 0;
                        }
                    }
                }

                if (var1 < 2)
                {
                    Common.WarningMsg("必要なバージョンを満たしていません。インポートを中止します。");
                    return false;
                }

                var csvFileName = System.IO.Path.GetDirectoryName(fileName) + "\\" + ip.CYM.ToString() + ".csv";
                if (ip.ImportCount == 0 || ip.CYM == 0 || !System.IO.File.Exists(csvFileName))
                {
                    Common.WarningMsg("必要な情報、またはファイルがありません。インポートを中止します。");
                    return false;
                }

                return Rece2.ImportRece(csvFileName, ip, wf);
            }
            finally
            {
                wf.InvokeCloseDispose();
            }
        }
    }
}
