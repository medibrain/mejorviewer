﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorViewer
{
    class Rece2
    {
        [DB.DbAttribute.Ignore]
        public bool Select { get; set; } = false;
        [DB.DbAttribute.PrimaryKey]
        public int AID { get; set; }
        public int ImportID { get; set; }
        public int RrID { get; set; }
        public int CYM { get; set; }
        public int YM { get; set; }
        public APP_TYPE AppType { get; set; } = APP_TYPE.Null;
        public string InsNum { get; set; } = string.Empty;
        public string Num { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Kana { get; set; } = string.Empty;
        public SEX Sex { get; set; } = SEX.Null;
        public int Birth { get; set; }
        public int ShokenDate { get; set; }
        public int StartDate { get; set; }
        public int FinishDate { get; set; }
        public int Family { get; set; }
        public string Fusho1 { get; set; } = string.Empty;
        public string Fusho2 { get; set; } = string.Empty;
        public string Fusho3 { get; set; } = string.Empty;
        public string Fusho4 { get; set; } = string.Empty;
        public string Fusho5 { get; set; } = string.Empty;
        public int FushoCount { get; set; }
        public int AnmaCount { get; set; }
        public bool AnmaBody { get; set; } = false;
        public bool AnmaRightUpper { get; set; } = false;
        public bool AnmaLeftUpper { get; set; } = false;
        public bool AnmaRightLower { get; set; } = false;
        public bool AnmaLeftLower { get; set; } = false;
        public NewCont NewCont { get; set; } =  NewCont.Null;
        public int Total { get; set; }
        public int Ratio { get; set; }
        public int Partial { get; set; }
        public int Charge { get; set; }
        public int Days { get; set; }
        public bool VisitFee { get; set; } = false;
        public string DrNum { get; set; } = string.Empty;
        public string DrName { get; set; } = string.Empty;
        public string ClinicNum { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public string GroupNum { get; set; } = string.Empty;
        public string GroupName { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Adds { get; set; } = string.Empty;
        public string DestName { get; set; } = string.Empty;
        public string DestKana { get; set; } = string.Empty;
        public string DestZip { get; set; } = string.Empty;
        public string DestAdds { get; set; } = string.Empty;
        public string Numbering { get; set; } = string.Empty;
        public string ComNum { get; set; } = string.Empty;
        public string ImageFile { get; set; } = string.Empty;
        public int DouiDate { get; set; }

        public string ShokaiID { get; set; } = string.Empty;
        public ShokaiReason ShokaiReasons { get; set; } = ShokaiReason.なし;
        public ShokaiResult ShokaiResult { get; set; } = ShokaiResult.なし;
        public KagoReason KagoReasons { get; set; } = KagoReason.なし;
        public SaishinsaReason SaishinsaReasons { get; set; } = SaishinsaReason.なし;
        public bool HenreiFlag { get; set; } = false;
        public HenreiReason HenreiReasons { get; set; } = HenreiReason.なし;
        [DB.DbAttribute.Ignore]
        public string Henrei => HenreiFlag ? "返戻" : "";
        public string Note { get; set; } = string.Empty;
        public string ShokaiFile { get; set; } = string.Empty;
        // 大阪広域用の続紙フラグ追加　ここから
        // 同意書あり
        public bool HasDouisyo { get; set; } = false;
        // 施術報告書あり
        public bool HasSejutsuHoukokusyo { get; set; } = false;
        // 状態記入書あり
        public bool HasJoutaikinyusyo { get; set; } = false;
        // 大阪広域用の続紙フラグ追加　ここまで

        public string ViewerMemo { get; set; } = string.Empty;
        
        public int ViewerMemoColor { get; set; }    //20221101 ito st 兵庫広域対応

        [DB.DbAttribute.Ignore]
        public string ImageFullPath
        {
            get
            {
                //20221101 ito st 兵庫広域対応
                //var s = Common.ImgFolder;
                //s += "\\" + ImportID.ToString();
                //s += "\\" + ImageFile;
                //return s;
                if (ImageFile.Substring(0, 2) == "SH") //兵庫広域の場合
                {
                    //string s = "c:\\users\\hyogokoiki\\downloads";
                    string s = Settings.ImageFolderOldSystem;
                    s += "\\" + ImageFile.Substring(2,5);
                    s += "\\" + ImageFile.Substring(2,8);
                    s += "\\" + ImageFile;
                    return s;
                }
                else //それ以外の保険者の場合
                {
                    var s = Common.ImgFolder;
                    s += "\\" + ImportID.ToString();
                    s += "\\" + ImageFile;
                    return s;
                }
                //20221101 ito end 兵庫広域対応
            }
        }


        [DB.DbAttribute.Ignore]
        public string ShokaiImageFullPath
        {
            get
            {
                if (ShokaiFile == string.Empty) return string.Empty;
                var s = Common.ImgFolder;
                s += $"\\{ImportID.ToString()}\\{ShokaiFile}";
                return s;
            }
        }

        public static Rece2 SelectByAid(int aid)
        {
            return DB.Main.Select<Rece2>($"aid=@aid").FirstOrDefault();
        }

        public static IEnumerable<Rece2> SelectByWhere(string where)
        {
            return string.IsNullOrWhiteSpace(where) ? 
                DB.Main.SelectAll<Rece2>() :
                DB.Main.Select<Rece2>(where);
        }

        private static List<Rece2> csvToRece(int importID, string fileName)
        {
            var csv = Common.ImportUTF8(fileName);

            var header = csv[0];
            var indexes = new Dictionary<string, int>();
            for (int i = 0; i < header.Length; i++) indexes.Add(header[i], i);
            var getValue = new Func<string[], string, string>((line, name) =>
                indexes.ContainsKey(name) ? line[indexes[name]] : null);

            var l = new List<Rece2>();
            for (int i = 1; i < csv.Count; i++)
            {
                var line = csv[i];
                var r = new Rece2();
                r.AID = int.Parse(getValue(line, nameof(AID)) ?? "0");
                r.ImportID = importID;
                r.RrID = int.Parse(getValue(line, nameof(RrID)) ?? "0");
                r.CYM = int.Parse(getValue(line, nameof(CYM)) ?? "0");
                r.YM = int.Parse(getValue(line, nameof(YM)) ?? "0");
                r.AppType = (APP_TYPE)int.Parse(getValue(line, nameof(AppType)) ?? "0");
                r.InsNum = getValue(line, nameof(InsNum)) ?? "";
                r.Num = getValue(line, nameof(Num)) ?? "";
                r.Name = getValue(line, nameof(Name)) ?? "";
                r.Kana = getValue(line, nameof(Kana)) ?? "";
                r.Sex = (SEX)int.Parse(getValue(line, nameof(Sex)) ?? "0");
                r.Birth = int.Parse(getValue(line, nameof(Birth)) ?? "0");
                r.ShokenDate = int.Parse(getValue(line, nameof(ShokenDate)) ?? "0");
                r.StartDate = int.Parse(getValue(line, nameof(StartDate)) ?? "0");
                r.FinishDate = int.Parse(getValue(line, nameof(FinishDate)) ?? "0");
                r.Family = int.Parse(getValue(line, nameof(Family)) ?? "0");
                r.Fusho1 = getValue(line, nameof(Fusho1)) ?? "";
                r.Fusho2 = getValue(line, nameof(Fusho2)) ?? "";
                r.Fusho3 = getValue(line, nameof(Fusho3)) ?? "";
                r.Fusho4 = getValue(line, nameof(Fusho4)) ?? "";
                r.Fusho5 = getValue(line, nameof(Fusho5)) ?? "";
                r.FushoCount = int.Parse(getValue(line, nameof(FushoCount)) ?? "0");
                r.AnmaCount = int.Parse(getValue(line, nameof(AnmaCount)) ?? "0");
                r.AnmaBody = (getValue(line, nameof(AnmaBody)) ?? "0") == "1";
                r.AnmaRightUpper = (getValue(line, nameof(AnmaRightUpper)) ?? "0") == "1";
                r.AnmaLeftUpper = (getValue(line, nameof(AnmaLeftUpper)) ?? "0") == "1";
                r.AnmaRightLower = (getValue(line, nameof(AnmaRightLower)) ?? "0") == "1";
                r.AnmaLeftLower = (getValue(line, nameof(AnmaLeftLower)) ?? "0") == "1";
                r.NewCont = (NewCont)int.Parse(getValue(line, nameof(NewCont)) ?? "0");
                r.Total = int.Parse(getValue(line, nameof(Total)) ?? "0");
                r.Ratio = int.Parse(getValue(line, nameof(Ratio)) ?? "0");
                r.Partial = int.Parse(getValue(line, nameof(Partial)) ?? "0");
                r.Charge = int.Parse(getValue(line, nameof(Charge)) ?? "0");
                r.Days = int.Parse(getValue(line, nameof(Days)) ?? "0");
                r.VisitFee = (getValue(line, nameof(VisitFee)) ?? "0") == "1";
                r.DrNum = getValue(line, nameof(DrNum)) ?? "";
                r.DrName = getValue(line, nameof(DrName)) ?? "";
                r.ClinicNum = getValue(line, nameof(ClinicNum)) ?? "";
                r.ClinicName = getValue(line, nameof(ClinicName)) ?? "";
                r.GroupNum = getValue(line, nameof(GroupNum)) ?? "";
                r.GroupName = getValue(line, nameof(GroupName)) ?? "";
                r.Zip = getValue(line, nameof(Zip)) ?? "";
                r.Adds = getValue(line, nameof(Adds)) ?? "";
                r.DestName = getValue(line, nameof(DestName)) ?? "";
                r.DestKana = getValue(line, nameof(DestKana)) ?? "";
                r.DestZip = getValue(line, nameof(DestZip)) ?? "";
                r.DestAdds = getValue(line, nameof(DestAdds)) ?? "";
                r.Numbering = getValue(line, nameof(Numbering)) ?? "";
                r.ComNum = getValue(line, nameof(ComNum)) ?? "";
                r.ImageFile = getValue(line, nameof(ImageFile)) ?? "";
                r.DouiDate = int.Parse(getValue(line, nameof(DouiDate)) ?? "0");

                r.HasDouisyo = (getValue(line, nameof(HasDouisyo)) ?? "0") == "1";
                r.HasSejutsuHoukokusyo = (getValue(line, nameof(HasSejutsuHoukokusyo)) ?? "0") == "1";
                r.HasJoutaikinyusyo = (getValue(line, nameof(HasJoutaikinyusyo)) ?? "0") == "1";

                l.Add(r);
            }
            return l;
        }

        public static bool ImportRece(string csvName, Import ip, WaitForm wf)
        {
            var l = csvToRece(ip.ImportID, csvName);
            wf.LogPrint("データベースに登録しています");

            var insertList = new List<Rece2>();

            using (var tran = DB.Main.CreateTran())
            {
                if (!ip.Insert(tran))
                {
                    tran.Rollback();
                    return false;
                }

                int count = 0;
                foreach (var item in l)
                {
                    count++;
                    insertList.Add(item);
                    if (count % 100 == 0 || count == l.Count)
                    {
                        if (!DB.Main.Inserts(insertList, tran))
                        {
                            tran.Rollback();
                            return false;
                        }
                        insertList.Clear();
                    }
                }

                //画像コピー
                var sorceDir = System.IO.Path.GetDirectoryName(csvName) + "\\Img";
                var imgDir = Common.ImgFolder + "\\" + ip.ImportID.ToString();

                System.IO.Directory.CreateDirectory(imgDir);
                wf.LogPrint("画像のコピー中です…");
                var dir = Common.ImgFolder + "\\" + ip.ImportID.ToString();

                int imgCount = 0;

                try
                {
                    System.IO.Directory.CreateDirectory(dir);
                    var fc = new FastCopy();
                    var fs = System.IO.Directory.GetFiles(sorceDir);
                    wf.InvokeMax = fs.Count();
                    wf.InvokeValue = 0;
                    wf.InvokeBarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    foreach (var item in fs)
                    {
                        if (wf.Cancel)
                        {
                            var res = System.Windows.Forms.MessageBox.Show("データの取り込みは完了しています。インポートデータを残しますか？",
                                "データ確認", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                            if (res == System.Windows.Forms.DialogResult.Yes)
                            {
                                tran.Commit();
                                System.Windows.Forms.MessageBox.Show("画像はインポートしきれていません。ご注意ください。");
                                return false;
                            }
                            else
                            {
                                tran.Rollback();
                                return false;
                            }
                        }

                        var fileaName = dir + "\\" + System.IO.Path.GetFileName(item);
                        fc.FileCopy(item, fileaName);

                        imgCount++;
                        wf.InvokeValue++;
                        if (imgCount % 5000 == 0) wf.LogPrint(imgCount.ToString() + "件の画像コピーが終了しました");
                    }
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    Log.ErrorWrite(ex);
                    return false;
                }

                tran.Commit();
            }

            return true;
        }

        public static bool DeleteImport(int importID)
        {
            using (var tran = DB.Main.CreateTran())
            {
                var sql = "DELETE FROM rece2 WHERE ImportID=@ImportID";
                var res = DB.Main.Excute(sql, new { ImportID = importID }, tran);

                sql = "DELETE FROM import WHERE ImportID=@ImportID";
                res &= DB.Main.Excute(sql, new { ImportID = importID }, tran);

                if (!res) tran.Rollback();
                else tran.Commit();

                return res;
            }
        }

        public bool UpdateViewerMemo(string memo)
        {
            ViewerMemo = memo;

            try
            {
                var sql = "UPDATE rece2 SET ViewerMemo=@ViewerMemo WHERE AID=@AID";
                return DB.Main.Excute(sql, this);
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
        }

        //20221101 ito st 兵庫広域対応
        public bool UpdateViewerMemo2(string memo, int memoColor = 0)
        {
            ViewerMemo = memo;
            ViewerMemoColor = memoColor;

            try
            {
                var sql = "UPDATE rece2 SET ViewerMemo=@ViewerMemo, ViewerMemoColor=@ViewerMemoColor WHERE AID=@AID";
                return DB.Main.Excute(sql, this);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
        }
        //20221101 ito end 兵庫広域対応
    }
}
