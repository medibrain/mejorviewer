﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MejorViewer
{
    public class FastCopy
    {
        //1メガバイト確保
        byte[] bs = new byte[10000000];
        int len = 0;

        public bool FileCopy(string sourceFileName, string destFileName)
        {
            try
            {
                using (var rs = new FileStream(sourceFileName, FileMode.Open))
                {
                    len = (int)rs.Length;
                    if (len > 10000000)
                    {
                        //10メガキャッシュ以上の場合、遅いがそのままコピー
                        File.Copy(sourceFileName, destFileName);
                        return true;
                    }
                    rs.Read(bs, 0, len);
                }

                using (var ws = new FileStream(destFileName, FileMode.Create))
                {
                    ws.Write(bs, 0, len);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool DirCopy(string copyDir, string sendDir)
        {
            var fs = Directory.GetFiles(copyDir);
            int len = 0;

            try
            {
                foreach (var item in fs)
                {
                    using (var rs = new FileStream(item, FileMode.Open))
                    {
                        len = (int)rs.Length;
                        if (len > 10000000) throw new Exception("buffer over");
                        rs.Read(bs, 0, len);
                    }

                    var fileName = sendDir + "\\" + Path.GetFileName(item);
                    using (var ws = new FileStream(fileName, FileMode.Create))
                    {
                        ws.Write(bs, 0, len);
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
