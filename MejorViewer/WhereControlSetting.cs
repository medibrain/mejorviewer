﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MejorViewer
{
    class WhereControlSetting
    {
        static List<WhereControlSetting> settings = new List<WhereControlSetting>();
        static string fileName = Application.StartupPath + "\\WhereControls";

        public string Name { get; set; }
        public string Text { get; set; }
        public int Index { get; set; }
        bool _mainVisible = false, _subVisible = false;
        public bool MainVisible
        {
            get { return _mainVisible; }
            set
            {
                _mainVisible = value;
                if (value) _subVisible = false;
            }
        }
        public bool SubVisible
        {
            get { return _subVisible; }
            set
            {
                _subVisible = value;
                if (value) _mainVisible = false;
            }
        }

        public static List<WhereControlSetting> GetSettings()
        {
            return settings;
        }

        public static void Load(FlowLayoutPanel panel)
        {
            settings.Clear();
            if (System.IO.File.Exists(fileName))
            {
                using (var sr = new System.IO.StreamReader(fileName, Encoding.UTF8))
                {
                    while (sr.Peek() > 0)
                    {
                        var ss = sr.ReadLine().Split(',');
                        if (ss.Length < 4) continue;
                        var cs = new WhereControlSetting();
                        cs.Name = ss[0];
                        cs.Index = int.Parse(ss[2]);
                        cs.MainVisible = ss[3] == "True";
                        cs.SubVisible = ss[4] == "True";
                        settings.Add(cs);
                    }
                }
            }

            //セッティングにないコントロール
            bool needSave = false;
            foreach (Control item in panel.Controls)
            {
                if (item.Name == "panelSearch") continue;
                if (item.Name == "panelSeparator") continue;
                var ct = settings.FirstOrDefault(c => c.Name == item.Name);
                if (ct == null)
                {
                    var cs = new WhereControlSetting();
                    cs.Name = item.Name;
                    cs.Text = item.Text;
                    cs.Index = settings.Count;
                    cs.MainVisible = false;
                    cs.SubVisible = false;
                    settings.Add(cs);
                    needSave = true;
                }
                else
                {
                    ct.Text = item.Text;
                }
            }
            
            if (System.IO.File.Exists(fileName) && needSave) Save();
        }

        public static void Load()
        {
            settings.Clear();
            if (System.IO.File.Exists(fileName))
            {
                using (var sr = new System.IO.StreamReader(fileName, Encoding.UTF8))
                {
                    while (sr.Peek() > 0)
                    {
                        var ss = sr.ReadLine().Split(',');
                        if (ss.Length < 4) continue;
                        var cs = new WhereControlSetting();
                        cs.Name = ss[0];
                        cs.Text = ss[1];
                        cs.Index = int.Parse(ss[2]);
                        cs.MainVisible = ss[3] == "True";
                        cs.SubVisible = ss[4] == "True";
                        settings.Add(cs);
                    }
                }
            }
        }

        public static void SetMainMode(FlowLayoutPanel panel)
        {
            settings.Sort((x, y) => x.Index.CompareTo(y.Index));
            foreach (Control item in panel.Controls)
            {
                var s = settings.FirstOrDefault(c => c.Name == item.Name);
                if (s == null) continue;
                item.Visible = s.MainVisible;
            }

            int tabIndex = 0;
            foreach (var item in settings)
            {
                var c = panel.Controls.Find(item.Name, false);
                if (c == null || c.Count() != 1) continue;
                panel.Controls.SetChildIndex(c[0], item.Index);
                c[0].TabIndex = tabIndex++;
            }

            //必ず最後に表示
            var ps = panel.Controls.Find("panelSearch", false)[0];
            ps.Visible = true;
            panel.Controls.SetChildIndex(ps, panel.Controls.Count);
            ps = panel.Controls.Find("panelSeparator", false)[0];
            ps.Visible = false;
        }

        public static void SetSubMode(FlowLayoutPanel panel)
        {
            panel.SuspendLayout();

            settings.Sort((x, y) => x.Index.CompareTo(y.Index));
            foreach (Control item in panel.Controls)
            {
                var s = settings.FirstOrDefault(c => c.Name == item.Name);
                if (s == null) continue;
                item.Visible = s.MainVisible || s.SubVisible;
            }

            int index = 0;

            //メイン
            foreach (var item in settings)
            {
                if (!item.MainVisible) continue;
                var c = panel.Controls.Find(item.Name, false);
                if (c == null || c.Count() != 1) continue;
                panel.Controls.SetChildIndex(c[0], index);
                c[0].TabIndex = index;
                index++;
            }

            //真ん中に表示
            var ps = panel.Controls.Find("panelSearch", false)[0];
            ps.Visible = true;
            panel.Controls.SetChildIndex(ps, index);
            index++;
            ps = panel.Controls.Find("panelSeparator", false)[0];
            ps.Visible = true;
            panel.Controls.SetChildIndex(ps, index);
            index++;

            //サブ
            foreach (var item in settings)
            {
                if (!item.SubVisible) continue;
                var c = panel.Controls.Find(item.Name, false);
                if (c == null || c.Count() != 1) continue;
                panel.Controls.SetChildIndex(c[0], index);
                c[0].TabIndex = index;
                index++;
            }

            panel.ResumeLayout();
        }

        public static void Save()
        {
            using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
            {
                foreach (var item in settings)
                {
                    var ss = new string[] {
                            item.Name,
                            item.Text,
                            item.Index.ToString(),
                            item.MainVisible ? "True":"False",
                            item.SubVisible ? "True":"False"
                        };

                    sw.WriteLine(string.Join(",", ss));
                }
            }
        }
    }
}
