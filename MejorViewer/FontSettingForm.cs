﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MejorViewer
{
    public partial class FontSettingForm : Form
    {
        public FontSettingForm()
        {
            InitializeComponent();

            //インストールされているすべてのフォントファミリアを取得
            var ifc = new System.Drawing.Text.InstalledFontCollection();
            FontFamily[] ffs = ifc.Families;

            var nowFontName = Settings.NameFont;
            foreach (FontFamily ff in ffs)
            {
                //スタイルにRegularが使用できるフォントのみを表示
                if (!ff.IsStyleAvailable(FontStyle.Regular)) continue;

                comboBox1.Items.Add(ff.Name);
                if (nowFontName == ff.Name) comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Settings.NameFont = (string)comboBox1.SelectedItem;
            Settings.Save();
            this.DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
