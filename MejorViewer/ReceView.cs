﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorViewer
{
    class ReceView
    {
        private Rece2 r;
        private ReceView(Rece2 rece) { r = rece; }

        public static List<ReceView> GetViewList(IEnumerable<Rece2> rs)
        {
            var l = new List<ReceView>();
            foreach (var item in rs)l.Add(new ReceView(item));
            return l;
        }   

        public static List<Rece2> GetReceList(IEnumerable<ReceView> rs)
        {
            var l = new List<Rece2>();
            foreach (var item in rs)l.Add(item.r);
            return l;
        }   

        private string getStr(int value) =>
             value == 0 ? string.Empty : value.ToString();

        private string getYmStr(int value) =>
             value == 0 ? string.Empty : value.ToString("0000/00");

        private string getJymStr(int value) =>
             value == 0 ? string.Empty : value.ToString("00/00");

        private string getDtStr(int value) =>
             value == 0 || value == 10101 ? string.Empty : value.ToString("0000/00/00");

        private int? getZeroNullInt(int value) =>
            value == 0 ? null : (int?)value;

        public bool Select { get; set; }
        public int AID => r.AID;
        public int ImportID => r.ImportID;
        public int? RrID => getZeroNullInt(r.RrID);
        public string CYM => getYmStr(r.CYM);
        public string JpCYM => getJymStr(DateTimeEx.GetYymmFromAdYM(r.CYM));
        public string YM => getYmStr(r.YM);
        public string JpYM => getJymStr(DateTimeEx.GetYymmFromAdYM(r.YM));
        public string AppType => r.AppType == APP_TYPE.Null ? string.Empty : r.AppType.ToString();
        public string InsNum => r.InsNum;
        public string Num => r.Num;
        public string Name => r.Name;
        public string Kana => r.Kana;
        public string Sex => r.Sex == SEX.Null ? string.Empty : r.Sex.ToString();
        public string Birth => getDtStr(r.Birth);
        public string JpBirth => DateTimeEx.GetShortJpDateStr(r.Birth);
        public string ShokenDate => getDtStr(r.ShokenDate);
        public string JpShokenDate => DateTimeEx.GetShortJpDateStrWithoutEra(r.ShokenDate);
        public string StartDate => getDtStr(r.StartDate);
        public string JpStartDate => DateTimeEx.GetShortJpDateStrWithoutEra(r.StartDate);
        public string FinishDate => getDtStr(r.FinishDate);
        public string JpFinishDate => DateTimeEx.GetShortJpDateStrWithoutEra(r.FinishDate);
        public int? Family => getZeroNullInt(r.Family);
        public string Fusho1 => r.Fusho1;
        public string Fusho2 => r.Fusho2;
        public string Fusho3 => r.Fusho3;
        public string Fusho4 => r.Fusho4;
        public string Fusho5 => r.Fusho5;
        public int? FushoCount => getZeroNullInt(r.FushoCount);
        public int? AnmaCount => getZeroNullInt(r.AnmaCount);
        public string AnmaBody => r.AnmaBody ? "○" : string.Empty;
        public string AnmaRightUpper => r.AnmaRightUpper ? "○" : string.Empty;
        public string AnmaLeftUpper => r.AnmaLeftUpper ? "○" : string.Empty;
        public string AnmaRightLower => r.AnmaRightLower ? "○" : string.Empty;
        public string AnmaLeftLower => r.AnmaLeftLower ? "○" : string.Empty;
        public string NewCont =>
            r.NewCont == MejorViewer.NewCont.新規 ? "新規" :
            r.NewCont == MejorViewer.NewCont.継続 ? "継続" : string.Empty;
        public int? Total => getZeroNullInt(r.Total);
        public int? Ratio => getZeroNullInt(r.Ratio);
        public int? Partial => getZeroNullInt(r.Partial);
        public int? Charge => getZeroNullInt(r.Charge);
        public int? Days => getZeroNullInt(r.Days);
        public string VisitFee => r.VisitFee ? "○" : string.Empty;
        public string DouiDate => getDtStr(r.DouiDate);
        public string JpDouiDate => DateTimeEx.GetShortJpDateStr(r.DouiDate);
        public string DrNum => r.DrNum;
        public string DrName => r.DrName;
        public string ClinicNum => r.ClinicNum;
        public string ClinicName => r.ClinicName;
        public string GroupNum => r.GroupNum;
        public string GroupName => r.GroupName;
        public string Zip => r.Zip;
        public string Adds => r.Adds;
        public string DestName => r.DestName;
        public string DestKana => r.DestKana;
        public string DestZip => r.DestZip;
        public string DestAdds => r.DestAdds;
        public string Numbering => r.Numbering;
        public string ComNum => r.ComNum;
        public string ImageFile => r.ImageFile;

        public string ShokaiID => r.ShokaiID;
        public string ShokaiReason => r.ShokaiReasons.ToString();
        public string ShokaiResult => r.ShokaiResult.ToString();
        public string KagoReasons => r.KagoReasons.ToString();
        public string SaishinsaReasons => r.SaishinsaReasons.ToString();
        public string Henrei => r.Henrei;
        public string HenreiReasons => r.HenreiReasons.ToString();
        public string Note => r.Note;
        public string ShokaiFile => r.ShokaiFile;

        public string ImageFullPath => r.ImageFullPath;
        public string ShokaiImageFullPath => r.ShokaiImageFullPath;

        public string HasDouisyo => r.HasDouisyo ? "○" : string.Empty;
        public string HasSejutsuHoukokusyo => r.HasSejutsuHoukokusyo ? "○" : string.Empty;
        public string HasJoutaikinyusyo => r.HasJoutaikinyusyo ? "○" : string.Empty;

        public bool GetNeedShokaiButtonVisible() =>
            r.ShokaiResult == MejorViewer.ShokaiResult.相違あり ||
            r.ShokaiResult == MejorViewer.ShokaiResult.相違なし;

        public string ViewerMemo  => r.ViewerMemo;

        //20221101 ito st 兵庫広域対応
        public string Fushomei => getFushomei();

        //public void UpdateViewerMemo(string memo)
        //{
        //    r.UpdateViewerMemo(memo);
        //}

        public void UpdateViewerMemo2(string memo, int memoColor = 0)
        {
            r.UpdateViewerMemo2(memo, memoColor);
        }

        public int ViewerMemoColor => r.ViewerMemoColor;
        private string getFushomei()
        {
            if (r.Fusho1 == "1") return ShobyomeiCode.神経痛.ToString();
            else if (r.Fusho1 == "2") return ShobyomeiCode.リウマチ.ToString();
            else if (r.Fusho1 == "3") return ShobyomeiCode.頚腕症候群.ToString();
            else if (r.Fusho1 == "4") return ShobyomeiCode.五十肩.ToString();
            else if (r.Fusho1 == "5") return ShobyomeiCode.腰痛症.ToString();
            else if (r.Fusho1 == "6") return ShobyomeiCode.腰椎捻挫後遺症.ToString();
            else if (r.Fusho1 == "7") return ShobyomeiCode.その他.ToString();
            else return string.Empty;
        }
        //20221101 ito end 兵庫広域対応

    }
}
